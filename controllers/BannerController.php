<?php

namespace app\controllers;

use app\components\YiiImagick;
use app\models\Account;
use app\models\Actions;
use app\components\Controller;
use app\models\ActionsLog;
use app\models\ActionsSliders;
use app\models\BalanceFlow;
use app\models\Users;
use app\models\UsersLog;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\web\Response;

class BannerController extends Controller
{
	public function actionIndex($ref, $size)
	{
		return $this->renderPartial('index', [
			'size' => $size,
			'ref' => $ref,
			'nonclickable' => Yii::$app->request->get('nonclickable', false)
		]);
	}


}