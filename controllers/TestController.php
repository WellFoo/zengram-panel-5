<?php

namespace app\controllers;

use app\components\Controller;
use app\models\Account;
use app\models\BalanceFlow;
use app\models\Hashtags;
use app\models\HashtagsRelations;
use app\models\RegionsPlaces;
use Yii;
use app\components\Counters;
use \YandexMoney\API;

class TestController extends Controller
{

	public function actionIndex()
	{

		return $this->render('index');
//		return $this->renderPartial('//layouts/mail', [
//			'caption' => 'Вы успешно зарегистрировались на сайте',
//			'body' => [
//				'Поздравляем!',
//				'Вы успешно зарегистрировались на сайте Zengram.ru.' . PHP_EOL .
//				'Для авторизации на сайте используйте данные введёные при регистрации:',
//				'',
//				'E-mail: ' . '$this->mail' . PHP_EOL .
//				'Пароль: ' . '$passwordOriginal'
//			],
//			'token' => 'asd',
//			'options' => ['price_url' => true]
//		]);
	}
	public function actionDue(){
		Counters::triggerMeasurement('Окончание бесплатного периода', 54);
	}
	public function actionGetAccounts(){
		foreach(Yii::$app->user->identity->accounts as $account){
			echo $account->login.'<br>';
		}
	}
	public  function actionMagicGeo(){

		/*

		Добавление городов в регионы:
		regions - таблица с регионами. Заполняется через админку https://zengram.ru/admin/regions

		При поиска города через автокомплит он записывается в таблицу:
		place

		Для сопоставления региона и места добавляем свяку id-ника места и id-ника региона в таблицу:
		regions_place

		 */

		/* @var $account Account */
/*
		$account = Account::findOne(25305);

		if (!$account->fillWhitelist()) {
			//self::log('Ошибка при обработке аккаунта: '.$account->login);
			//$account->whitelist_added = Whitelist::WHITELIST_DELAY;
			//$account->save();
			echo 'Ошибка при обработке аккаунта: '.$account->login;
		}else{
			echo 'Вайтлист сформирован';
			var_dump($account);
		}
*/
		/*
		$places = $account->places;
		//print_r($places);
		$regionID = 558;

		foreach($places as $place){
			$relationGeo = new RegionsPlaces();
			$relationGeo->region_id = $regionID;
			$relationGeo->place_id = $place->id;
			$relationGeo->save();
			echo 'Добавлен город '. $place->name . ' с id '. $place->id . '<br>';
		}
		*/
	}
	public function actionWhite(){
		/* @var $account Account */
		$account = Account::findOne(25305);

		if (!$account->fillWhitelist()) {
			//self::log('Ошибка при обработке аккаунта: '.$account->login);
			//$account->whitelist_added = Whitelist::WHITELIST_DELAY;
			//$account->save();
			echo 'Ошибка при обработке аккаунта: '.$account->login;
		}else{
			echo 'Вайтлист сформирован';
			var_dump($account);
		}
	}

	public function actionRoiTest(){
		//Counters::roiStatsSendEvent('free_end');
		//Counters::roiClient();
		//Counters::roiAddOrder();
		//Counters::roiStatsRegister();

		/*

				'id' => $invoice['id'],
				'name' => $invoice['name'],
				'date_create' => time(),
				'status' => $invoice['status'],
				'roistat' => $roistatCookie,
				'price' => $invoice['price'],
				'cost' => $invoice['price'],
				'client_id' => Yii::$app->user->id


		 */

		$invoice = [
			'id' => 666,
			'name' => '3 дня',
			'status' => 1,
			'price' => 99,
		];
		Counters::roiInvoice($invoice);
	}

	public function actionAddHashTags(){
		$geoId = 3038; // Москва
		$hashtags = ['кремль', 'метро', 'красная площадь', 'макдак', 'ленин'];

		/*
		$model = new HashtagsRelations();
		$model->place_id = $geoId;
		$model->hashtags_ids = [4,5,6];
		$model->save();
		*/
		$hashtagIds = [];
		$hashtagPlacesIds = [];
		foreach($hashtags as $hashtag){
			/** @var Hashtags $model  */
			// Сохраняем хэштеги. В случае дублей, получаем id-ники хэштегов и увеличиваем счётчик
			$q = "INSERT INTO hashtags (name)
				  VALUES ('$hashtag')
				  ON CONFLICT (name)
				  DO UPDATE SET counter = hashtags.counter + 1 RETURNING id;";

			$result = Yii::$app->db->createCommand($q)
				->queryOne();

			$hashtagIds[] = $result['id'];

		}
		asort($hashtagIds);

		foreach($hashtagIds as $hashtagId){
			// Записываем связку с регионами
			$q = "INSERT INTO hashtags_places (place_id, hashtags_id)
				  VALUES ($geoId,$hashtagId)
				  ON CONFLICT (place_id, hashtags_id)
				  DO UPDATE SET counter = hashtags_places.counter + 1";

			Yii::$app->db->createCommand($q)
				->queryOne();
		}
		//print_r($hashtagIds);
		// Добавляем в базу массив ключе

		$ids = implode(',', $hashtagIds);
		$q = "INSERT INTO hashtags_relations (place_id, hashtags_ids)
				  VALUES ($geoId,ARRAY[$ids])
				  ON CONFLICT (place_id, hashtags_ids)
				  DO UPDATE SET counter = hashtags_relations.counter + 1";
		Yii::$app->db->createCommand($q)
			->queryOne();
	}
	public function actionBalanceFlowByDate(){
		$model = new BalanceFlow();
		$result = $model->find()->where(['user_id' => '11636'])->orderBy(['date' => SORT_DESC])->asArray()->all();
		//$currentDate = '';
		$sum = 0;
		$sum2 = 0;
		$currentDate = substr($result[0]['date'], 0, 10);
		$accounts = '';
		foreach($result as $row){
			/*if(substr($row['date'], 0, 10) != '2016-05-28' && substr($row['date'], 0, 10) != '2016-05-29') {
				continue;
			}*/
			if (substr($row['date'], 0, 10) != $currentDate){

				$hour = floor($sum/3600);
				$sec = $sum - ($hour*3600);
				$min = floor($sec/60);
				//$sec = $sec - ($min*60);

				$time = $hour.';'.$min;

				echo $time . ';' . $currentDate.'<br>';

				$accounts = '';
				$sum = 0;
				$currentDate = substr($row['date'], 0, 10);
			}

			if (strpos($row['description'],'Автоматическое снятие по планировщику') !== false){
				//echo '<br>'.$row['description'].'<br>';
				$sum += $row['value'];
				//$accounts = .; 4 октября..всего 6...аккаунт #1...ра..отдельными файлами..дата, сколько всего было добавлено...ссылочка
				// брать и по каждому добавленному аккаунту -
				// аккаунт #1 - списания...2 - старт стоп по проекту
			}else{
				$hour = floor($row['value']/3600);
				$sec = $row['value'] - ($hour*3600);
				$min = floor($sec/60);
				//$sec = $sec - ($min*60);

				$time = $hour.';'.$min;

				//echo $time . ';' . $currentDate.';'.$row['description'].'<br>';

			}


			if($currentDate == '2016-05-29'){

				$hour = floor($row['value']/3600);
				$sec = $row['value'] - ($hour*3600);
				$min = floor($sec/60);



				if (strpos($row['description'],'Автоматическое снятие по планировщику') !== false){
					//echo '<br>'.$row['description'].'<br>';
					$sum2 += $row['value'];
					//echo $row['date'].';'.$hour.'ч'.$min.'м;'.$row['description'].'<br>';
				}

				//echo $row['date'].';'.$hour.'ч'.$min.'м;'.$row['description'].'<br>';
			}

			// string substr ( string $string , int $start [, int $length ] )
		}

		$hour = floor($sum/3600);
		$sec = $sum - ($hour*3600);
		$min = floor($sec/60);
		//$sec = $sec - ($min*60);

		$time = $hour.';'.$min;

		//echo $time . ';' . $currentDate.'<br>';

		$hour = floor($sum2/3600);
		$sec = $sum2 - ($hour*3600);
		$min = floor($sec/60);
		//echo '<br><br>'.$hour.':'.$min ;
		//echo $time . ';' . $currentDate.'<br>';
	}
	public function actionCheckGetProxy(){
		/*
		foreach(Yii::$app->user->identity->accounts as $account){
			//echo $account->getFreeProxyId();
			print_r($account);
			var_dump($account->proxy);
			//var_dump($account);
			break;
		}
		*/
	}

	public function actionCheckYad(){
		//$code = Yii::$app->params['ym-secret'];
		//$client_id = Yii::$app->params['ym-id'];

		$redirect_uri = 'http://zengram.ru/test/check-yad/';
		$client_id = '9CC705C3CD092AB01E3BA8F959ACB69B4D6098C73236EB221141A5852E21F942';
		$client_secret = 'A331CB8EFE6DAEF67EB40D38E02F17AC9E574A80323CAF364183489B221A1DD085ED8BDF1D291AE58C08088523D6FF98BAD4735A1C9464B4D1A04B2EF49B97ED';

		$code = Yii::$app->request->get('code', false);
		if(!$code){
			$auth_url = API::buildObtainTokenUrl($client_id, $redirect_uri, ['account-info', 'operation-history', 'operation-details']);
			//echo $auth_url;
			$this->redirect($auth_url);
		}
		echo $code;
		//var_dump($auth_url);
		//$redirect_uri

		/*
		$access_token_response = API::getAccessToken($client_id, $code, $redirect_uri, $client_secret=NULL);
		if(property_exists($access_token_response, "error")) {
			// process error
			echo 'Оишбка подключения';
		}
		$access_token = $access_token_response->access_token;
		*/
		//$access_token = 'A331CB8EFE6DAEF67EB40D38E02F17AC9E574A80323CAF364183489B221A1DD085ED8BDF1D291AE58C08088523D6FF98BAD4735A1C9464B4D1A04B2EF49B97ED';
		//$access_token_response = API::getAccessToken($client_id, $code, $redirect_uri, $client_secret=NULL);
		//$access_token = '9CC705C3CD092AB01E3BA8F959ACB69B4D6098C73236EB221141A5852E21F942';
		//$api = new API($access_token);
		//$api->operationHistory();

		//var_dump($api);
		// get account info
		//$acount_info = $api->accountInfo();
		//var_dump($acount_info);
	}

}