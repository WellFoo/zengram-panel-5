<?php

namespace app\controllers;

use app\models\Discounts;
use app\models\Prices;
use app\components\Controller;
use app\components\Counters;
use Yii;
use app\models\Page;
use yii\web\NotFoundHttpException;

class PageController extends Controller
{
	public function actionAbout()
	{
		Counters::gaFirstSource();
		return $this->render('about');
	}
	
	public function actionContacts()
	{
		return $this->render('contacts');
	}

	public function actionFaq()
	{
		// Сохраняем данные об источнике перехода
		Counters::gaFirstSource();
		return $this->render('faq');
	}

	public function actionTutorial()
	{
		// Сохраняем данные об источнике перехода
		Counters::gaFirstSource();
		return $this->render('tutorial');
	}

	public function actionSitemap()
	{
		$pages = Page::find()->all();
		return $this->render('sitemap', [
			'pages' => $pages
		]);
	}

	public function actionIndex($id)
	{
		if ($id == NULL) {
			throw new NotFoundHttpException();
		} else {
			$model = Page::find()->where("alias = '".$id."'")->one();
		}


		if ($model == NULL) {
			throw new NotFoundHttpException();
		}

		return $this->render('page',
			[
				'title' => $model->title,
				'text' => $model->text
			]);
	}

	public function actionPrice()
	{
		$id       = empty($_GET['sel_id']) ? 1 : intval($_GET['sel_id']);
		$prices   = Prices::find()->all();
		$discount = Discounts::findOne(['code' => Yii::$app->request->get('discount', '')]);

		return $this->render('price', [
			'prices' => $prices,
			'selected_id' => $id,
			'discount' => $discount
		]);
	}
}