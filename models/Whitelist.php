<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "whitelist".
 *
 * @property integer $id
 * @property string  $account_id
 * @property string  $white_id
 * @property string  $login
 */
class Whitelist extends ActiveRecord
{
	const WHITELIST_EMPTY  = 0;
	const WHITELIST_FILLED = 1;
	const WHITELIST_DELAY  = 2;
	const WHITELIST_ERROR  = 3;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'whitelist';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['account_id', 'white_id', 'login'], 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'     => 'ID',
			'account_id'  => 'Instagram ID аккаунта',
			'white_id' => 'Instagram ID беляшика',
			'login' => 'Логин'
		];
	}
}
