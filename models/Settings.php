<?php

namespace app\models;
use \yii\db\ActiveRecord;

/**
 * Class Settings
 * @package app\models
 *
 * @property integer $id
 * @property integer $pause
 * @property string  $setting
 * @property string  $value
 * @property string  $description
 */

class Settings extends ActiveRecord
{

	public static function tableName()
	{
		return 'settings';
	}

	public function rules()
	{
		return [
			[['value'], 'required'],
			[['value'], 'string', 'max' => 255]
		];
	}

	public function attributeLabels()
	{
		return array(
			'setting' => 'Ключ',
			'value' => 'Значение',
			'description' => 'Описание',
		);
	}

}