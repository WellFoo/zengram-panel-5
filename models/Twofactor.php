<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "twofactor".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $added
 */
class Twofactor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'twofactor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['added'], 'safe'],
            [['login', 'password'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'login' => Yii::t('app', 'Login'),
            'password' => Yii::t('app', 'Password'),
            'added' => Yii::t('app', 'Added'),
        ];
    }
}
