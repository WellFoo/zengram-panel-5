<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string  $date
 * @property integer $user_id
 * @property integer $balance
 *
 * This is the model class for table "balance_fix".
 */

class BalanceFix extends ActiveRecord
{
	public static function tableName()
	{
		return 'balance_fix';
	}

	public function rules()
	{
		return [
			[['user_id', 'balance', 'id'], 'integer'],
			[['date'], 'string']
		];
	}
}