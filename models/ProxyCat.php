<?php namespace app\models;

use app\models\Proxy;
use yii\db\ActiveRecord;
/**
 * Категории прокси серверов
 *
 * @author: BetsuNo
 *
 * @property integer      $id
 * @property string       $name
 * @property boolean      $default
 * @property array[Proxy] $proxies
 */

class ProxyCat extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%proxy_cats}}';
	}

	

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['name', 'required'],
			['name', 'string', 'max' => 255],
			['name', 'unique'],
			['default', 'number'],
			['default', 'default', 'value' => false]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'      => 'ID',
			'name'    => 'Название',
			'default' => 'Использовать по умолчанию'
		];
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if ($this->default && !$this->getOldAttribute('default')) {
				self::updateAll(['default' => 0], ['default' => 1]);
			}
			return true;
		}
		return false;
	}

	public function getProxies()
	{
		return self::hasMany(Proxy::className(), ['cat_id' => 'id']);
	}
}
