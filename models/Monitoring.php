<?php

namespace app\models;

use Yii;
use yii\console\Exception;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "monitoring".
 *
 * @property integer $id
 * @property string $type
 * @property integer $category
 * @property integer $count
 * @property integer $value
 * @property string $result
 * @property integer $period
 * @property integer $comparation
 * @property boolean $incorrect
 * @property string $email
 * @property string $title
 * @property string $last_result
 * @property integer $sms
 */
class Monitoring extends ActiveRecord
{
	const MONITORING_PERIOD_10MIN = 0;
	const MONITORING_PERIOD_HOURLY = 1;
	const MONITORING_PERIOD_DAILY = 2;
	const MONITORING_PERIOD_TEST = 9;

	const MONITORING_COMPORATION_LESS = 0;
	const MONITORING_COMPORATION_MORE = 1;
	
	public $count;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'monitoring';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['type', 'value', 'period'], 'required'],
			[['value', 'comparation'], 'integer'],
			['period', 'in', 'range' => [self::MONITORING_PERIOD_10MIN, self::MONITORING_PERIOD_HOURLY, self::MONITORING_PERIOD_DAILY, self::MONITORING_PERIOD_TEST]],
			[['incorrect'], 'boolean'],
			[['type', 'category', 'email', 'sms'], 'string', 'max' => 100],
			[['title', 'result', 'last_result'], 'string', 'max' => 200],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'category'  => 'Категория',
			'value'  => 'Норматив',
			'result' => 'Факт',
			'type'  => 'Функция(для разработчиков)',
			'period' => 'Частота проверки',
			'email'  => 'Email для уведомлений',
			'sms'    => 'Телефон для уведомлений',
			'count'    => 'Количество проишествий',
			'comparation' => 'Условие',
			'title'    => 'Показатель',
		];
	}

	public static function periodLabels(){
		return [
			self::MONITORING_PERIOD_10MIN => 'Раз в 10 минут',
			self::MONITORING_PERIOD_HOURLY => 'Раз в час',
			self::MONITORING_PERIOD_DAILY => 'Раз в день',
			self::MONITORING_PERIOD_TEST => 'Для теста',
		];
	}

	public static function comparationLabels(){
		return [
			self::MONITORING_COMPORATION_LESS => 'Недобор нормы',
			self::MONITORING_COMPORATION_MORE => 'Превышение нормы',
		];
	}
	
	public function getLog(){
		return $this->hasOne(MonitoringLog::className(), ['monitoring_id' => 'id']);
	}

	public function getFails(){
		$dateTo = strtotime(date('Y-m-d'));
		$date = Yii::$app->request->get('date', 'day');

		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 7;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 30;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from'));
				$dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		};
		
		return $this->hasMany(MonitoringLog::className(), ['monitoring_id' => 'id'])
			->from(['l' => MonitoringLog::tableName()])
			->andOnCondition(['l.success' => 0])
			->andOnCondition(['>=', 'l.time', date('Y-m-d', $dateFrom)])
			->andOnCondition(['<', 'l.time', date('Y-m-d', $dateTo)])->count('l.id');
	}

	/** @return float */
	public function getMonthLog(){
		$dateTo = time();
		$dateFrom = $dateTo - 86400 * 30;
		return $this->hasMany(MonitoringLog::className(), ['monitoring_id' => 'id'])
			->from(['m' => MonitoringLog::tableName()])
			->andOnCondition(['>=', 'm.time', new Expression('to_timestamp('.$dateFrom.')')])
			->andOnCondition(['<', 'm.time', new Expression('to_timestamp('.$dateTo.')')])->average('m.result::real');
	}

	/** @return float */
	public function getQuarterLog(){
		$dateTo = time();
		$dateFrom = $dateTo - 86400 * 90;
		return $this->hasMany(MonitoringLog::className(), ['monitoring_id' => 'id'])
			->from(['q' => MonitoringLog::tableName()])
			->andOnCondition(['>=', 'q.time', new Expression('to_timestamp('.$dateFrom.')')])
			->andOnCondition(['<', 'q.time', new Expression('to_timestamp('.$dateTo.')')])->average('q.result::real');
	}
}
