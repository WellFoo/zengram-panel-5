<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "unfollow_queue".
 *
 * @property integer $id
 * @property string  $account_id
 * @property string  $unfollow_id
 * @property integer $date
 */
class UnfollowQueue extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'unfollow_queue';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['account_id', 'unfollow_id'], 'string'],
			[['date', 'id'], 'integer']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'account_id' => Yii::t('app', 'Account ID'),
			'unfollow_id' => Yii::t('app', 'Unfollow ID'),
			'date' => Yii::t('app', 'Date'),
		];
	}

	public static function addUnfollows($date)
	{



	}


}
