<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property string $date
 * @property string $text_short
 * @property string $text
 * @property string $meta_description
 * @property string $image
 * @property string $imagePreview
 */
class Post extends ActiveRecord
{
	const PREVIEW_MAX_SIZE = 240;
	const PREVIEW_SUFFIX = '.preview.jpg';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'posts';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'alias', 'text', 'date'], 'required'],
			[['title', 'alias', 'text', 'text_short', 'image'], 'string'],
			['meta_description', 'string', 'max' => 255],
			['alias', 'match', 'pattern' => '/^[a-zA-Z0-9-_.]+$/'],
			['alias', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'               => 'ID',
			'title'            => 'Заголовок',
			'alias'            => 'Алиас (ЧПУ)',
			'date'             => 'Дата',
			'text_short'       => 'Короткий текст',
			'text'             => 'Полный текст',
			'meta_description' => 'Meta description',
			'image'            => 'Изображение',
		];
	}

	public function beforeSave($insert){
		if (!empty($this->oldAttributes['image']) && $this->oldAttributes['image'] != $this->image){
			@unlink(Yii::getAlias('@app/'.$this->oldAttributes['image']));
		}
		if ($this->image && substr($this->image,0,1) !== '/'){
			$new_file = '/userdata/blog/'.time().'.jpg';
			if (copy($this->image, Yii::getAlias('@app/'.$new_file))) {
				$this->addError('image', 'Unable to download image');
				$this->image = $new_file;
				return parent::beforeSave($insert);
			} else {
				return false;
			}
		}
		return parent::beforeSave($insert);
	}

	public function getImagePreview()
	{
		$filename = $this->image.self::PREVIEW_SUFFIX;
		if (!file_exists(Yii::getAlias('@app/web/'.$filename))) {
			$this->createImagePreview(self::PREVIEW_MAX_SIZE);
		}
		return $filename;
	}

	private function createImagePreview($max_size)
	{
		if (!is_file(Yii::getAlias('@app/web/'.$this->image))) {
			return;
		}
		$filename = Yii::getAlias('@app/web/'.$this->image);
		list($width, $height) = getimagesize($filename);

		if ($width > $height) {
			$new_width = $max_size;
			$new_height = $height * ($max_size / $width);
		} else {
			$new_width = $width * ($max_size / $height);
			$new_height = $max_size;
		}

		$image = imagecreatefromjpeg($filename);
		$preview = imagecreatetruecolor($new_width, $new_height);
		imagecopyresized(
			$preview, $image,
			0, 0,
			0, 0,
			$new_width, $new_height,
			$width, $height
		);

		imagejpeg($preview, $filename.self::PREVIEW_SUFFIX);
		imagedestroy($preview);
		imagedestroy($image);
	}
}
