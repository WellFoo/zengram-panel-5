<?php namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%usage_stats_detail}}".
 *
 * @property string $date
 * @property integer $user_id
 * @property integer $used
 */
class UsageStatsDetail extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%usage_stats_detail}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['date'], 'safe'],
			[['user_id', 'used'], 'integer'],
			[['date', 'user_id'], 'unique', 'targetAttribute' => ['date', 'user_id'], 'message' => 'The combination of Date and User ID has already been taken.'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'date' => 'Date',
			'user_id' => 'User ID',
			'used' => 'Used',
		];
	}

	public static function fillData($date = null)
	{
		if ($date === null) {
			$date = date('Y-m-d');
		}
		\Yii::$app->db->createCommand(sprintf(
			'INSERT INTO usage_stats_detail 
			SELECT
				\'%s\',
				u.id,
				(((CASE WHEN u.regdate >= \'2016-10-03\' THEN 5 ELSE 3 END) + coalesce(sum(p.value), 0)) * 86400) - u.balance
			FROM users u
			LEFT JOIN invoice i ON i.user_id = u.id
			LEFT JOIN prices p ON p.id = i.price_id
			GROUP BY u.id
			ON CONFLICT (date, user_id) DO NOTHING',
			$date
		))->execute();
	}
}
