<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
/**
 * This is the model class for table "Users_Info".
 *
 * @property integer $user_id
 * @property string  $try_add
 * @property string  $try_start
 * @property string  $was_checkpoint
 * @property string  $was_wrongpass
 * @property string  $deleted_account
 * @property integer $refund_policy_accepted
 * @property integer $refund_policy_accept_date
 * @property string  $country
 * @property string  $refferer
 * @property string  $cheater
 * @property string  $service
 */

class UsersInfo extends ActiveRecord
{
	public static function tableName()
	{
		return 'users_info';
	}

	public static function primaryKey()
	{
		return ['user_id'];
	}

	public function rules()
	{
		return [
			[['user_id'], 'required'],
			[['refferer'], 'string'],
			[['refferer'], 'default', 'value' => ''],
			[['service'], 'string'],
			[['service'], 'default', 'value' => 'RU'],
			[['try_add','try_start','was_checkpoint','was_wrongpass','deleted_account','cheater'], 'default',
					'value' => 0],
			//[['country'], 'default', 'value' => Users::locationInfo('country')]
			[['country'], 'default', 'value' => '']
		];
	}

	public function attributeLabels()
	{
		return [
				'user_id'                   => Yii::t('app', 'ID'),
				'try_add'                   => Yii::t('app', 'Пытался добавить аккаунт'),
				'try_start'                 => Yii::t('app', 'Пытался запустить аккаунт'),
				'was_checkpoint'            => Yii::t('app', 'Был чекпоинт у аккаунта'),
				'was_wrongpass'             => Yii::t('app', 'Был неверный пароль у аккаунта'),
				'deleted_account'           => Yii::t('app', 'Удалял аккаунт'),
				'refund_policy_accept_date' => Yii::t('app', 'Правила оплаты приняты'),
				'country'                   => Yii::t('app', 'Страна'),
				'service'                   => Yii::t('app', 'Сервис'),
//				'cheater'                   => Yii::t('app', 'Пытался повторно бесплатно запускать аккаунт'),
		];
	}

	public function getRefund_policy_accept_date()
	{
		if ($this->refund_policy_accepted) {
			return Yii::$app->formatter->asDate($this->refund_policy_accepted);
		}
		return 'нет';
	}
}