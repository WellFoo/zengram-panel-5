<?php namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "fb_geotags".
 *
 * @property integer $id
 * @property integer $fb_id
 * @property string  $name
 * @property string  $coords
 * @property string  $address
 */
class FbGeotag extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'fb_geotags';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['fb_id'], 'integer'],
			[['coords'], 'string'],
			[['name', 'address'], 'string', 'max' => 255],
			[['fb_id'], 'unique'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'     => Yii::t('app', 'ID'),
			'fb_id'  => Yii::t('app', 'Fb ID'),
			'name'   => Yii::t('app', 'Name'),
			'coords' => Yii::t('app', 'Coords'),
		];
	}

	public function getCoordsArray()
	{
		list($lat, $lng) = explode(',', trim($this->coords, '()'));
		return [
			'lat' => floatval($lat),
			'lng' => floatval($lng),
		];
	}
}
