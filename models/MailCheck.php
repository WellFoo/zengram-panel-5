<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "mail_check".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string  $user_email
 * @property integer $mx_checked
 * @property integer $undeliver_report
 */
class MailCheck extends ActiveRecord
{

	const MX_NO_CHECKED    = 0;
	const MX_CHECKED_FALSE = 1;
	const MX_CHECKED_TRUE  = 2;
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'mail_check';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'user_id', 'mx_checked', 'undeliver_report'], 'integer'],
			[['user_email'], 'string'],
		];
	}
}
