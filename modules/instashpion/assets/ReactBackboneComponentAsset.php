<?php namespace app\modules\instashpion\assets;

use yii\web\AssetBundle;

class ReactBackboneComponentAsset extends AssetBundle
{
	public $js = [
		'js/lib/backbone-react-component.min.js'
	];
	public $depends = [
		'app\modules\instashpion\assets\ReactAsset',
		'app\modules\instashpion\assets\BackboneAsset',
	];
}