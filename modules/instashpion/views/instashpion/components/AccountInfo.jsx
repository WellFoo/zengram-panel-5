var AccountInfo = React.createClass({
    getInitialState() {
        return {
            authMessage: null,
            accountCheckLoading: false,
            showModal: false,
            showMaxFollowsErrorModal: true,
            showPayModal: false
        }
    },

    mixins: [
        Backbone.React.Component.mixin
    ],

    componentWillMount () {
        this.on(this, {
            models: {
                store: app,
                analize: app.get('analize'),
                model: this.props.model
            },
        });
    },

    componentWillUnmount () {
        this.off(this);
    },
    shouldComponentUpdate(nextProps, nextState) {
        console.log(nextState.store.analize.get('id'), this.state.analize.id);

        if (nextState.store.analize.get('id') != this.state.analize.id) {
            this.off(this);

            console.log('Обновили инфо юзера');

            this.on(this, {
                models: {
                    store: app,
                    analize: app.get('analize'),
                    model: this.props.model
                },
            });
        }

        return true;
    },

    getUser() {
        return this.props.model;
    },

    analize() {
        window.store.set('press', true);
        app.get('analize').start();
    },

    reanalize() {
        window.store.set('press', true);

        app.get('analize').start();
    },

    enter() {
        $('#login-modal').modal('show');
    },

    /*
     * Метод добавления аккаунта для обработки приватного пользователя
     */
    addAccount(e) {
        e.preventDefault();
        e.stopPropagation();

        var self = this;

        self.setState({
            authMessage: null,
            accountCheckLoading: true
        });

        $.post('/instashpion/ajax/check-account', {
            instagram_id:   this.getUser().get('instagram_id'),
            login:          this.refs.login.value,
            password:       this.refs.password.value

        }, function (response) {
            if (response.status == 'ok') {
                /*
                 * Нет доступа к страницу жерты
                 */
                if (!response.user.following) {
                    self.setState({authMessage: 'К сожалению указанный Вами аккаунт не имеет доступа к странице жертвы. Возможно другой?'});
                } else {
                    self.props.onAddAccount(response.user, self.refs.login.value, self.refs.password.value);
                }

                self.setState({accountCheckLoading: false});

            } else {
                /*
                 * Ошибка при добавлении аккаунта
                 */
                var message = 'Не удалось добавить аккаунт. Попробуйте позже.';

                if (response.message == 'Authentication failed') {
                    /*
                     * Неправильный логин или пароль
                     */
                    message = 'Неправильный логин или пароль';
                }
                self.setState({authMessage: message});
            }

            // Проверка аккаунта завершена
            self.setState({accountCheckLoading: false});
        });
    },

    addAccountForm() {
        return (
            <form className="form-horizontal add-account-form" style={{width: "400px", margin: '0px auto;'}} onSubmit={this.addAccount}>
                <p className="alert alert-danger" style={{display: (this.state.authMessage === null) ? "none" : "block"}}>
                    {this.state.authMessage}
                </p>

                <div className="form-group required">
                    <div className="col-lg-12">
                        <input type="text" style = {{width: '400px', height: '54px', padding: '6px 12px', fontSize: '18px'}} className="form-control" ref = 'login' placeholder={"Логин Instagram"} />
                    </div>
                </div>
                <div className="form-group required">
                    <div className="col-lg-12">
                        <input type="password" style = {{width: '400px', height: '54px', padding: '6px 12px', fontSize: '18px'}} className="form-control" ref = 'password' placeholder={"Пароль"} />
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-lg-12 col-md-12">
                        <button type="submit" className="btn btn-danger btn-lg" name="login-button" style = {{height: '65px'}}>
                            <Spin label = "Добавить аккаунт" loading = {this.state.accountCheckLoading} size = "20px" color = "#fff" />
                        </button>
                    </div>
                </div>
            </form>
        );
    },

    /*
     * Рендер пользователя при просмотре новых подписок
     */
    renderNewFollow(user) {
        return (
            <tr >
                <td style = {{width: '200px', padding: '5px 10px'}}>
                    <a href = {'https://instagram.com/' + user.get('username')} target ={'_blank'}>
                        <img src={user.get('avatar')} style = {{width: '85px'}} />
                    </a>
                </td>
                <td style = {{width: '580px', padding: '5px 0px', verticalAlign: 'middle'}}>
                    <a href = {'https://instagram.com/' + user.get('username')} target ={'_blank'}>
                        @{user.get('username')}
                    </a>
                </td>
            </tr>
        );
    },


    /*
     * Модалка с формой добавления аккаунта для просмотра приватного пользователя
     */
    renderPrivateUserModal() {
        var self = this;
        if (self.state.model.is_private && self.state.model.following_count < 1000 && !self.state.model.loadedInfo) {
            if (self.state.model.showPrivateModal) {
                return (
                    <div>
                        <div className = "modal-overlay" onClick = {() => { self.props.model.set({showPrivateModal: false}); }}></div>
                        <div className="modal-dialog modal-reg">
                            <div className="modal-content" style = {{padding: '20px 99px 40px 99px'}}>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onClick = {() => { self.props.model.set({showPrivateModal: false}); }}>×</span></button>
                                <div className="modal-body" style = {{textAlign: 'center'}}>
                                    <h2 className="modal-title" style = {{lineHeight: '1.4',fontSize: '26px',color: '#ff6666'}}>
                                        Приватный аккаунт
                                    </h2>

                                    <p style = {{color: '#ff6666',margin: '25px auto 45px auto',fontSize: '18px',lineHeight: '25px',}}>
                                        К сожалению данный аккаунт приватный и его анализ невозможен.
                                        Если Вы подписаны на данный аккаунт, добавьте свой аккаунт на этом сервисе и запустите работу
                                    </p>

                                    {self.addAccountForm()}
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
        }
    },

    /*
     * Рендер модалки для просмотра новых пользователей
     */
    renderFollowsModal () {
        var self = this;
        if (this.state.showModal) {
            return (
                <div>
                    <div className="modal-overlay" onClick={() => { self.setState({showModal: false}); }}></div>
                    <div className="modal-dialog modal-media">
                        <div className="modal-content">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true" onClick={() => { self.setState({showModal: false}); }}>×</span>
                            </button>
                            <div className="modal-body">
                                <h2 className="modal-title" style={{textAlign: 'center', width: '100%'}}>Новые подписки
                                </h2>

                                <div className="body-modal" style = {{maxHeight: '450px', minHeight: '450px'}}>
                                    <table>
                                        <tr>
                                            <td>Пользователь</td>
                                            <td>Имя</td>
                                        </tr>
                                        {app.get('analize').get('new_follows').map(this.renderNewFollow)}
                                    </table>
                                </div>
                                <br />
                                <center>
                                    <a className="btn btn-danger"
                                       onClick={() => { self.setState({showModal: false}); }}>Закрыть</a>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        return null;
    },

	/*
	* Модалка, оповещающая пользователя о необходимости регистрации или пополнения баланса
	*/
    renderBalanceModal() {
        var self = this;

        if (this.state.analize.showBalanceModal === true) {
            return (
                <div>
                    <div className="modal-overlay" onClick={() => { app.get('analize').set('showBalanceModal', false) }}></div>
                    <div className="modal-dialog modal-media">
                        <div className="modal-content">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" onClick={() => { app.get('analize').set('showBalanceModal', false) }}>×</span>
                            </button>
                            <div className="modal-body">
                                <center>
                                    <div className = 'maxfollows-message'>
                                        <h2>Уважаемый клиент!</h2>
                                        {(() => {
                                            return (
                                                (isGuest) ?
	                                                <div style={{
		                                                display: 'inlineBlock',
		                                                width: '600px',
		                                                margin: '0px auto'
	                                                }}>
		                                                В настоящий момент вы используете сервис в режиме ограниченной
		                                                функциональности, поэтому не можете видеть некоторые строки
		                                                рейтинга и не можете анализировать более 3 аккаунтов
		                                                одновременно. Для получения возможности полного просмотра
		                                                рейтинга и возможности расширения пакета Вам необходимо
		                                                пополнить баланс!<br />
		                                                Кроме того, Вы можете использовать функционал Instashpion
		                                                БЕСПЛАТНО если являетесь клиентом сервиса Zengram.ru, совершали
		                                                там оплату и работа по Вашему аккаунту идет более 12 часов!
	                                                </div> :
	                                                <div style={{
		                                                display: 'inlineBlock',
		                                                width: '600px',
		                                                margin: '0px auto'
	                                                }}>
		                                                Для возможности добавления новых аккаунтов Вам необходимо приобрести еще один пакет
	                                                </div>
                                            );
                                        })()}

	                                    <div style = {{width: '100%'}}>

		                                    <a className="btn btn-success btn-lg" style={{
			                                    padding: '10px 50px',
			                                    marginTop: '40px',
			                                    marginRight: '20px'
		                                    }} onClick={() => { app.get('analize').set('showBalanceModal', false); app.get('analize').set({showPayModal: true}) }}>Оплата</a>

		                                    {(() => {
			                                    return (
				                                    (isUser) ?
					                                    <a className="btn btn-success btn-lg" style={{
						                                    padding: '10px 50px',
						                                    marginTop: '40px'
					                                    }} onClick={() => { app.get('analize').set('showBalanceModal', false); $('#regModal').modal('show'); }}>Регистрация</a> :
					                                    <a className="btn btn-danger btn-lg" style={{
						                                    padding: '10px 50px',
						                                    marginTop: '40px'
					                                    }} onClick={() => { app.get('analize').set('showBalanceModal', false) }}>Отмена</a>
			                                    );
		                                    })()}
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    },

    /*
     * Рендер модалки с сообщением об ограничении (на период бета тестирования)
     */
    renderMaxFollowsModal() {
        var self = this;

        if (app.get('user').get('following_count') >= 1000 && app.get('user').get('showMaxFollowsModal')) {
            return (
                <div>
                    <div className="modal-overlay" onClick={() => { self.props.model.set({showMaxFollowsModal: false}); }}></div>
                    <div className="modal-dialog modal-media">
                        <div className="modal-content">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true" onClick={() => { self.props.model.set({showMaxFollowsModal: false}); }}>×</span>
                            </button>
                            <div className="modal-body">
                                <center>
                                    <div className = 'maxfollows-message'>
                                        <h2>Уважаемый клиент!</h2>
                                        <br /><br />

                                        В настоящее время ресурс находится на стадии <br />
                                        бета-тестирования и работает в режиме<br />
                                        ограниченного функционала. <br />
                                        Сервис обрабатывает аккаунты с количеством<br />
                                        подписок не более 1000.

                                        <div style = {{width: '100%'}}>
                                            <a className="btn btn-success btn-lg" style={{
                                                padding: '10px 50px',
                                                marginTop: '40px'
                                            }} onClick={() => { self.props.model.set({showMaxFollowsModal: false}); }}>ОК</a>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    },

    /*
     * Основной шаблон
     * Карточка пользователя
     */
    render() {
        var self = this;
        
        var user = this.getUser();
        
        return (
            <div>
                <div className="data__wrap">

                    <section className="data" >
                        <div className="data__target-wrap">
                            <div className="data__target">
                                <div className="data__target-item _user">
                                    <a className="username" href = {"https://instagram.com/" + user.get('username') + '/'} target = '_blank'>
                                        <img src={user.get('avatar')} alt="Анализируемый пользователь" className="data__target-image" style = {{width: '87px'}} />
                                    </a>
                                    <p className="data__target-name">{user.get('username')}</p>
                                    <a href="#" className="data__target-btn-del" onClick = {this.props.onRemove}>(удалить)</a>
                                </div>
                                <div className="data__target-item _photo">
                                    <p className="data__target-property">Фото</p>
                                    <p className="data__target-value">{((app.get('user').isLoaded()) ? (<Spin label = " " loading = {true} size = "20px" color = "#666" />) : user.get('media_count'))}</p>
                                </div>
                                <div className="data__target-item _subscribers">
                                    <p className="data__target-property">Подписчики</p>
                                    <p className="data__target-value">{user.get('follower_count')}</p>
                                </div>
                                <div className="data__target-item _subscription">
                                    <p className="data__target-property">Подписки</p>
                                    <p className="data__target-value">
                                        {(() => {
                                            /*
                                             * Рендеринга количества подписок и новых подписок (если таки есть)
                                             */
                                            if ((app.get('user').isLoaded())) {
                                                // Загрузка
                                                return (<Spin label = " " loading = {true} size = "20px" color = "#666" />);
                                            } else {
                                                // Отображение
                                                return (
                                                    <span>
                                                        {user.get('following_count')}
                                                        {(() => {
                                                            if (_.size(app.get('analize').get('new_follows')) > 0) {
                                                                return ( <span className="incrementationValue" onClick = {(() => { self.setState({showModal: true}) })}> +{_.size(app.get('analize').get('new_follows'))}</span>);
                                                            }
                                                        })()}
                                                    </span>
                                                );
                                            }
                                        })()}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </section>
                    {(() => {
                        /*
                         * Нижняя часть карточки пользователя
                         */
	                    var following_count = app.get('user').get('following_count');
	                    //this.state.model.isLoaded
                        if (app.get('user').isLoaded() || following_count >= 1000 || following_count === null) {
                            // Если идет процесс отправки
                            return null;
                        }

                        if (self.state.model.is_private) {
                            /*
                             * Аккаунт приватный
                             */
                            return (
                                <div className = "private-account-message" style = {{textAlign: 'center', fontSize: '18px', lineHeight: '25px'}}>
                                    К сожалению данный аккаунт приватный и его анализ невозможен.
                                    Если Вы подписаны на данный аккаунт, добавьте свой аккаунт на этом сервисе и запустите работу
                                    <br /><br />
                                    <a className = "btn btn-danger btn-lg" onClick = {() => { self.props.model.set({showPrivateModal: true}); }}>Добавить аккаунт</a>
                                </div>
                            );

                        } else {
                            /*
                             * Аккаунт не приватный
                             */
                            if (((!self.state.analize.updated && self.state.analize.process && app.get('isBrowser')))) {
                                // Ничего не отображаем если идет инициализация модуля после перезагрузки страницы
                                return null;
                            }

                            if (app.get('analize').get('id') === null) {
                                // Кнопка анализа
                                // Для первой инициализации

                                return (
                                    <div className = "btn btn-lg btn-success btn-analize" onClick={self.analize} disabled = {self.props.process || app.get('user').isLoaded()}>
                                        <Spin label = "Анализировать" loading = {app.get('analize').get('process')} size = "20px" color = "#fff" />
                                    </div>
                                );

                            } else {
                                // Кнопка анализа
                                // Для повторной инициализации

                                console.log(this.state.analize, app.get('analize'));

                                if (this.state.analize.complete && app.get('analize').get('result').size() > 0) {
                                    return (
                                        <div className = "btn btn-lg btn-success btn-analize" onClick={self.reanalize} disabled = {self.props.process || this.state.analize.process}>
                                            <Spin label = "Повторить анализ" loading = {self.props.process || this.state.analize.process} size = "20px" color = "#fff" />
                                        </div>
                                    );
                                }
                            }
                        }
                    })()}
                </div>

                {this.renderPrivateUserModal()}
                {this.renderFollowsModal()}
                {this.renderBalanceModal()}
                {this.renderMaxFollowsModal()}
                
            </div>
        );
    }
});
