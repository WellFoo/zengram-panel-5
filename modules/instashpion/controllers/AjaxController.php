<?php

namespace app\modules\instashpion\controllers;

use app\components\Controller;
use app\models\ShpionBalance;
use app\models\Users;
use app\modules\instashpion\models\Analize;
use app\modules\instashpion\models\Victim;
use MongoDB\Client;
use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\web\Response;
use app\modules\instashpion\components\Api;

class AjaxController extends Controller
{
	use Api;

	private $prev_result = [];

	public function actionTest() {
		print 'hello';
	}

	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

	public function actionSearch()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$login = $this->_checkSearchQuery(Yii::$app->request->post('query'));

		try {

			$result = $this->request('searchUser', [
				'query' => $login
			])['users'];

		} catch (Exception $e) {
			return [
				'status' => 'error',
				'message' => $e->getMessage()
			];
		}

		if (!is_array($result)){
			return [
				'status' => 'error',
				'message' => 'response does not array',
				'response' => print_r($result)
			];
		}
		
		$result = array_slice($result, 0, 5);

		return [
			'status' => 'ok',
			'users' => $this->_normalizeUsers($result)
		];
	}

	public function actionUserInfo()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$instagram_id = Yii::$app->request->post('instagram_id');

		try {
			$result = $this->request('getUserInfo', [
				'instagram_id' => $instagram_id
			]);
		} catch (Exception $e) {
			return [
				'status' => 'error',
				'message' => $e->getMessage()
			];
		}

		if (empty($result['pk'])) {
			return [
				'status' => 'error',
				'message' => 'response does not array',
				'response' => print_r($result)
			];
		}

		return [
			'status' => 'ok',
			'user' => $this->_normalizeUser($result)
		];
	}

	/**
	 * Запускает анализ инстаграм аккаунта
	 *
	 * - Создает list для формирования списка подписок и указывает страницу возврата при завершении формирования списка
	 * - Создает задачу для анализа жертвы
	 * - Отдает информацию на клиент для дальнейшего опроса
	 *
	 * @return array
	 */
	public function actionAnalize()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$instagram_id = Yii::$app->request->post('instagram_id');
		$handler = Yii::$app->request->post('handlerUser', null);

		try {
			$result = $this->request('getUserInfo', [
				'instagram_id' => $instagram_id
			]);

			// Нормализация пользовательских данных
			$user = $this->_normalizeUser($result);

			/*
			 * Создаем новую жертву или обновляем информацию если жертва есть в базе данных
			 */
			$this->setVictimInfo($instagram_id, $user);

			$victim = Victim::findOne(['instagram_id' => $instagram_id]);

			/*
			 * Делаем запрос к API на формирование списка подписок
			 */
			$data = [
				'instagram_id' 			=> $instagram_id,
				'completed_callback' 	=> Yii::$app->request->getHostInfo().'/instashpion/events/completed-update-list'
			];

			if ($handler !== null) {
				$data['handler'] = [
					'login'     => $handler['login'],
					'password'  => $handler['password'],
					'ip'        => Yii::$app->request->getUserIP(),
				];
			}

			$list = $this->request('getFollowsList', $data);

			/*
			 * Создаем запись об анализе жертвы
			 */
			$analize = new Analize();

			$analize->victim_id             = $victim->id;
			$analize->analize_list_status   = Analize::STATUS_LIST_STOP;
			$analize->analize_likes_status   = 0;
			$analize->list_id               = $list['list_id'];

			if ($id = Yii::$app->user->id) {
				$analize->user_id   = $id;
			}

			if (!\Yii::$app->user->isGuest)
			{
				$balance = ShpionBalance::find()
					->select(['user_id', 'id', 'date', new Expression('array_to_json(victims) as victims')])
					->where('user_id = :user_id and date > :interval and ((array_length(victims, 1) < 3 or array_length(victims, 1) is null))', [
						':user_id' => \Yii::$app->user->id,
						':interval' => time() - MainController::BONUS_TIME
					])
					->orderBy('date')
					->one();

				$victims = [];

				if (!empty($balance->victims)) {
					$victims = json_decode($balance->victims);
				}

				if (!is_array($victims)) {
					$victims = [];
				}

				$victims[] = $victim->id;

				if (count($victims) > 1) {
					$balance->victims = '{' . implode(',', $victims) . '}';
				} else if (count($victims) == 1) {
					$balance->victims = '{' . $victim->id . '}';
				} else {
					$balance->victims = '{}';
				}

				$balance->save();

			} else {
				\Yii::$app->response->cookies->add(new \yii\web\Cookie([
					'name' => 'shpion_balance',
					'value' => \Yii::$app->request->cookies->get('shpion_balance')->value - 1
				]));
			}

			if (!$analize->save()) {
				throw new Exception(array_values($analize->getFirstErrors())[0]);
			}

			/*
			 * Отправляем результат клиенту
			 */
			return [
				'status'                => 'ok',
				'victim_id'             => $victim->id,
				'analize_id'            => $analize->id,
				'list_id'               => $analize->list_id,
				'analize_list_status'   => Analize::STATUS_LIST_STOP
			];

		} catch (Exception $e) {
			return [
				'status'    => 'error',
				'message'   => $e->getMessage()
			];
		}
	}

	public function actionRestoreVictim($instagram_id) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$victim = Victim::find()->where(['instagram_id' => $instagram_id])->one();

		$balance = ShpionBalance::find()
			->select(['user_id', 'id', 'date', new Expression('array_to_json(victims) as victims')])
			->where('user_id = :user_id and date > :interval and ((array_length(victims, 1) < 3 or array_length(victims, 1) is null))', [
				':user_id' => \Yii::$app->user->id,
				':interval' => time() - MainController::BONUS_TIME
			])
			->orderBy(['date' => SORT_ASC])
			->one();

		$victims = [];

		if (!empty($balance->victims)) {
			$victims = json_decode($balance->victims);
		}

		if (!is_array($victims)) {
			$victims = [];
		}

		$victims[] = $victim->id;

		if (count($victims) > 1) {
			$balance->victims = '{' . implode(',', $victims) . '}';
		} else if (count($victims) == 1) {
			$balance->victims = '{' . $victim->id . '}';
		} else {
			$balance->victims = '{}';
		}

		// Удаление старых анализов жертвы из сессии пользователя
		\Yii::$app
			->db
			->createCommand("UPDATE shpion_balance SET victims = array_remove(victims, :victim) WHERE user_id = :user_id and array_position(victims, :victim) > 0")
			->bindValues([
				':victim' => $victim->id,
				':user_id' => \Yii::$app->user->id,
			])
			->execute();

		// Обновление истории анализа жертв
		if ($balance->save()) {
			return [
				'status' => 'ok'
			];
		} else {
			return [
				'status'    => 'error',
				'message'   => (end($balance->getFirstErrors()))
			];
		}
	}

	public function actionReanalize() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$instagram_id = Yii::$app->request->post('instagram_id');
		$analize_id = Yii::$app->request->post('analize_id');

		$handler = Yii::$app->request->post('handlerUser', null);

		try {
			$result = $this->request('getUserInfo', [
				'instagram_id' => $instagram_id
			]);

			// Нормализация пользовательских данных
			$user = $this->_normalizeUser($result);

			/*
			 * Создаем новую жертву или обновляем информацию если жертва есть в базе данных
			 */
			$this->setVictimInfo($instagram_id, $user);

			$victim = Victim::findOne(['instagram_id' => $instagram_id]);
			$analize = Analize::findOne(['id' => $analize_id]);

			/*
			 * Делаем запрос к API на формирование списка подписок
			 */
			$data = [
				'instagram_id' 			=> $instagram_id,
				'completed_callback' 	=> Yii::$app->request->getHostInfo().'/instashpion/events/completed-reupdate-list',
				'list_id'               => $analize->list_id
			];

			if ($handler !== null) {
				$data['handler'] = [
					'login'     => $handler['login'],
					'password'  => $handler['password'],
					'ip'        => Yii::$app->request->getUserIP(),
				];
			}

			$list = $this->request('getFollowsList', $data);

			/*
			 * Создаем запись об анализе жертвы
			 */
			$analize = new Analize();

			$analize->victim_id             = $victim->id;
			$analize->prev_analize_id       = $analize_id;
			$analize->analize_list_status   = Analize::STATUS_LIST_STOP;
			$analize->analize_likes_status  = Analize::STATUS_LIST_STOP;
			$analize->list_id               = $list['list_id'];
			$analize->is_update             = 1;

			if ($id = Yii::$app->user->id) {
				$analize->user_id   = $id;
			}

			if (!$analize->save()) {
				throw new Exception(array_values($analize->getFirstErrors())[0]);
			}

			/*
			 * Отправляем резльтат клиенту
			 */
			return [
				'status'                => 'ok',
				'victim_id'             => $victim->id,
				'analize_id'            => $analize->id,
				'list_id'               => $analize->list_id,
				'analize_list_status'   => Analize::STATUS_LIST_STOP
			];

		} catch (Exception $e) {
			return [
				'status'    => 'error',
				'message'   => $e->getMessage()
			];
		}
	}


	public function isNewMedia($user, $media)
	{
		if (!empty($this->prev_result) && !empty($this->prev_result[$user]) && count((array) $this->prev_result[$user]['media']) > 0) {
			$ids = [];
			foreach ($this->prev_result[$user]['media'] as $m) {
				$ids[] = $m['id'];
			}

			if (in_array($media, $ids)) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	public function isNewRelike($user, $media)
	{
		if (empty($this->prev_result) or empty($this->prev_result[$user]) or !is_array((array) $this->prev_result[$user]['relikes'])) {
			return false;
		}
		$ids = [];
		foreach ($this->prev_result[$user]['relikes'] as $m) {
			$ids[] = $m['id'];
		}

		return (!in_array($media, $ids));
	}

	public function actionStop() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$analize_id = Yii::$app->request->post('id');
		$analize    = Analize::findOne(['id' => $analize_id]);

		if (!$analize) {
			return [
				'status' => 'error',
				'message' => 'Analize not found'
			];
		}

		$db = (new Client(\Yii::$app->params['mongo']))->api;
		$tasks = $db->tasks;

		$task = $tasks->updateOne(['_id' => new \MongoDB\BSON\ObjectID($analize->task_id)], ['$set' => ['status' => 9]]);

		if ($task) {
			$analize->analize_list_status = 2;
			$analize->analize_likes_status = 2;

			$analize->save();

			return [
				'status' => 'ok'
			];
		}
	}

	public function actionCheckAnalizeProgress()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$analize_id = Yii::$app->request->post('analize_id');
		$analize    = Analize::findOne(['id' => $analize_id]);
		$new_follows = [];

		if (!$analize) {
			return [
				'status' => 'error',
				'message' => 'Analize not found'
			];
		}

		$db = (new Client(\Yii::$app->params['mongo']))->api;
		$lists = $db->lists;
		$tasks = $db->tasks;

		$relikes_progress = 0;
		$rated_progress = 0;

		$list = $lists->findOne(['_id' => new \MongoDB\BSON\ObjectID($analize->list_id)]);
		$result = [];
		$task = null;

		/*
		 * Прогресс анализа списка
		 */
		if($analize->analize_list_status == Analize::STATUS_LIST_COMPLETE) {
			$follows_progress = 100;
		} else {
			$done = count($list['follows']);
			$total = (int) $list['follows_count'];

			$follows_progress = $total > 0 ? (int) ($done / ($total / 100)) : 0;
		}

		$task = $tasks->findOne(['_id' => new \MongoDB\BSON\ObjectID($analize->task_id)]);

		if (!empty($task['prev_result'])) {
			$this->prev_result = (array) $task['prev_result'];
		}

		/*
		 * Прогресс анализа медиа
		 */
		if($analize->analize_likes_status == 2 || $task['status'] == 2) {
			$likes_progress = 100;
			$relikes_progress = 100;
			$rated_progress = 100;
		} else {
			if (!empty($task['result']) && count($task['result']) > 0) {
				$done = count($task['result']);
			} else {
				$done = 0;
			}

			$total = (int) count($list['follows']);

			$likes_progress = $total > 0 ? (int) ($done / ($total / 100)) : 0;
		}

		/*
		 * Формирование результата
		 */
		if ($task !== null && !empty($task['result']))
		{
			$items = $task['result'];
			$relikers = [];

			foreach ($items as $item)
			{
				if (count($item['relikes']) > 0 and count($item['media']) < 1) {
					$relikers[] = $item;
				}

				if (count($item['media']) > 0 || count($item['relikes']) > 0) {
					$result[$item['id']] = $item;
					$medias = (array) $item['media'];

					$result[$item['id']]['newMedia'] = [];

					foreach($medias as $key => $m)
					{
						if ($this->isNewMedia($item['id'], $m['id']))
						{
							$result[$item['id']]['newMedia'][] = $m;
						}
					}

					foreach($item['relikes'] as $key => $m)
					{
						if ($this->isNewRelike($item['id'], $m['id']))
						{
							$result[$item['id']]['newRelikes'][] = $m;
						}
					}

					if ($item['relikes'] && count($item['relikes']) > 0) {
						$result[$item['id']]['relikes'] = array_values( (array) $item['relikes']);
					}
				}
			}
		}

		if (!empty($list['mediaLikers']) && count((array) $list['mediaLikers']) > 0) {
			$result = array_merge($result, (array) $list['mediaLikers']);
		}

		uasort($result, function ($last, $current) {
			if ((count($last['media'])) < (count($current['media']))) {
				return 1;
			} else if ((count($last['media'])) > (count($current['media']))) {
				return -1;
			}

			return 0;
		});

		if (!empty($list['new_follows'])) {
			$new_follows = $this->_normalizeUsers((array) $list['new_follows']);
		}

		$exists = [];

		foreach ($result as $key => $res) {
			if (empty($res['id'])) {
				unset($result[$key]);
				continue;
			}

			if (in_array($res['id'], $exists)) {
				unset($result[$key]);
				continue;
			}

			$exists[] = $res['id'];
		}

		$result = array_slice(array_values($result), 0, 80);

		return [
			'status' => 'ok',
			
			'progress' => [
				'follows'   => $follows_progress,
				'likes'     => $likes_progress,
				'relikes'   => $relikes_progress,
				'rated'     => $rated_progress
			],
			'result' => $result,
			'new_follows' => $new_follows
		];
	}

	public function actionCheckAccount()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$instagram_id   = Yii::$app->request->post('instagram_id');
		$login          = Yii::$app->request->post('login');
		$password       = Yii::$app->request->post('password');

		try {
			$result = $this->request('checkUserRelation', [
				'instagram_id'  => $instagram_id,
				'handler'       => [
					'login'     => $login,
					'password'  => $password,
					'ip'        => Yii::$app->request->getUserIP()
				]
			]);

			$user = $this->_normalizeUser($result['handler_account']);
			$user['following'] = $result['relation']['following'];

			return [
				'status' => 'ok',
				'user' => $user
			];
		} catch (Exception $e) {
			return [
				'status'    => 'error',
				'message'   => $e->getMessage()
			];
		}
	}

	private function setVictimInfo($instagram_id, $user)
	{
		$victim = Victim::findOne(['instagram_id' => $instagram_id]);

		if (is_null($victim)) {
			$victim = new Victim();
		}

		$victim->instagram_id   = $instagram_id;
		$victim->username 		= $user['username'];
		$victim->name           = ''; //$user['name'];
		$victim->avatar         = $user['avatar'];

		$victim->follower_count = $user['follower_count'];
		$victim->following_count = $user['following_count'];
		$victim->media_count = $user['media_count'];

		if (!$victim->validate()) {
			throw new Exception(array_values($victim->getFirstErrors())[0]);
		}

		return $victim->save();
	}

	private function _normalizeUsers($users)
	{
		return array_map([$this, '_normalizeUser'], array_values((array) $users));
	}

	private function _normalizeUser($data)
	{
		if (is_null($data)) {
			Yii::error($data);
			return false;
		}

		return [
			'instagram_id'   	=> $data['pk'],
			'username'       	=> $data['username'],
			'name'       	 	=> trim($data['full_name']),
			'avatar'         	=> $data['profile_pic_url'],
			'follower_count' 	=> (!empty($data['follower_count'])) ? $data['follower_count'] : 0,
			'following_count'   => ((!empty($data['following_count'])) ? $data['following_count'] : 0),
			'media_count' 		=> ((!empty($data['media_count'])) ? $data['media_count'] : 0),
			'is_private'     	=> $data['is_private'],
		];
	}


	private function _checkSearchQuery($query)
	{
		$query = trim($query);

		if (substr($query, 0, 1) == '@')
		{
			return substr($query, 1, strlen($query));

		} elseif (substr($query, 0, 4) == 'http')
		{
			$parts = explode('/', $query);

			if (empty($parts[count($parts) - 1])) {
				return $parts[count($parts) - 2];
			}

			return $parts[count($parts) - 1];
		} else {
			return $query;
		}
	}
}
