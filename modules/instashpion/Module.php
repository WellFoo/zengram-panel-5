<?php
/*
 * Когда я начинал это писать, только Бог и я понимали, что я делаю
 * Сейчас остался только Бог.
 */
namespace app\modules\instashpion;

use app\models\Users;
use yii\web\ForbiddenHttpException;
use Yii;

class Module extends \yii\base\Module
{
	public $controllerNamespace = 'app\modules\instashpion\controllers';

	public $defaultRoute = 'main/index';
	public $layout = 'main';

	public function init()
	{
		parent::init();
	}
}
