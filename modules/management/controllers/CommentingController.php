<?php

namespace app\modules\management\controllers;

use app\modules\admin\models\CommentingProject;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;

class CommentingController extends Controller
{
	public function actionIndex()
	{
		$projects = CommentingProject::find()
			->asArray()
			->all();

		$projects = ArrayHelper::map($projects, 'id', 'name');

		$user = Yii::$app->request->get('user', 0);

		$date = Yii::$app->request->get('date', 'day');

		$date_to = strtotime(date('Y-m-d 23:59'));

		switch ($date) {
			case 'day':
			default:
				$date_from = $date_to - 86400 * 2;
				break;

			case 'week':
				$date_from = $date_to - 86400 * 8;
				break;

			case 'month':
				$date_from = $date_to - 86400 * 31;
				break;

			case 'range':
				$date_from = strtotime(Yii::$app->request->get('from'));
				$date_to = strtotime(Yii::$app->request->get('to'));
				break;
		}

		return $this->render('index', [
			'projects'  => $projects,
			'date'      => $date,
			'date_from' => $date_from,
			'date_to'   => $date_to,
			'user'      => $user,
		]);
	}
}
