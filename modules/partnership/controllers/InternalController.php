<?php namespace app\modules\partnership\controllers;

use app\models\Projects;
use app\modules\partnership\models\Partner;
use app\modules\partnership\models\Referal;
use Yii;
use yii\web\Controller;

class InternalController extends Controller
{
	public function actionRegister()
	{
		$secret = Yii::$app->request->get('secret');
		if ($secret !== Yii::$app->params['partnership']['internal_secret']) {
			return 'error';
		}

		$ref_key = Yii::$app->request->get('ref_key', 0);
		$user_id = Yii::$app->request->get('user_id', 0);

		/** @var Partner $partner */
		$partner = Partner::find()
			->where(['ref_key' => $ref_key])
			->one();

		if (is_null($partner)) {
			return 'error';
		}

		$referral = new Referal();
		$referral->partner_id = $partner->id;
		$referral->user_id = $user_id;

		return $referral->save() ? 'ok' : 'error';
	}

	public function actionPayment()
	{
		$secret = Yii::$app->request->get('secret');
		if ($secret !== Yii::$app->params['partnership']['internal_secret']) {
			return 'error';
		}

		$user_id = Yii::$app->request->get('user_id');
		$amount = Yii::$app->request->get('amount');
		$description = Yii::$app->request->get('description');

		/** @var Referal $referral */
		$referral = Referal::find()->where(['user_id' => $user_id])->one();
		if (is_null($referral)) {
			return 'error';
		}

		$data = [
			'referral' => $referral->id,
			'amount' => $amount,
			'description' => $description,
			'referral_level' => 1
		];

		$referral->partner->addBalance($data);

		if (!is_null($referral->partner->parent)) {
			$data['referral_level'] = 2;
			$referral->partner->parent->addBalance($data);
		}

		return 'ok';
	}
}