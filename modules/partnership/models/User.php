<?php namespace app\modules\partnership\models;

use app\models\Users;

class User extends Users implements ModuleUserInterface
{
	/*
	 * Важно! Имплементацию методов интерфейса делать в базовом классе
	 * Этот класс нужен только для простоты наследования и проверки имплементации
	 */
}