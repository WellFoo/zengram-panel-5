<?php

namespace app\modules\partnership\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "*partnership_managers".
 *
 * @property integer $id
 * @property integer $user_id
 * @property User    $user
 */
class Manager extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%partnership_managers}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id'], 'required'],
			[['user_id'], 'integer']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'User ID'),
		];
	}

	public function getUser()
	{
		return self::hasOne(User::className(), ['id' => 'user_id']);
	}
}
