<?php namespace app\modules\partnership\models;

use app\modules\partnership\Module;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pfx_partnership_banners".
 *
 * @property integer $id
 * @property string $code
 */
class Banner extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%partnership_banners}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['code'], 'required'],
			[['code'], 'string']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'   => Module::t('module', 'ID'),
			'code' => Module::t('module', 'Banner code'),
		];
	}
}
