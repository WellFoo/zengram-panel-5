<?php namespace app\modules\partnership\asset;

use yii\web\AssetBundle;
use yii\web\View;

class ModuleAsset extends AssetBundle
{
	public $sourcePath = '@app/modules/partnership/asset/data';

	public $jsOptions = [
		'position' => View::POS_HEAD
	];

	public $js = [
		'js/clipboard.min.js',
	];

	public $css = [
		'css/partner.css',
	];
}