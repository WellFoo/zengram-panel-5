<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\partnership\models\Partner $partner
 * @var \yii\data\ActiveDataProvider $data_provider
 * @var \app\modules\partnership\forms\RefundForm $refund_form
 * @var float $sum
 */
//$this->registerCssFile('/css/partner.css');
use app\models\Projects;
use app\modules\partnership\asset\ModuleAsset;
use app\modules\partnership\models\Refund;
use app\modules\partnership\Module;
use yii\helpers\Html;

ModuleAsset::register($this);

Yii::$app->formatter->numberFormatterSymbols = [NumberFormatter::CURRENCY_SYMBOL => '₽'];

$this->title = Module::t('module', 'Personal Area');
?>
<style>
	.js-btn,
	.js-btn:hover {
		text-decoration: none;
		border-bottom: 1px dashed;
		font-style: italic;
	}

	.container p {
		margin-bottom: 10px;
	}
</style>

<div class="row">
	<div class="col-md-8 col-xs-12">
		<div class="link-f clearfix">
			<a class="i2 active" href="#" style="background-color: #FFF;">Рефералы</a>
			<a class="i1" href="/partnership/cabinet/banners/" style="background-color: #E8E8E8;">Рекламные
				материалы</a>

		</div>

		<p class="green-text">
			* <?= Module::t('module', 'You get {percent}% of the amount of the order of your referrals.', ['percent' => Module::$settings->percent_1lvl * 100]) ?>
			<img src="/img/help.png" alt="" style="vertical-align: top;" data-original-title="Комиссия"
			     data-content="Вы получите комиссию в размере 5% от расходов привлечённого Вами клиента"
			     data-toggle="popover">
		</p>

		<p class="green-text">
			* <?= Module::t('module', 'Additional {percent}% of the amount of orders for each of your referral second level.', ['percent' => Module::$settings->percent_2lvl * 100]) ?>
			<img src="/img/help.png" alt="" style="vertical-align: top;" data-original-title="Комиссия второго уровню"
			     data-content="Вы получите комиссию в размере 1% доходов Ваших рефералов"
			     data-toggle="popover">
		</p>

	</div>
	<div class="pull-right">
		<table  style="width:250px; float:right; padding-right:0; margin-right:-20px;">
			<tr>
				<td style="padding-bottom:10px;"><?= Module::t('module', 'My current balance') ?></td>
				<td style="padding-bottom:10px;"><span class="green-text"><?= Yii::$app->formatter->asCurrency($partner->balance) ?></span></td>
			</tr>
			<tr>
				<td style="padding-bottom:10px;"><?= Module::t('module', 'My walet') ?></td>
				<td style="padding-bottom:10px;"><?= $partner->walet ?></td>
			</tr>
		</table>
		<?php

		if ($partner->balance > 0) {
			echo Html::a(Module::t('module', 'Cash out'), '#', [
				'class' => 'getmoney',
				'data-target' => '#refund_modal',
				'data-toggle' => 'modal',
			]);
		}

		?>
		<div class="clear"></div>

		<div class="text-center" style="width:270px">

			<?= Html::a(Module::t('module', 'Refunds histroy'), ['history'], ['class' => 'get_history']) ?><br>
			<?= Html::a(Module::t('module', 'Edit'), '#payment', ['class' => 'get_history', 'id' => 'paymentinfoChange']) ?>
		</div>
	</div>
</div>
<div class="row" style="margin-top:30px;">
	<div class="col pull-right">
		<div>
			<?= Module::t('module', 'Total orders on your referral link:') ?>
			<img src="/img/help.png" alt="" style="vertical-align: top;" data-original-title="Всего покупок"
			     data-content="Количество покупок, совершённых Вашими рефералами"
			     data-toggle="popover">

			<div class="clearfix visible-xs"></div>
			<div class="refs"><?= $partner->referalsPaymentsCount ?></div>
		</div>

		<div style="margin-top: 20px;">
			<?= Module::t('module', 'The total amount of orders on your referral link:') ?>
			<img src="/img/help.png" alt="" style="vertical-align: top;" data-original-title="Расходы рефералов"
			     data-content="Общая сумма расходов Ваших рефералов"
			     data-toggle="popover">

			<div class="refs" style="padding-left:20px;"><?= Yii::$app->formatter->asCurrency($partner->referalsPaymentsSum) ?></div>
		</div>
	</div>
</div>
<div class="row" style="margin-top: 20px;">
	<div class="table-responsive">
		<?php
		echo \yii\grid\GridView::widget([
			'dataProvider' => $data_provider,
			'showFooter' => true,
			'tableOptions' => ['class' => 'table table-striped table-bordered', 'id' => 'table-responsive'],
			'columns' => [
				[
					'attribute' => 'date',
					'format' => 'date',
					'label' => Module::t('module', 'Payment date'),
					'footerOptions' => ['style' => 'border-right: none;'],
				], [
					'label' => Module::t('module', 'Status')/*.
					'&nbsp;&nbsp;<a style="color:#333333;" href="/partnership/cabinet/index/?sortstatus=asc">
					<img src="/images/arrow-table.png" alt="" title="Сортировать по возрастанию">
					</a>'*/,
					'encodeLabel' => false,
					'value' => function ($item) use ($partner) {
						/** @var \app\modules\partnership\models\Payment $item */
						return Module::t('module', '{level}-level referral', ['level' => $item->referral_level]);
					},
					'footerOptions' => ['style' => 'border-left: none; border-right: none;'],
				], [
					'attribute' => 'description',
					'label' => 'URL реферального проекта'/*Module::t('module', 'Referral')/*.
					'&nbsp;&nbsp;<a style="color:#333333;" href="/partnership/cabinet/index/?sortreferal=asc">
					<img src="/images/arrow-table.png" alt="" title="Сортировать по возрастанию">
					</a>'*/,
					'encodeLabel' => false,
					'footerOptions' => ['style' => 'border-left: none; border-right: none;'],
				], [
					'label' => Module::t('module', 'Amount')/*.
					'&nbsp;&nbsp;<a style="color:#333333;" href="/partnership/cabinet/index/?sortreferal=asc">
					<img src="/images/arrow-table.png" alt="" title="Сортировать по возрастанию">
					</a>'*/,
					'encodeLabel' => false,
					'attribute' => 'amount',
					'format' => 'currency',
					'footer' => Module::t('module', 'Bonus total:'),
					'footerOptions' => ['class' => 'totalSum'],
				], [
					'attribute' => 'bonus',
					'format' => 'raw',
					'label' => Module::t('module', 'Your bonus')/*.
					'&nbsp;&nbsp;<a style="color:#333333;" href="/partnership/cabinet/index/?sortreferal=asc">
					<img src="/images/arrow-table.png" alt="" title="Сортировать по возрастанию">
					</a>'*/,
					'encodeLabel' => false,
					'value' => function ($item) {
						/** @var \app\modules\partnership\models\Payment $item */
						$result = Yii::$app->formatter->asCurrency($item->bonus);
						if ($item->refunded > 0) {
							$result .= Html::tag(
								'sup',
								'-' . Yii::$app->formatter->asCurrency($item->refunded),
								[
									'class' => 'text-danger',
									'title' => Module::t('module', 'Refunded')
								]
							);
						}
						return $result;
					},
					'footer' => Yii::$app->formatter->asCurrency($sum),
				],
			]
		]);
		?>
	</div>
	<div class="view" style="display: none;">
		Отображать по:
		<a class="selected" href="#">10</a>
		<a onclick="change(20);" href="#">20</a>
		<a onclick="change(30);" href="#">30</a>
	</div>

</div>

<div>
	<?= ''// Html::a(Module::t('module', 'Available banners'), ['banners'])     ?>

</div>

<div class="modal fade" tabindex="-1" role="dialog" id="ref_link_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?= Module::t('module', 'Referal link') ?></h4>
			</div>
			<div class="modal-body">
				<?= Html::input('text', '', $partner->referal_link, ['class' => 'form-control']) ?>
				<div class="pull-right">
					<?= Html::a(
						Module::t('module', 'copy to clipboard'),
						'#',
						[
							'data-clipboard-text' => $partner->referal_link,
							'class' => 'js-btn',
							'id' => 'copy_btn',
						]
					) ?>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="refund_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?= Module::t('module', 'Cash out') ?></h4>
			</div>
			<?php $form = \yii\widgets\ActiveForm::begin([
				//'enableAjaxValidation' => true,
				//'validationUrl' => ['validate-refund']
			]); ?>
			<div class="modal-body">
				<?= $form->field($refund_form, 'amount')->textInput([
					'onchange' => '$(\'#min_refund_warning\').toggle(Number(this.value) < ' . Module::$settings->min_refund . ');'
				]) ?>
				<?php
				if ($partner->walet == '') {
					$partner->walet = 'Webmoney';
				}
				?>
				<?= $form->field($refund_form, 'target')->dropDownList([
					Refund::TYPE_WALLET => Module::t('module', 'Wallet ({wallet})', ['wallet' => $partner->walet]),
					Refund::TYPE_INNER => Module::t('module', 'Zengram balance'),
				], [
					'prompt' => Module::t('module', 'Choose one')
				]) ?>
				<div id="koshel" style="display: none;">

					<?= $form->field($refund_form, 'walet')->textInput(['placeholder' => 'R123456789012'])->label('Кошелёк WMR');; ?>

				</div>
				<div class="trigger-webmoney" style="display: none;">
					<div id="min_refund_warning"
					     class="text-warning" <?= ''// ($partner->balance < Module::$settings->min_refund ?: 'style="display: none;"')   ?>>
						<b>
							<?= Module::t('module', 'The amount of payments less than {min_refund} are carried out on the second Monday of each month', [
								'min_refund' => \Yii::$app->formatter->asCurrency(Module::$settings->min_refund)
							]) ?>
						</b>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="pull-right">
					<?= Html::submitButton(Module::t('module', 'Cash out'), ['class' => 'btn btn-success']) ?>
				</div>
			</div>
			<?php $form->end(); ?>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="partnership-paymentinfo" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Укажите кошелёк для вывода средств</h4>
			</div>
			<div class="modal-body">

				<?php $form = \yii\widgets\ActiveForm::begin([]); ?>
				<?= $form->field($model, 'walet')->textInput(['placeholder' => 'R123456789012']); ?>
				<?= $form->field($model, 'confirm_oferta')->hiddenInput(['value' => '1'])->label(false); ?>

				<?= Html::submitButton(Module::t('module', 'Confirm'), ['class' => 'btn btn-success']); ?>
				<button class="btn btn-default" id="closePaymentInfo"
				        type="button"><?= Module::t('module', 'Cancel') ?></button>
				<button class="btn btn-default" id="closePaymentInfoFirst" style="display: none;"
				        type="button"><?= Module::t('module', 'Later') ?></button>
				<?php
				$form::end();
				?>

			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="partnership-cashback" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Заявка на вывод средств сформирована</h4>
			</div>
			<div class="modal-body">
				<span class="3-days">Вывод будет завершен в течение 3 рабочих дней. <br><br></span>
				<span class="big-sum">Средства будут выведены во второй понедельник месяца.<br><br></span>

				<button class="btn btn-default" id="closePaymentCashback"
				        type="button"><?= Module::t('module', 'ОК') ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="partnership-cashback-inner" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Вывод средств на внутренний счёт Zengram</h4>
			</div>
			<div class="modal-body">
				Средства перечислены на Ваш внутренний счёт Zengram.<br>
				<br>
				<button class="btn btn-default" data-dismiss="modal" aria-label="Close" id="closePaymentCashback"
				        type="button"><?= Module::t('module', 'ОК') ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>
	jQuery(function () {
		new Clipboard('#copy_btn');
	});

	var infoModal = $('#partnership-paymentinfo'),
		refundAmount = $('#refundform-amount');

	$('#paymentinfoChange').click(function () {
		infoModal.modal('show');
	});
	$('#closePaymentInfo').click(function () {
		infoModal.modal('hide');
	});
	$('#closePaymentInfoFirst').click(function () {
		infoModal.modal('hide');
	});

	$('#closePaymentCashback').click(function () {
		$('#partnership-cashback').modal('hide');
	});


	$('#refundform-target').change(function () {
		checkWMSumAdnShowMessage();
		var wallet = '<?= $partner->walet ?>';
		if (wallet == 'Webmoney' && $('#refundform-target option:selected').val() == 'wallet') {
			$('#koshel').show();
		} else {
			$('#koshel').hide();
		}

	});
	refundAmount.keyup(function () {
		checkWMSumAdnShowMessage();
		console.log(refundAmount.val());
	});
	refundAmount.change(function () {
		checkWMSumAdnShowMessage();
	});

	function checkWMSumAdnShowMessage() {
		if ($('#refundform-target option:selected').val() == 'wallet') {
			if (refundAmount.val() < 1000) {
				$('.trigger-webmoney').show();
			} else {
				$('.trigger-webmoney').hide();
			}
		} else {
			$('.trigger-webmoney').hide();
		}
	}
	$(document).ready(function () {
		var paymentinfo = <?= Yii::$app->request->get('paymentinfo', 0)?>;
		var walet = '<?= $partner->walet ?>';
		if (paymentinfo == 1 && walet == 'Webmoney') {
			$('#partnership-paymentinfo').modal('show');
			$('#closePaymentInfoFirst').show();
			$('#closePaymentInfo').hide();
			// Отправляем событие в Аналитикс о регистрации в партнёрке
			// TODO: Потом лучше сделать отдельный сабмит
			ga('send', 'event', 'действие', 'партнерка');
			yaCounter31898476.reachGoal('newpartner');
		}

		var cashback = <?= Yii::$app->request->get('cashback', 0)?>;
		if (cashback == 1) {
			$('#partnership-cashback').modal('show');
			$('.3-days').show();
			$('.big-sum').hide();

		}
		if (cashback == 2) {
			$('#partnership-cashback').modal('show');
			$('.3-days').hide();
			$('.big-sum').show();
		}
		if (cashback == 3) {
			$('#partnership-cashback-inner').modal('show')
		}
		$('[data-toggle = popover]').popover({
			placement: 'top',
			container: 'body',
			trigger: 'hover'
		});
	});


	function change($num) {

	}


</script>