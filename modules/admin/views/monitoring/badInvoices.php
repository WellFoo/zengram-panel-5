<?php

use app\modules\admin\models\Balance;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Лог незачисленных платежей';
$this->params['breadcrumbs'][] = ['label' => 'Отслеживания', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoring-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns'      => [
			'id',
			[
				'attribute' => 'user_id',
				'header' => 'ID пользователя',
			],
			'date',
			[
				'attribute' => 'agent',
				'header' => 'Система оплаты',
			],
			[
				'attribute' => 'sum',
				'header' => 'Цена',
			],
		],
	]); ?>

</div>
