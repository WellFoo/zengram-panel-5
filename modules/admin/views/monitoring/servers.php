<?php

use app\modules\admin\models\Balance;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Лог отслеживаний по серверам';
$this->params['breadcrumbs'][] = ['label' => 'Отслеживания', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoring-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns'      => [
			'starttime',
			[
				'attribute' => 'offline_time',
				'value' => function($item){
					return Balance::formatBalance($item->offline_time);
				}
			],
			'complete:boolean',
			'worker',
		],
	]); ?>

</div>
