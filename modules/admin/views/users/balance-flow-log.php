<?php

/* @var $this yii\web\View
 * @var $data array
 * @var $email
 * @var $interval
 * @var $user_id
 */

use app\modules\admin\models\Balance;
use yii\bootstrap\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$this->title = 'Логи списания баланса';

$data_provider = new ArrayDataProvider([
	'allModels' => $data,
]);

ActiveForm::begin([
	'action' => [''],
	'method' => 'get',
	'options' => [
		'class' => 'form-horizontal',
		'style' => 'margin: 20px 0;'
	]
]);

?>

	<style>
		.expander {
			width: 135px;
			text-align: right;
		}
	</style>

<h2>Списание баланса <?= $email ?> за <?= Balance::formatBalance($interval, '%r%Dд, %Hч') ?></h2>

<h4>
	* <b>ВНИМАНИЕ!</b>
	<br>- в счётчике блокировок учитывается время блокировки параллельных действий. Т.е. учитывается блокировка по комментам, когда подписки ещё в работе, т.е. аккаунт не остановлен
	<br>- не учитываются удалённые аккаунты
</h4>

<br><br>

<div class="row">

	<input type="hidden" name="user_id" value="<?= $user_id ?>">
	<div class="col-xs-6">
		<div class="input-group col-xs-10 col-xs-offset-1">
			<label for="date-from" class="input-group-addon">с</label>
			<input name="from" value="<?= $date_from ?>" id="date-from"
			       class="form-control date-picker" data-date-format="yyyy-mm-dd">
			<label for="date-to" class="input-group-addon">по</label>
			<input name="to" value="<?= $date_to ?>" id="date-to"
			       class="form-control date-picker" data-date-format="yyyy-mm-dd">
			<span class="input-group-btn">
			<button type="submit" class="btn btn-success">Вперед</button>
		</span>
		</div>
	</div>

</div>

<br><br>

<?php

ActiveForm::end();

echo GridView::widget([
	'dataProvider' => $data_provider,
	'showHeader' => true,
	'tableOptions' => [
		'class' => 'table table-bordered table-hover'
	],
	'columns' => [
		[
			'attribute' => 'account',
			'label' => 'Аккаунт',
			'value' => function ($data) {
				return $data['account']->login;
			}
		],
		[
			'attribute' => 'balance',
			'label' => 'Время работы аккаунта',
			'value' => function ($data) {
				return Balance::formatBalance($data['balance']);
			}
		],
		[
			'attribute' => 'blocked_time',
			'format' => 'raw',
			'label' => 'Время в блокировке',
			'value' => function ($data) {
				return Balance::formatBalance($data['blocked_time']);
			}
		],
		[
			'attribute' => 'balance',
			'label' => 'Остановлен',
			'value' => function ($data) use ($interval) {
				return Balance::formatBalance($interval - $data['balance']);
			}
		],
		[
			'attribute' => 'balance',
			'label' => 'Списанный баланс',
			'value' => function ($data) {
				$value = $data['balance'] - $data['balance'] % 60;
				return Balance::formatBalance($value, '%r%Dд, %Hч, %Iм');
			}
		],
		[
			'attribute' => 'actions',
			'format' => 'raw',
			'label' => 'Действия',
			'value' => function ($data) {
				return \yii\bootstrap\Html::tag('div', implode('<br>', $data['actions']), [
					'class' => 'expander'
				]);
			}
		],
		[
			'attribute' => 'blocks',
			'format' => 'raw',
			'label' => 'Полученные блокировки',
			'value' => function ($data) {
				return implode(', ', $data['blocks']);
			}
		],
	]
]);

?>