<?php

use app\modules\admin\assets\AdminAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AdminAsset::register($this);
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<style scoped>
	.asc:after {
		content:"\f160";
		display: inline-block;
		font: normal normal normal 14px/1 FontAwesome;
		font-size: inherit;
		text-rendering: auto;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		margin-left: 2px;
	}
	.desc:after {
		content:"\f161";
		display: inline-block;
		font: normal normal normal 14px/1 FontAwesome;
		font-size: inherit;
		text-rendering: auto;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		margin-left: 2px;
	}

	.zen-logo {
		position: relative;
		display: inline-block;
	}

	.zen-logo .server {
		position: absolute;
		left: 0;
		top: 210%;
		display: none;
		color: #000;
	}

	.zen-logo:hover .server {
		display: block;
	}
</style>
<div class="wrap">
	<?php
	NavBar::begin([
		'brandLabel' => '<span class="zen-logo">'.Yii::$app->params['company'].'<span class="server">'.$_SERVER['SERVER_ADDR'].'</span></span>',
		'brandUrl' => Yii::$app->homeUrl,
		'options' => [
			'class' => 'navbar-inverse',
		],
	]);

	$menuItems[] = ['label' => 'Мониторинг', 'url' => ['/admin/servers/monitoring']];

	/*$menuItems[] = ['label' => 'Звёзды', 'items' => [
		['label' => 'Список', 'url' => ['/admin/accounts-grid/popular-accounts']],
		['label' => 'Настройки', 'url' => ['/admin/commenting/']],
		['label' => 'Статистика ', 'url' => ['/admin/accounts-grid/popular-log']],
	]];*/

	$menuItems[] = ['label' => 'Клиенты', 'items' => [
		['label' => 'Все', 'url' => ['/admin/users']],
		['label' => 'Оплатившие', 'url' => ['/admin/payed-users']],
		'<li class="divider"></li>',
		['label' => 'Рассылки', 'url' => ['/admin/users/mailing']],
		'<li class="divider"></li>',
		['label' => 'Админский акк', 'url' => ['/admin/users?UsersSearch[id]=54']],
		'<li class="divider"></li>',
		['label' => 'Статистика по функциям', 'url' => ['/admin/stats/functions']],
	]];



	$menuItems[] = ['label' => 'Проекты', 'items' => [
		['label' => 'Список проектов', 'url' => ['/admin/projects']],
		['label' => 'Стартовые логи проектов', 'url' => ['/admin/projects/logs']],
		'<li class="divider"></li>',
		['label' => 'Определение ID по логину', 'url' => ['/admin/utility/id-by-login']],
		['label' => 'Ошибочные whitelist\'ы', 'url' => ['/admin/utility/whitelist-log']],
	]];
	$menuItems[] = ['label' => 'Статистика', 'items' => [
		['label' => 'Отчеты', 'url' => ['/admin/report/']],
		['label' => 'Общая статистика', 'url' => ['/admin/stats/overall']],
		['label' => 'Статика работы клиента', 'url' => ['/admin/stats/usage']],
		['label' => 'Статистика оплат', 'url' => ['/admin/stats/payments']],
		['label' => 'Статистика предложений', 'url' => ['/admin/stats/suggest-promo']],
		['label' => 'Статистика регионов', 'url' => ['/admin/stats/regions']],
		'<li class="divider"></li>',
		['label' => 'Статистика действий общая', 'url' => ['/admin/stats/accounts-summary']],
		['label' => 'Статистика действий по проектам', 'url' => ['/admin/stats/accounts-actions']],
		'<li class="divider"></li>',
		['label' => 'Статистика акций', 'url' => ['/admin/stats/discount-actions']],
		'<li class="divider"></li>',
		['label' => 'Статистика блокировок действий', 'url' => ['/admin/stats/blocks']],
		['label' => 'Антиспам', 'url' => ['/admin/antispam']],
		'<li class="divider"></li>',
		['label' => 'Мониторинг', 'url' => ['/admin/monitoring']],
		['label' => 'Хэштеги', 'url' => ['/admin/hash-tags']],
	]];
	$menuItems[] = [
		'label' => 'Содержимое',
		'items' => [
			'<li class="dropdown-header">F.A.Q.</li>',
			['label' => '   *Разделы', 'url' => ['/admin/faq-cats']],
			['label' => '   *Статьи', 'url' => ['/admin/faq']],
			'<li class="divider"></li>',
			['label' => 'Блог', 'url' => ['/admin/blog']],
			'<li class="divider"></li>',
			['label' => 'Страницы', 'url' => ['/admin/pages']],
		],
		'visible' => Yii::$app->user->identity->isAdmin
	];
	$menuItems[] = [
		'label' => 'Настройки',
		'items' => [
			['label' => 'Глобальные настройки', 'url' => ['/admin/settings']],
			'<li class="divider"></li>',
			['label' => 'Сервера', 'url' => ['/admin/servers']],
			['label' => 'Прокси', 'url' => ['/admin/proxy']],
			'<li class="divider"></li>',
			['label' => 'База комментариев', 'url' => ['/admin/comments-base']],
			'<li class="divider"></li>',
			['label' => 'Рабочие точки', 'url' => ['/admin/geo-points']],
			['label' => 'Регионы', 'url' => ['/admin/regions']],
			['label' => 'Антиспам', 'url' => '/admin/antispam-settings'],
			//['label' => 'Антиспам', 'url' => '/admin/antispam-settings'],
		],
		'visible' => Yii::$app->user->identity->isAdmin
	];
	$menuItems[] = [
		'label' => 'Google Analytics',
		'url' => ['/admin/custom-dimensions'],
	];

	$menuItems[] = [
		'label' => 'Выход (' . Yii::$app->user->identity->mail . ')',
		'url' => ['/site/logout'],
		'linkOptions' => ['data-method' => 'post']
	];

	echo Nav::widget([
		'options' => ['class' => 'navbar-nav navbar-right'],
		'items' => $menuItems,
	]);
	NavBar::end();
	?>

	<div class="container" style="margin-top: 60px;">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<?= $content ?>
	</div>
</div>

<footer class="footer">
	<!--div class="container">
    <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div-->
</footer>

<?php $this->endBody() ?>
<script>
	$(function ()
	{
		$('.date-picker').datepicker().on('changeDate', function(/*ev*/){
			$('.date-picker').datepicker('hide');
		});
	});
</script>
</body>
</html>
<?php $this->endPage() ?>
