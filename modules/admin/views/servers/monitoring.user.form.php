<?php

/**
 * @var $this \yii\web\View
 * @var $user \app\models\SnifferUsers
 */

use app\models\SnifferServers;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();

echo $form->field($user, 'name');
echo $form->field($user, 'email');
echo $form->field($user, 'phone');
echo $form->field($user, 'skype');

echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);

ActiveForm::end();