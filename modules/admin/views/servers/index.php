<?php
/**
 * @var \app\models\Account $projects
 * @var array $servers
 */
$this->title = 'Servers';
?>
<div class="site-admin">

	<h1>Пользователи</h1>
	<table class="table">
		<thead>
		<tr>
			<th>ID</th>
			<th class="text-center">Login</th>
			<th class="text-center">Instagram ID</th>
			<th class="text-center">Server</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($projects as $account): ?>
			<tr>
				<td><?php echo $account->id; ?></td>
				<td class="text-center"><?php echo $account->login; ?></td>
				<td class="text-center"><?php echo $account->instagram_id; ?></td>
				<td class="text-center"><?php echo $account->server ? $account->server : '-Не выбран-'; ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

	<h1>Сервера</h1>
	<table class="table">
		<thead>
		<tr>
			<th>IP адресс</th>
			<th class="text-center">Пользователей</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($servers as $server): ?>
			<tr>
				<td><?php echo $server['server'] ? $server['server'] : '-Не выбран-'; ?></td>
				<td class="text-center"><?php echo $server['count']; ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

</div>