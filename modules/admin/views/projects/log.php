<?php
/** @var \yii\web\View $this */
/** @var \app\models\AccountsLog $project */
$projectData = json_decode($project->data, true);

$this->title                   = 'Информация о проекте';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-admin">

	<h1><?php echo $this->title; ?></h1>
	<table class="table table-striped table-bordered">
		<?php if($projectData): ?>
			<?php foreach ($projectData['user'] as $name => $value): ?>
				<tr>
					<td><?=$name?></td>
					<td><?=$value;?></td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</table>
</div>