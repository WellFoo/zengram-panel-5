<?php

use app\models\Regions;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Регионы';

$tree = Regions::getTree();
?>

<div class="site-admin">

	<h1><?=$this->title?></h1>

	<div class="row">
		<div class="col-xs-12 col-md-6" id="regions-list">
			<?php
			echo \app\widgets\Tree::widget([
				'items' => $tree,
				'valueCallback' => function ($item) {
					$html =  $item['label']; //Html::a($item['label'], Url::to(['/admin/regions', 'id' => $item['id']]));
					$html .= Html::a(
						Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']),
						Url::to(['remove', 'id' => $item['id']]),
						[
							'class' => 'remove-region pull-right',
							'style' => 'margin-left: 10px;',
							'onClick' => 'return confirm("Вы уверены, что хотите удалить выбранный регион?")'
						]
					);
					$html .= Html::a(
						Html::tag('span', '', ['class' => 'glyphicon glyphicon-plus']),
						'#',
						[
							'class' => 'add-region pull-right',
							'data' => ['id' => $item['id']]
						]
					);
					return $html;
				}
			]);
			?>
		</div>
	</div>

</div>

<div class="modal fade" id="addRegion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Добавить новый регион</h4>
			</div>
			<div class="modal-body">
				<?php
				function renderTree($data, &$list = [], $level = 1) {
					foreach ($data as $item) {
						$list[$item['id']] = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level) . $item['label'];
						if (isset($item['items'])) {
							renderTree($item['items'], $list, $level + 1);
						}
					}
					return $list;
				}

				$model = new Regions();

				$form = ActiveForm::begin([
					'id' => 'proxy-form',
					'options' => ['class' => 'form-horizontal'],
					'action' => Url::toRoute('/admin/regions/create'),
					'fieldConfig' => [
						'template' => '{label}<div class="col-sm-8">{input}</div><div class="col-sm-12 text-center">{error}</div>',
						'labelOptions' => ['class' => 'col-sm-4 control-label'],
					],
				]);

				echo $form->field($model, 'parent_id')->dropDownList(
					renderTree($tree, $list),
					[
						'id'     => 'parent-region',
						'prompt' => 'Корневая категория',
						'encode' => false
					]
				);

				echo $form->field($model, 'name');

				echo '<p>Если вы хотите добавить несколько городов/регионов, принадлежащих выбранной категории, введите их через запятую</p>';

				echo Html::submitButton(Yii::t('app','Добавить новый регион'), ['class' => 'btn btn-primary center-block', 'name' => 'login-button']);

				ActiveForm::end();
				?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('#regions-list .add-region').on('click', function(){
		var id = $(this).data('id');
		$('#parent-region option').each(function(){
			this.selected = (this.value == id);
		});
		$('#addRegion').modal();
	});
	$('#regions-list .remove-region').on('click', function(){

	});
});
</script>