<?php

use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $region app\models\Regions
 */

$this->registerJsFile('@web/js/lib/bootstrap3-typeahead.min.js', ['position' => View::POS_BEGIN]);

$this->title = 'Регионы';
?>

<style type="text/css">
	.balloon {
		background: #A6A6A4;
		border-radius: 5px;
		color: #FFF;
		display: block;
		float: left;
		clear: right;
		min-width: 280px;
		position: relative;
		padding: 7px 70px 7px 15px;
		margin: 0 10px 10px 0;
	}
	.balloon * {
		font-size: 14px;
		line-height: 24px;
	}
	.balloon .button-holder {
		position: absolute;
		right: 0;
		top: 2px;
	}
	.balloon .button-holder * {
		cursor: pointer;
		padding: 5px;
		margin-right: 5px;
		opacity: .3;
	}
	.balloon .button-holder *:hover {
		opacity: 1;
	}
	.pagination > .active > a {
		background-color: #4DA54D;
		border-color: #4DA54D;
	}
</style>

<div class="site-admin">

	<h1>
		<a href="<?= Url::to(['/admin/regions']) ?>"><?= $this->title ?></a>
		/
		<?= $region->name ?>
	</h1>

	<div class="row" style="margin-bottom: 15px;">

		<div class="col-md-6" id="mapInputHolder" style="padding-left: 0;">
			<input id="mapText" type="search" class="form-control" placeholder="Введите город"
			       autocomplete="off" data-url="<?=Url::to(['/admin/places/list/' . $region->id])?>">
			<div class="help-block"></div>
		</div>
	</div>

	<div class="row" id="placesList">

	</div>

	<div id="placeTemplate" class="balloon" style="display: none;">
		<span class="button-holder">
			<span class="delete glyphicon glyphicon-trash" title="Удалить"></span>
		</span>
		<span class="content"></span>
	</div>

</div>

<script type="text/javascript">
// Places block
(function($){ 'use strict';
	var $mapInput,
		placeManageUrl,
		$mapInputHolder,
		$mapBtn,
		$placesList,
		$placeTemplate,
		$errorContainer;

	function createPlace(id, name)
	{
		var $place = $placeTemplate.clone();
		$place
			.attr('id', 'place_' + id)
			.data('id', id);
		$place.find('.content').text(name);
		$place.prependTo($placesList);
		$place.show('fast');
	}

	function deletePlace(id)
	{
		$('#place_' + id).hide('fast', function(){
			$(this).remove();
		})
	}

	function showError(text)
	{
		$mapInputHolder.addClass('has-error');
		$errorContainer
			.text(text)
			.show();
	}

	function hideError()
	{
		$errorContainer.text('').hide();
		$mapInputHolder.removeClass('has-error');
	}

	function runAjax(data)
	{
		$.post(
			placeManageUrl,
			data,
			placeSuccessCallback,
			'JSON'
		).always(placeAjaxCallback);
		$mapInputHolder.addClass('disable');
		hideError();
	}

	function placeSuccessCallback(response)
	{
		switch (response.action) {
			case 'create':
				createPlace(response.id, response.name);
				break;
			case 'delete':
				deletePlace(response.id);
				break;
			case 'error':
				showError(response.text);
				break;
		}
	}

	function placeAjaxCallback()
	{
		$mapInput.val('');
		$mapInputHolder.removeClass('disable');
	}

	function placesListCallback(result)
	{
		for (var i = 0; i < result.length; i++) {
			if (result[i]) {
				createPlace(result[i].id, result[i].name);
			}
		}
	}

	function addMap(place)
	{
		var $mapInput = $('#mapText');
		place = place || $mapInput.data('place');
		if (place) {
			placeChanged(place);
			$mapInput.data('place', null);
			$mapInput.val('');
		}
		return false;
	}

	function geocoderDataSource(query, process)
	{
		$.ajax({
			url: 'https://geocode-maps.yandex.ru/1.x/',
			type: 'GET',
			data: {
				geocode:  query,
				kind:     'locality',
				format:   'json',
				results:  20
			},
			dataType: 'jsonp',
			success: function callback(response/*, status, xhr*/) {
				/* @var response.response Object */
				var points = $.map(response.response.GeoObjectCollection.featureMember, function(item/*, index*/) {
					/* @var item Object */
					var kind = item.GeoObject.metaDataProperty.GeocoderMetaData.kind;
					if (kind !== 'locality' && kind !== 'district') {
						return null;
					}
					return {
						name: item.GeoObject.name,
						fullName: item.GeoObject.name + ', ' + item.GeoObject.description,
						center: item.GeoObject.Point.pos,
						corners: {
							ne: item.GeoObject.boundedBy.Envelope.upperCorner,
							sw: item.GeoObject.boundedBy.Envelope.lowerCorner
						}
					};
				});
				process(points);
			}
		});
	}

	function placeChanged(place)
	{
		var id = place.center.replace(/\./g, '').replace(' ', '_'),
			center = geoPoint(place.center),
			cornerNe = geoPoint(place.corners.ne),
			cornerSw = geoPoint(place.corners.sw);

		var dL = Math.abs((parseFloat(center.lat) + 90) - (parseFloat(cornerSw.lat) + 90)),
			dR = Math.abs((parseFloat(center.lat) + 90) - (parseFloat(cornerNe.lat) + 90)),
			dT = Math.abs((parseFloat(center.lng) + 180) - (parseFloat(cornerNe.lng) + 180)),
			dB = Math.abs((parseFloat(center.lng) + 180) - (parseFloat(cornerSw.lng) + 180));

		var corner = {
			lat: dL > dR ? cornerSw.lat : cornerNe.lat,
			lng: dB > dT ? cornerSw.lng : cornerNe.lng
		};

		runAjax({
			action: 'create',
			place: {
				googleId: id,
				name: place.name,
				center: {
					lat: center.lat,
					lng: center.lng
				},
				radius: sectionLength(center, corner),
				corners: {
					ne: {
						lat: cornerNe.lat,
						lng: cornerNe.lng
					},
					sw: {
						lat: cornerSw.lat,
						lng: cornerSw.lng
					}
				}
			}
		});
	}

	function sectionLength(from, to)
	{
		var w = to.lng;
		var centerLng = from.lng;

		var centerLat = toRad(from.lat);

		var dlong = toRad(Math.abs(centerLng - w)); // This is the distance from the center to the left in radians

		//Calculating distance using the Haversine formula.
		var a = Math.pow(Math.cos(centerLat), 2) *
			Math.sin(dlong / 2) * Math.sin(dlong / 2);

		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return Math.round(6371000 * c);
	}

	function toRad(number)
	{
		return number * Math.PI / 180;
	}

	var geoPoint = function(string, lngFirst)
	{
		var parts = string.split(' ');
		return {
			lat: lngFirst ? parts[0] : parts[1],
			lng: lngFirst ? parts[1] : parts[0]
		};
	};

	$(document).ready(function(){
		$mapInput = $('#mapText');
		placeManageUrl = $mapInput.data('url');
		$mapInputHolder = $('#mapInputHolder');
		$mapBtn = $('#mapAddBtn');
		$placesList = $('#placesList');
		$placeTemplate = $('#placeTemplate');
		$errorContainer = $mapInputHolder.parent().find('.help-block');

		$mapInput.typeahead({
			minLength: 2,
			//delay: 1,
			matcher: function() { return true; },
			displayText: function(item) {
				return item === undefined ? '' : item.fullName;
			},
			source: geocoderDataSource,
			afterSelect: function(place) {
				$mapInput.data('place', place);
				addMap(place);
			}
		});

		$mapBtn.on('click', addMap);

		$placesList.on('click', '.delete', function(){
			var data = {
				action: 'delete',
				place: {
					id: $(this).parents('.balloon').data('id')
				}
			};
			runAjax(data);
		});

		$.post(
			placeManageUrl,
			{action: 'list'},
			placesListCallback
		);
	});
})(jQuery);
</script>