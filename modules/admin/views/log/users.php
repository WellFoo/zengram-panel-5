<?php

use app\models\UsersLog;

/* @var $this yii\web\View
 * @var $logs array
 */

$this->title                   = 'Логи';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-admin">

	<table class="table">
		<thead>
		<tr>
			<th>id</th>
			<th>Дата</th>
			<th>Текст</th>
		</tr>
		</thead>
		<tbody>
		<?php

		/** @var UsersLog $logs */
		foreach($logs as $log): ?>
			<tr>
				<td><?= $log->id; ?></td>
				<td><?= $log->date; ?></td>
				<td><?= $log->text; ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
