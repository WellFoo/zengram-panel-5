<?php

use app\modules\admin\models\Stopwords;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\StopwordsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки модуля Антиспам';
?>

<div class="pull-right">
    <form method="post">
        <input type="hidden" name="antispamTrigger" value="1">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
        <?= Html::submitButton('Включить Антиспам для всех', ['class' => 'btn btn-success pull-right', 'style' => 'width:230px; margin-bottom:15px;']) ?>
    </form>
    <form method="post">
        <input type="hidden" name="antispamTrigger" value="2">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
        <?= Html::submitButton('Выключить Антиспам для всех', ['class' => 'btn btn-danger pull-right', 'style' => 'width:230px; margin-bottom:15px;']) ?>
    </form>
</div>

<div class="stopwords-index">

    <h2><?= Html::encode($this->title) ?></h2>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить Стоп-Слово', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

	        [
		        'attribute' => 'pattern',
		        'value' => function($item) {
			        return Stopwords::getPatternDescription($item->pattern);
		        }
	        ],
            'text_word',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
