<?php

use app\modules\admin\models\Stopwords;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Stopwords */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stopwords-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text_word')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'pattern')->dropDownList(Stopwords::patternDescriptions()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
