<?php
/* @var string $reportType */
?>
<form method="get" class="form-horizontal" style="margin: 20px 0;">
	<input type="hidden" value="<?= $reportType ?>" class="type-report" name="type-report">
	<div class="row">
		<div class="col-xs-3">
			<h1 style="margin-top:0;">Отчёты</h1>
		</div>
		<div class="col-xs-7">
			<div class="btn-group select-report" role="group" aria-label="...">
				<button type="button"  value="registered"
				        class="btn <?=$reportType == 'registered' ? 'btn-success' : 'btn-default' ?>">Зарегистрированных
				</button>
				<button type="button" value="payed"
				        class="btn  <?=$reportType == 'payed' ? 'btn-success' : 'btn-default' ?>">Оплативших за период
				</button>
			</div>
		</div>
		<div class="col-xs-2">
			<button type="submit" name="download" value="1" class="btn btn-success"><span class="glyphicon glyphicon-download-alt"></span> Скачать xlsx</button>
		</div>

	</div>

	<div class="row">
		<div class="col-xs-3">
			<?php /*
			<div class="btn-group" role="group" aria-label="...">
				<button type="submit" name="date" value="day"
				        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
				</button>
				<button type="submit" name="date" value="week"
				        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
				</button>
				<button type="submit" name="date" value="month"
				        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
				</button>
			</div>
 */
			?>
		</div>
		<div class="col-xs-6">
			<label class="control-label col-xs-2">Диапазон</label>

			<div class="input-group col-xs-8 col-xs-offset-1">
				<label for="date-from" class="input-group-addon">с</label>
				<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">
				<label for="date-to" class="input-group-addon">по</label>
				<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">

				<span class="input-group-btn">
					<button type="submit" name="date" value="range"
					        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
					</button>
				</span>
			</div>
		</div>
		<div class="col-xs-1">
			<?php /*
			<button class="btn btn-success"><span class="glyphicon glyphicon-download-alt"></span> Сохранить отчёт
			</button>
 */
            ?>
		</div>
	</div>
	<div class="row" style="margin-top: 20px;">
		<div class="col-xs-3"><h3>Фильтры</h3></div>
		<div class="col-xs-9">
			<select name="ufilter" style="margin-top: 20px;">
				<option <?= $ufilter == 0 ? 'selected="selected"' : ''; ?> value="0">Без фильтра</option>
				<option <?= $ufilter == 1 ? 'selected="selected"' : ''; ?> value="1">Оплачивали и не совершали действия за последний месяц</option>
				<option <?= $ufilter == 2 ? 'selected="selected"' : ''; ?> value="2">Оплачивали, имеют нулевой баланс и не совершали действий за последний месяц</option>
				<option <?= $ufilter == 3 ? 'selected="selected"' : ''; ?> value="3">Оплатили один раз и далее не оплачивали</option>
				<option <?= $ufilter == 4 ? 'selected="selected"' : ''; ?> value="4">Зарегистрировались через rf.zengram.ru</option>
			</select>
		</div>
	</div>
</form>



<script>
	$(document).ready(function(){
		$('.select-report button').click(function(){
			var report = $(this).val();
			$('.type-report').val(report);
			$('.form-horizontal').submit();
		});
	})
</script>