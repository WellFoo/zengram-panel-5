<h2>Сервис Instashpion</h2>
<?php
use yii\grid\GridView;
?>
<form method="get" class="form-horizontal" style="margin: 20px 0;">
	<div class="row">
		<div class="col-xs-3">
			<div class="btn-group" role="group" aria-label="...">
				<button type="submit" name="date" value="day"
				        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
				</button>
				<button type="submit" name="date" value="week"
				        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
				</button>
				<button type="submit" name="date" value="month"
				        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
				</button>
			</div>

		</div>

		<div class="col-xs-9">
			<label class="control-label col-xs-2">Диапазон</label>

			<div class="input-group col-xs-6 col-xs-offset-1">
				<label for="date-from" class="input-group-addon">с</label>
				<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">
				<label for="date-to" class="input-group-addon">по</label>
				<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">

				<span class="input-group-btn">
					<button type="submit" name="date" value="range"
					        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
					</button>
				</span>
			</div>
		</div>
	</div>
</form>

<table style = "width: 100%">
	<tr>
		<td style = "width: 33%">
			<h3>Переходы</h3>
			<table style = "width: 100%">
				<tr>
					<td>Zengram</td>
					<td><?= $zengramHits?></td>
				</tr>
				<tr>
					<td>Google</td>
					<td><?=$googleHits?></td>
				</tr>
				<tr>
					<td>Yandex</td>
					<td><?=$yandexHits?></td>
				</tr>
				<tr>
					<td>Прямые заходы</td>
					<td><?=$normalHits?></td>
				</tr>
				<tr>
					<td>Другие источники</td>
					<td><?=$etcHits?></td>
				</tr>
			</table>
		</td>
		<td valign = "top" style = "width: 18%">
			<h3>Оплаты</h3>
			<table style = "width: 100%">
				<tr>
					<td>Yandex</td>
					<td><?=intval($yandexPayments)?></td>
				</tr>
				<tr>
					<td>Интеркасса</td>
					<td><?=intval($interkassaPayments)?></td>
				</tr>
				<tr>
					<td>Paypal</td>
					<td><?=intval($paypalPayments)?></td>
				</tr>
			</table>
		</td>
		<td style = "width: 48%">
			<h3>Анализы</h3>
			<table  style = "width: 100%">
				<tr>
					<td>От пользователей</td>
					<td><?=intval($usersAnalizeCount)?></td>
				</tr>
				<tr>
					<td>От гостей</td>
					<td><?=intval($guestsAnalizeCount)?></td>
				</tr>
				<tr>
					<td valign = "top">Купленные</td>
					<td>
						<?=intval($guestsAnalizeCount)?><br />
						<small style = "color: #ccc; font-size: 11px;">
							Кол. купленных анализов <br />(возможно не все израсходованы).
						</small>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<h3>История анализов</h3>

<?php
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'columns'      => [
		'id',
		[
			'attribute' => 'user_id',
			'format'    => 'raw',
			'value'     => function ($item) {
				$victim = $item->user;
				if (!empty($victim)) {
					return '<a href = "'.  \yii\helpers\Url::to(['admin/users', 'mail' => $item->user->mail]) . '">'.$item->user->mail.'</a>';
				}

				return 'Гость';
			}
		],
		[
			'attribute' => 'victim_id',
			'format'    => 'raw',
			'value'     => function ($item) {
				$victim = $item->victim;
				if (!empty($victim)) {
					return $item->victim->username;
				}

				return null;
			}
		],
		'updated_at'

	],
]);