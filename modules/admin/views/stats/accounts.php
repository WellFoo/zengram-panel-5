<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var app\models\AccountStats[] $summary
 * @var string[] $overall
 * @var string[] $service
 * @var string $date
 * @var integer $dateFrom
 * @var integer $dateTo
 * @var \app\modules\Admin\models\AccountStatsSearch $filterModel
 */

use app\modules\admin\models\AccountStatsSearch;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use MongoDB\Client;

$this->title = 'Статистика действий';
?>
<div class="accounts-actions-index">

	<h1><?= $this->title ?></h1>

	<?php
	Pjax::begin();
	ActiveForm::begin([
		'method' => 'get',
		'options' => [
			'class' => 'form-horizontal',
			'style' => 'margin: 20px 0;'
		]
	]); ?>
		<div class="row">
			<div class="col-xs-3">
				<div class="btn-group" role="group" aria-label="...">
					<button type="submit" name="date" value="day"
					        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
					</button>
					<button type="submit" name="date" value="week"
					        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
					</button>
					<button type="submit" name="date" value="month"
					        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
					</button>
				</div>

			</div>

			<div class="col-xs-9">
				<label class="control-label col-xs-2">Диапазон</label>

				<div class="input-group col-xs-6 col-xs-offset-1">
					<label for="date-from" class="input-group-addon">с</label>
					<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
					       class="form-control date-picker" data-date-format="yyyy-mm-dd">
					<label for="date-to" class="input-group-addon">по</label>
					<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
					       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
				</div>
			</div>
		</div>
	<?php
	ActiveForm::end();

	$models = $dataProvider->getModels();
	$ids = [];

	foreach ($models as $model) {
		$ids[] = intval($model->instagram_id);
	}

	$mongo = new Client(\Yii::$app->params['mongo_dsn']);

	$cursor = $mongo->zengram->out->aggregate([
			['$match' => [
				'$and' => [
					['value.instagram_id' => ['$in' => $ids]],
					['value.date' => [ '$gte' => (new \MongoDB\BSON\Timestamp(0, $dateFrom - (60 * 60 * 3 + 20))) ]]
				],
			]],
			['$project' => [
				'instagram_id' => '$value.instagram_id',
				'problem' => '$value.followersCountProblem',
				'date' => '$value.date',
				'came' => [ '$size' => '$value.camefollowers' ],
				'gone' => [ '$size' => '$value.gonefollowers' ]
			]],
		]
	);


	$documents = [];


	foreach ($cursor as $document) {
		$documents[$document['instagram_id']] = $document;
	}


	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $filterModel,
		'tableOptions' => [
			'class' => 'table table-bordered table-hover'
		],
		'columns'      => [
			'account_id',
			[
				'attribute' => 'mail',
				'format'    => 'raw',
				'value'     => function ($item) {
					/** @var app\models\AccountStats $item */
					if (!empty($item->user->mail)) {
						return $item->user->mail;
					}
					return null;
				}
			],
			[
				'attribute' => 'login',
				'format'    => 'raw',
				'value'     => function ($item, $key, $row, $column) use ($documents) {
					$filterModel = $column->grid->filterModel;
					/** @var app\models\AccountStats $item */
					if (empty($item->account->login)) {
						if (empty($item->instagram_id)) {
							return null;
						} else {
							$link = \yii\helpers\Html::a($item->instagram_id, Url::to(['/admin/stats/accounts-actions',
								'id'   => $item->account_id, 'date' => $filterModel->date,
								'from' => date('Y-m-d', $filterModel->dateFrom), 'to' => date('Y-m-d', $filterModel->dateTo)
							]), ['target' => '_blank']);

						}
					} else {
						$link =\yii\helpers\Html::a($item->account->login, Url::to(['/admin/stats/accounts-actions',
							'id'   => $item->account_id, 'date' => $filterModel->date,
							'from' => date('Y-m-d', $filterModel->dateFrom), 'to' => date('Y-m-d', $filterModel->dateTo)
						]), ['target' => '_blank']);
					}
					$problem = '';

					if (!empty($documents[$item->instagram_id]) && !empty($documents[$item->instagram_id]['problem']) && $documents[$item->instagram_id]['problem']) {
						$problem = '<b color = "red">!!!</b>';
					}

					return $link . ' ' . $problem;
				}
			],
			[
				'label'  => 'Статус',
				'format' => 'raw',
				'value'  => function ($item, $key, $row, $column) {
					if (empty($item->account)) {
						return '<span class="text-warning">Удален</span>';
					}
					/** @var DataColumn $column */
					/** @var AccountStatsSearch $filterModel */
					//$filterModel = $column->grid->filterModel;
					//return ArrayHelper::getValue($filterModel->instagram_id, intval($item->account->instagram_id), false) ? '<span class="text-success">запущен</span>' : 'остановлен';

					return $item->account->monitoring_status == 'work' ? 'Запущен' : 'Остановлен';
				}
			],
			[
				'attribute' => 'speed',
				'format'    => 'raw',
				'value'     => function ($item) {
					/** @var app\models\AccountStats $item */
					$speeds = [Yii::t('views', 'slow'), Yii::t('views', 'medium'), Yii::t('views', 'fast')];
					return $speeds[$item->speed];
				}
			],
			'likes',
			'comments',
			'follows',
			'follows_private',
			[
				'attribute' => 'mongo_came',
				'label'     => 'Пришли',
				'format'    => 'raw',
				'value'     => function ($model) {
					if ($model->mongo_came < 1 && $model->came > 0) {
						return $model->came;
					}

					return (
						'<span title = "Старый формат: '. $model->came .'">'.
							intval($model->mongo_came) .
						'</span>'
					);
				}
			],
			[
				'attribute' => 'mongo_gone',
				'label'     => 'Ушли',
				'format'    => 'raw',
				'value'     => function ($model)
				{
					if ($model->mongo_gone < 1 && $model->gone > 0) {
						return $model->gone;
					}

					return (
						'<span title = "Старый формат: '. $model->gone .'">'.
							intval($model->mongo_gone) .
						'</span>'
					);
				}
			],
			'unfollows',
			'usedlist_reseted',
			'manual_comments',
			'password_resets'
		],
	]);
	Pjax::end();
	?>
</div>