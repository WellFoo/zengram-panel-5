<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/** @var array $errors */

$this->title = 'Stop list';
?>
<div class="site-admin">

	<?php if (Yii::$app->session->hasFlash('StopDeleted')): ?>
		<div class="alert alert-success">Запись успешно удалена</div>
	<?php endif; ?>

	<table class="table">
		<thead>
		<tr>
			<th>Date</th>
			<th>User email</th>
			<th>Instagram login</th>
			<th>Error text</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($errors as $id => $error): ?>
			<tr id="error_<?=$id?>">
				<td><?=ArrayHelper::getValue($error, 0, '')?></td>
				<td><?=ArrayHelper::getValue($error, 'email', '')?></td>
				<td><?=ArrayHelper::getValue($error, 1, '')?></td>
				<td><?=ArrayHelper::getValue($error, 2, '')?></td>
				<td><?php echo Html::a('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', array(
						'#'.$id
					), array('class' => 'delete', 'data-value' => ArrayHelper::getValue($error, 'value', ''), 'data-id' => $id)); ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

</div>
<script>
	$('.delete').click(function(){
		var value = $(this).data('value');
		var id = $(this).data('id');

		$('#error_' + id).remove();

		$.post( "<?=\yii\helpers\Url::to(['/admin/stats/stop-list'])?>/" + id, { value: value, id: id } );

		return false;
	});
</script>