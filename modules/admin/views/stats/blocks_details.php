<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $data_provider
 * @var string $action
 * @var string $date
 * @var string $date_form
 * @var string $date_to
 */

use app\modules\admin\models\AccountBlocks;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


$this->title = 'Статистика по блокировкам (' . AccountBlocks::actionsLabels()[$action] . ')';
?>

<h2><?= $this->title ?></h2>

<?php
Pjax::begin();

ActiveForm::begin([
	'method' => 'get',
	'options' => [
		'class' => 'form-horizontal',
		'style' => 'margin: 20px 0;'
	]
]); ?>

	<div class="row">
		<div class="col-xs-3">
			<div class="btn-group" role="group" aria-label="...">
				<button type="submit" name="date" value="day"
				        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
				</button>
				<button type="submit" name="date" value="week"
				        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
				</button>
				<button type="submit" name="date" value="month"
				        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
				</button>
			</div>

		</div>

		<div class="col-xs-6">
			<label class="control-label col-xs-2">Диапазон</label>

			<div class="input-group col-xs-10 col-xs-offset-1">
				<label for="date-from" class="input-group-addon">с</label>
				<input name="from" value="<?= $date_form ? date('Y-m-d', $date_form) : '' ?>" id="date-from"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">
				<label for="date-to" class="input-group-addon">по</label>
				<input name="to" value="<?= $date_to ? date('Y-m-d', $date_to) : '' ?>" id="date-to"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">

			<span class="input-group-btn">
				<button type="submit" name="date" value="range"
				        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
				</button>
			</span>
			</div>
		</div>

	</div>

	<br><br>

<div class="row">
	<div class="form-inline clearfix">

		E-mail&nbsp;
		<div class="input-group">
			<input type="text" class="form-control" name="email" value="<?=$email?>">
		</div>
		Логин
		<div class="input-group">
			<input type="text" class="form-control" name="account" value="<?=$account?>">
		</div>
	</div>
</div>

<?php
ActiveForm::end();

echo GridView::widget([
	'dataProvider' => $data_provider,
	'columns' => [
		[
			'attribute' => 'account.login',
			'format' => 'raw',
			'value'  => function ($item) {
				/** @var AccountBlocks $item */
				if (is_null($item->account)) {
					return null;
				}
				return '@' . $item->account->login;
			}
		],
		'user.mail',
		'block_date',
		'block_expire',
		'account_media',
		'account_follows',
		'account_followers',
		'num_actions',
		[
			'attribute' => 'message',
			'value'  => function ($item) {
				/** @var AccountBlocks $item */
				return empty($item->message) ? '-' : $item->message;
			}
		],
	]
]);

Pjax::end();
?>