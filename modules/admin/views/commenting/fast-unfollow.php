<?php


/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use app\modules\admin\models\CommentingProject;

$this->title = 'Настройка комментирования';


?>

<h1>Комментирование звёзд <a href="#" class="add-project btn btn-success">Добавить проект</a></h1>

<style>

	.comment-settings {
		padding: 15px;
		border: 5px solid transparent;
		border-radius: 10px;
		box-shadow: inset 0 0 1px #555;
		background-color: #F7F7F7;
		background-clip: padding-box;
		position: relative;
		margin-bottom: 5px;
	}

</style>



<div class="projects-container row">

	<?php

	$projects = CommentingProject::find()->all();

	/* @var $project CommentingProject */
	foreach ($projects as $project) {
		echo $this->render('settings-block', [
			'id' => $project->id,
		]);
	}

	?>

</div>

<script>

	$('.add-project').click(function(){

		$.post(
			'/admin/commenting/new-project/',
			{},
			function (data) {

				$('.projects-container').append(data);

			},
			'json'
		);

		return false;

	});

	$(document).on('change', '.comment-settings input,select', function(){

		var $this = $(this),
			id = $this.closest('.comment-settings').attr('data-id'),
			data = {
				id: id
			};

		data[$this.attr('name')] = $this.val();

		$.post(
			'/admin/commenting/change-value/',
			data,
			function (data)
			{
				console.log(data)
			},
			'json'
		);

		return false;

	});

	$(document).on('click', '.comment-settings [data-action]', function(){

		var $this = $(this),
			container = $this.closest('.comment-settings'),
			id = container.attr('data-id'),
			action = $this.attr('data-action'),
			element = container.find('[data-action = ' + action + ']');

		if (action === 'delete' && !confirm('Проект будет удалён, продолжить?')) {
			return false;
		}

		$.post(
			'/admin/commenting/change-status/',
			{
				id: id,
				action: action
			},
			function (data) {

				if (data.status == 'ok') {
					container.find('[data-action]')
						.removeClass('btn-danger')
						.removeClass('btn-success')
						.removeClass('btn-warning')
						.addClass('btn-default');

					element.removeClass('btn-default');

					switch(action) {
						case 'start':
							element.addClass('btn-success');
							break;
						case 'stop':
							element.addClass('btn-danger');
							break;
						case 'pause':
							element.addClass('btn-warning');
							break;
						case 'delete':
							container.remove();
							break;
						default:
							return false;
							break;
					}
				}

			},
			'json'
		);

		return false;

	});

</script>