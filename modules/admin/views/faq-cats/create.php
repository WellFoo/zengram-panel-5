<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FAQCats */

$this->title = 'Create Faqcats';
$this->params['breadcrumbs'][] = ['label' => 'Faqcats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faqcats-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
