<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Proxy;
use app\models\ProxyCat;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class ProxyController extends Controller
{
	/**
	 * Initializes the object.
	 * This method is invoked at the end of the constructor after the object is initialized with the
	 * given configuration.
	 */
	public function init()
	{
		parent::init();

		if (!Yii::$app->user->identity->isAdmin) {
			throw new ForbiddenHttpException('У вас нет прав для просмотра этой страницы.');
		}
	}

	public function actionIndex()
	{
		$cat_id = Yii::$app->request->get('cat', null);
		$proxies = Proxy::getProxies($cat_id);

		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return $proxies;
		}

		$cats = ProxyCat::find()
			->orderBy(['default' => SORT_DESC, 'name' => SORT_ASC])
			->all();

		return $this->render('index', [
			'cats' => $cats,
			'currentCatId' => $cat_id,
			'proxies' => $proxies
		]);
	}

	public function actionCheck($id)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		/* @var $proxy Proxy */
		$proxy = Proxy::findOne($id);
		if ($proxy === null) {
			throw new NotFoundHttpException();
		}

		return [
			'id' => $id,
			'status' => $proxy->checkStatus() ? Proxy::STATUS_OK : Proxy::STATUS_ERROR
		];
	}

	public function actionCreate()
	{
		$model = new Proxy();
		$model->load(Yii::$app->request->post());

		if ($model->validate() && $model->save()) {
			return Yii::$app->response->redirect(['admin/proxy', 'cat' => $model->cat_id]);
		} else {
			return Yii::$app->response->redirect(['admin/proxy']);
		}
	}

	public function actionValidate()
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException('AJAX only');
		}
		Yii::$app->response->format = Response::FORMAT_JSON;

		$model = new Proxy();
		$model->load(Yii::$app->request->post());

		return ActiveForm::validate($model);
	}


	public function actionRemove()
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException('AJAX only');
		}
		Yii::$app->response->format = Response::FORMAT_JSON;

		$id = ArrayHelper::getValue(Yii::$app->request->post(), 'id', null);
		if ($id === null) {
			throw new BadRequestHttpException('Params mismatch');
		}

		/** @var Proxy $proxy */
		$proxy = Proxy::findOne(['id' => $id]);
		Proxy::spreadProxy($proxy);
		$proxy->delete();

		return ['status' => 'ok'];
	}

}