<?php

namespace app\modules\admin\controllers;

use app\models\Hashtags;
use app\modules\admin\models\HashtagsSearch;
use Yii;
use app\models\FAQ;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FaqController implements the CRUD actions for FAQ model.
 */
class HashTagsController extends Controller
{

	public function actionIndex()
	{
		// Выводим список городов и количество хэштегов по ним
		$q = 'SELECT DISTINCT place_id, sum(counter) as total, place.name FROM hashtags_places
LEFT JOIN place ON place.id=hashtags_places.place_id GROUP BY place_id, place.name ORDER BY total DESC';
		$hashtags = Yii::$app->db->createCommand($q)
			->queryAll();
		//echo 'Хэштеги';
		return $this->render('index', [
			'hashtags' => $hashtags
		]);
	}

	public function actionInRegion($id){


		// Получаем название региона
		$q = "SELECT name FROM place WHERE id = $id";
		$result = Yii::$app->db->createCommand($q)
			->queryOne();
		$place = $result['name'];


		// Ищем id-ники хэштегов, которые надо выгрузить

		$q = "SELECT hashtags_id FROM hashtags_places WHERE place_id = $id";
		$result = Yii::$app->db->createCommand($q)
			->queryAll();

		//Получаем id-ники хэштегов
		$ids = array_column($result, 'hashtags_id');

		$searchModel = new HashtagsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ids);

		return $this->render('hashtags-by-place', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'place' => 'Хэштеги города '.$place,
			'place_id' => $id
		]);

	}
	public function actionHashtagCommunications($id){

		$q = "SELECT name FROM hashtags WHERE id=$id";
		$result = Yii::$app->db->createCommand($q)
			->queryAll();
		$hashtagName = $result[0]['name'];


		$place = Yii::$app->request->get('place', null);


		$q = "SELECT name FROM place WHERE id=$place";
		$result = Yii::$app->db->createCommand($q)
			->queryAll();
		$placeName = $result[0]['name'];


		$q = "SELECT * FROM hashtags_relations WHERE $id = ANY (hashtags_ids)";
		$result = Yii::$app->db->createCommand($q)
			->queryAll();
		//$regionFilter = false;
		// Формируем единый




		$captions = [];
		$htFreqs = [];
		foreach($result as $row){
			if (!is_null($place)){
				if($row['place_id'] != $place){
					continue;
				}
			}

			$htArr = explode(',', trim($row['hashtags_ids'], '{}'));
			$captions[] = [
				'place_id' => $row['place_id'],
				'hashtags_ids' => $htArr,
				'counter' => $row['counter']
			];
			foreach($htArr as $htId){
				if (!isset($htFreqs[$htId])){
					$htFreqs[$htId] = $row['counter'];
				}else{
					$htFreqs[$htId] += $row['counter'];
				}

			}


		}

		//echo '<pre>';

		arsort($htFreqs);
		$hKeys = array_keys($htFreqs);

		//print_r($hKeys);

		//echo '</pre>';
		/*
		$uniksHt = [];

		foreach($captions as $){

		}
		*/

		$searchModel = new HashtagsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $hKeys);

		return $this->render('hashtags-by-place', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'place' => 'Хэштеги, встречающиеся вместе с тегом '.$hashtagName,
			'placeName' => $placeName,
			'place_id' => $place
		]);

	}
//explode(',', trim($str, '{}'))

}
