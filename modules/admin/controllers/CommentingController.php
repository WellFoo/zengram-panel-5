<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\modules\admin\models\CommentingProject;
use Yii;
use yii\web\Response;

class CommentingController extends Controller
{
	public function actionIndex()
	{

		return $this->render('fast-unfollow');

	}

	public function actionChangeStatus()
	{

		Yii::$app->response->format = Response::FORMAT_JSON;

		$id     = Yii::$app->request->post('id', false);
		$action = Yii::$app->request->post('action', false);

		if (!$id || !$action) {
			return [
				'status' => 'error',
			];
		}

		/* @var $project CommentingProject */
		$project = CommentingProject::findOne($id);

		if (is_null($project)) {
			return [
				'status' => 'error',
			];
		}

		switch ($action) {
			case 'delete':

				if (!$project->delete()) {
					return [
						'status' => 'error',
					];
				} else {
					return [
						'status' => 'ok',
					];
				}

				break;
			case 'start':
				$project->cron_status = CommentingProject::STATUS_ACTIVE;
				break;
			case 'stop':
				$project->cron_status = CommentingProject::STATUS_STOP;
				break;
			case 'pause':
				$project->cron_status = CommentingProject::STATUS_PAUSE;
				break;
			default:
				break;
		}

		if (!$project->save()) {
			return [
				'status' => 'error',
			];
		}

		return [
			'status' => 'ok',
		];

	}

	public function actionChangeValue()
	{

		Yii::$app->response->format = Response::FORMAT_JSON;

		$id = Yii::$app->request->post('id', 0);

		if ($id === 0) {
			return [
				'status' => 'error',
			];
		}

		/* @var $project CommentingProject */
		$project = CommentingProject::findOne($id);

		if (is_null($project)) {
			return [
				'status' => 'error',
			];
		}

		$project->load(Yii::$app->request->post());

		if ($project->save()) {
			return [
				'status' => 'ok',
			];
		}

		return [
			'status' => 'error',
		];

	}

	public function actionNewProject()
	{

		Yii::$app->response->format = Response::FORMAT_JSON;

		return $this->renderPartial('settings-block', [
			'id' => CommentingProject::createDefault(),
		]);

	}
}