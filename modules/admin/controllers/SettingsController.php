<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Settings;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class SettingsController extends Controller
{
	/**
	 * Initializes the object.
	 * This method is invoked at the end of the constructor after the object is initialized with the
	 * given configuration.
	 */
	public function init()
	{
		parent::init();

		if (!Yii::$app->user->identity->isAdmin) {
			throw new ForbiddenHttpException('У вас нет прав для просмотра этой страницы.');
		}
	}

	public function actionIndex()
	{
		$settings         = Settings::find()->where('pause IS NULL')->asArray()->all();
		$settings_pause_0 = Settings::find()->where(['pause' => 0])->asArray()->all();
		$settings_pause_1 = Settings::find()->where(['pause' => 1])->asArray()->all();
		$settings_pause_2 = Settings::find()->where(['pause' => 2])->asArray()->all();

		return $this->render('index',
			[
				'settings'         => $settings,
				'settings_pause_0' => $settings_pause_0,
				'settings_pause_1' => $settings_pause_1,
				'settings_pause_2' => $settings_pause_2,
			]);
	}

	public function actionEdit($id = null)
	{

		if ($id == null) {
			$model = new Settings();
		} else {
			$model = Settings::findOne($id);
		}

		if ($model === null) {
			throw new NotFoundHttpException('Model not found.');
		}

		if (isset($_POST['Settings'])) {
			$model->load($_POST);
			if ($model->save()) {
				Yii::$app->session->setFlash('success', 'Model has been saved');
				Yii::$app->response->redirect(['/admin/settings']);
			} else {
				Yii::$app->session->setFlash('error', 'Model could not be saved');
			}
		}

		return $this->render('edit', [
			'model' => $model,
		]);
	}
}