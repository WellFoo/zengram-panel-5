<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\modules\admin\models\AntispamLog;
use app\modules\admin\models\AntispamLogSearch;
use app\models\Account;
use app\models\AccountStartPlaceStats;
use app\models\AccountStats;
use app\models\ActionsLog;
use app\models\SupportRequest;
use app\models\Users;
use app\modules\admin\models\AccountBlocks;
use app\modules\admin\models\AccountStatsSearch;
use app\models\Actions;
use app\models\Invoice;
use app\models\Place;
use app\models\Prices;
use app\models\Proxy;
use app\models\Regions;
use app\models\Statistics;
use app\modules\admin\models\OverallStatistics;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use Yii;

class AntispamController extends Controller
{
	public function actionIndex()
	{
		$searchModel = new AntispamLogSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);


		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,]
		);
	}
	public function actionSettings(){
		return $this->render('settings');
	}

}