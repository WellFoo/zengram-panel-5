<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\UsersSearchReport;
use Yii;
use app\models\FAQ;
use app\modules\admin\models\UsersSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use XLSXWriter;
use app\modules\admin\models\Balance;
use yii\helpers\Html;

/**
 * FaqController implements the CRUD actions for FAQ model.
 */
class ReportController extends Controller
{
	private $dateFrom;
	private $dateTo;
	private $reportType;
	private $date;

	/**
	 * Initializes the object.
	 * This method is invoked at the end of the constructor after the object is initialized with the
	 * given configuration.
	 */
	public function init()
	{
		parent::init();

		if (!Yii::$app->user->identity->isAdmin) {
			throw new ForbiddenHttpException('У вас нет прав для просмотра этой страницы.');
		}
	}


	/**
	 * Lists all FAQ models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		if (empty($this->reportType)) {
			$this->reportType = 'registered';
		}
		$this->reportType = Yii::$app->request->get('type-report', 'registered');
		$this->initDatePeriod();

		$searchModel = new UsersSearchReport();
		//$params = Yii::$app->request->get();
		$dataProvider = $searchModel->search();


		$forViewArr['dateTo'] = $this->dateTo;
		$forViewArr['dateFrom'] = $this->dateFrom;
		$forViewArr['date'] = $this->date;
		$forViewArr['reportType'] = $this->reportType;
		$forViewArr['ufilter'] = Yii::$app->request->get('ufilter', 0);

		$forViewArr['dataProvider'] = $dataProvider;
		if (Yii::$app->request->get('download', 0) === 0){
			switch ($this->reportType) {
				case 'registered':
					return $this->render('registered', $forViewArr);
					break;
				case 'payed':
					return $this->render('payed', $forViewArr);
					break;
				case 'doaction':
					return $this->render('doaction', $forViewArr);
					break;
			}
		} else {
			$this->download($dataProvider);
		}

		/*
		switch($this->reportType){
			case 'registered':
				echo $this->reportType;
				return $this->render('registered', $forViewArr);
				break;
		}
		*/
		/*
		 Возможность выгрузки:
		1) зарегистрированных за период
		2) оплативших за период с указанием дат, сумм оплат, кол-ва оплат, общей суммы оплат по пользователю
		3) совершивших какое-либо действие за указанный период
		Соответственно эти фильтры должны комбинироваться.
		Т.е. должна быть возможность получить список пользователей, которые оплачивали 1 и более раза, и которые не совершали действия последний месяц.
		Либо которые оплачивали 1 и более раза, имеют нулевой баланс и не совершали действий за определенный промежуток времени
		*/
		//return $this->render('index');
	}

	private function getData()
	{
		return false;
	}

	private function initDatePeriod()
	{
		$this->date = Yii::$app->request->get('date', 'day');
		$this->dateTo = strtotime(date('Y-m-d'));
		switch ($this->date) {
			case 'day':
			default:
				$this->dateFrom = $this->dateTo - 86400;
				break;

			case 'week':
				$this->dateFrom = $this->dateTo - 86400 * 7;
				break;

			case 'month':
				$this->dateFrom = $this->dateTo - 86400 * 30;
				break;

			case 'range':
				$this->dateFrom = strtotime(Yii::$app->request->get('from'));
				$this->dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}
	}

	public function download($dataProvider){

		$dataProvider->pagination = false;
		$data = [];
		$data[] = ['ID', 'Mail', 'Кол-во платежей','Платежи', 'Баланс', 'Дата регистрации'];
		foreach ($dataProvider->models as $key => $myModel) {

			if (count($myModel->invoices) !== 0) {
				$allPayments = '';
				foreach ($myModel->invoices as $invoice) {
					$allPayments .= '(';
					$allPayments .=  $invoice->sum . ' | ' . $invoice->agent;
					$date = new \DateTime($invoice->date);
					$allPayments .= ' | ' . $date->format('Y-m-d H:i:s') . ') ';

				}
			}
			else{
				$allPayments = 'Нет';
			}

			$balance = Balance::formatBalance($myModel->balance);
			$regDate = $date = new \DateTime($myModel->regdate);
			$data[] = [$myModel->id, $myModel->mail, count($myModel->invoices) ,$allPayments, $balance, $regDate->format('Y-m-d H:i:s')];
		}


		$file = 'Report.xlsx';

		$writer = new XLSXWriter();
		$writer->writeSheet($data);

		$writer->writeToFile($file);

		// заставляем браузер показать окно сохранения файла
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename=' . basename($file));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));

		// читаем файл и отправляем его пользователю
		readfile($file);

		unlink($file);
	}

	public function actionDownloadTest()
	{

		if (empty($this->reportType)) {
			$this->reportType = 'registered';
		}
		$this->reportType = Yii::$app->request->get('type-report', 'registered');
		$this->initDatePeriod();


		$searchModel = new UsersSearchReport();
		//$params = Yii::$app->request->get();
		$dataProvider = $searchModel->search();


		$forViewArr['dateTo'] = $this->dateTo;
		$forViewArr['dateFrom'] = $this->dateFrom;
		$forViewArr['date'] = $this->date;
		$forViewArr['reportType'] = $this->reportType;
		$forViewArr['ufilter'] = Yii::$app->request->get('ufilter', 0);

		$forViewArr['dataProvider'] = $dataProvider;

		$dataProvider->pagination = false;


		$data = [];
		$data[] = ['ID', 'Mail', 'Кол-во платежей','Платежи', 'Баланс', 'Дата регистрации'];
		foreach ($dataProvider->models as $key => $myModel) {

			if (count($myModel->invoices) !== 0) {
				$allPayments = '';
				foreach ($myModel->invoices as $invoice) {
					$allPayments .= '(';
					$allPayments .=  $invoice->sum . ' | ' . $invoice->agent;
					$date = new \DateTime($invoice->date);
					$allPayments .= ' | ' . $date->format('Y-m-d H:i:s') . ') ';

				}
			}
			else{
				$allPayments = 'Нет';
			}

			$balance = Balance::formatBalance($myModel->balance);
			$regDate = $date = new \DateTime($myModel->regdate);
			$data[] = [$myModel->id, $myModel->mail, count($myModel->invoices) ,$allPayments, $balance, $regDate->format('Y-m-d H:i:s')];
		}


		$file = 'Report.xlsx';

		$writer = new XLSXWriter();
		$writer->writeSheet($data);

		$writer->writeToFile($file);

		// заставляем браузер показать окно сохранения файла
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename=' . basename($file));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));

		// читаем файл и отправляем его пользователю
		readfile($file);

		unlink($file);

	}


	/*
		public function actionA()
		{
			$data = self::getData();

			switch ($param) {
				case '':
					$page = $this->foo($data);
					break;
				case '':
					$page = $this->foo($data);
					break;
				default:
					return;
					break;
			}

			return $this->render('main_view', [
				'grid' => $page['table'],
				'form' => self::generateForm($page['form']),
			]);

		}

		public function foo($data)
		{
			return [
				'form' => [
					'foo' => 'text',
					'bar' => 'data'
				],
				'table' => $this->renderFile('...', [
					'data' => $data
				])
			];
		}
	*/
}
