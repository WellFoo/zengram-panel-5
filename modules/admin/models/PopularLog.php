<?php

namespace app\modules\admin\models;

use app\models\Users;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "popular_log".
 *
 * @property integer $id
 * @property integer $instagram_id
 * @property integer $user_id
 * @property string $date
 * @property integer $comments
 * @property integer $media_update
 * @property integer $project_id
 *
 * @property PopularAccounts $account
 * @property Users $user
 */
class PopularLog extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'popular_log';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			//[['instagram_id', 'user_id', 'date', 'comments', 'media_update'], 'required'],
			[['instagram_id', 'date', 'comments', 'media_update'], 'required'],
			[['instagram_id', 'user_id', 'comments', 'media_update', 'project_id'], 'integer'],
			[['date'], 'safe'],
			//[['instagram_id', 'user_id', 'date'], 'unique', 'targetAttribute' => ['instagram_id', 'user_id', 'date'], 'message' => 'The combination of Instagram ID, User ID and Date has already been taken.']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'           => 'ID',
			'instagram_id' => 'Instagram ID',
			'user_id'      => 'User ID',
			'date'         => 'Дата',
			'comments'     => 'Оставлено комментариев',
			'media_update' => 'Обновлений медиа',
		];
	}

	public function getAccount()
	{
		return self::hasOne(PopularAccounts::className(), ['instagram_id' => 'instagram_id']);
	}

	public function getUser()
	{
		return self::hasOne(Users::className(), ['id' => 'user_id']);
	}
}
