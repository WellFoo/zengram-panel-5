<?php

namespace app\modules\admin\models;

use app\models\Account;
use app\models\Users;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "accounts_grid".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $account_id
 * @property string  $email
 * @property string  $email_pwd
 * @property string  $donor_url
 * @property string  $sign
 * @property string  $sign_url
 * @property integer $mark_number
 * @property string  $description
 * @property integer $create_date
 *
 * @property \app\models\Users   $user
 * @property \app\models\Account $account
 */
class AccountsGrid extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'accounts_grid';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'account_id', 'email', 'email_pwd', 'donor_url', 'sign_url', 'mark_number', 'create_date'], 'required'],
			[['user_id', 'account_id', 'mark_number', 'create_date'], 'integer'],
			['email', 'email'],
			[['donor_url', 'sign_url'], 'url'],
			[['sign', 'description'], 'string'],
			[['email', 'email_pwd', 'donor_url', 'sign_url'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'user_id' => 'User ID',
			'account_id' => 'Account ID',
			'email' => 'E-Mail',
			'email_pwd' => 'E-Mail пароль',
			'donor_url' => 'Донор',
			'sign' => 'Подпись',
			'sign_url' => 'Ссылка в подписи',
			'mark_number' => '№ метки',
			'description' => 'Отметки',
			'create_date' => 'Дата запуска'
		];
	}

	public function getUser()
	{
		return $this->hasOne(Users::className(), ['id' => 'user_id']);
	}

	public function getAccount()
	{
		return $this->hasOne(Account::className(), ['id' => 'account_id']);
	}

}