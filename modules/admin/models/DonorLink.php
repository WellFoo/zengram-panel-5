<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "donor_links".
 *
 * @property integer $id
 * @property integer $donor_id
 * @property string $link
 *
 * @property Donor $donor
 */
class DonorLink extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'donor_links';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['donor_id', 'link'], 'required'],
			[['donor_id'], 'integer'],
			[['link'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'donor_id' => 'Donor ID',
			'link' => 'Link',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDonor()
	{
		return $this->hasOne(Donor::className(), ['id' => 'donor_id']);
	}
}
