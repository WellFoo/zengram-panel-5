<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "popular_accounts_comments".
 *
 * @property integer $id
 * @property string  $popular_id
 * @property string  $user_id
 * @property string  $comment_id
 * @property string  $media_id
 * @property integer $expire_date
 * @property integer $project_id
 * @property integer $try_count
 *
 */
class PopularAccountsComments extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	const DEFAULT_TRY_COUNT = 2;


	public static function tableName()
	{
		return 'popular_accounts_comments';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['popular_id'], 'required'],
			[['comment_id', 'media_id', 'popular_id', 'user_id'], 'string'],
			[['id', 'expire_date', 'try_count', 'project_id'], 'integer']
		];
	}

	public function actionTry()
	{
		$this->try_count--;

		if ($this->try_count <= 0) {
			$this->delete();
		} else {
			$this->save();
		}
	}
}
