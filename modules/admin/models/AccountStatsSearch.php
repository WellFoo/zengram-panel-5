<?php namespace app\modules\admin\models;

use app\models\Account;
use app\models\AccountStats;
use app\models\Users;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class AccountStatsSearch extends Model
{
	public $account_id;
	public $mail;
	public $login;

	public $date;
	public $dateFrom;
	public $dateTo;

	public $instagram_id;

	public $trial;
	public $is_service;

	public function rules()
	{
		return [
			[['mail', 'login', 'account_id', 'trial', 'is_service'], 'safe'],
			['account_id', 'integer'],
		];
	}

	public function search($params = null)
	{
		$this->date = Yii::$app->request->get('date', 'day');

		$query = AccountStats::find()
			->select([
				'ast.account_id',
				'ast.user_id',
				'ast.instagram_id',
				//'ast.date',
				'speed',
				'SUM(usedlist_reseted) as usedlist_reseted',
				'SUM(follows_private) as follows_private',
				'COUNT(DISTINCT ast.account_id) as count',
				'SUM(ast.likes) as likes',
				'SUM(ast.comments) as comments',
				'SUM(ast.follows) as follows',
				'SUM(ast.unfollows) as unfollows',
				'SUM(photos) as photos',
				'SUM(ast.manual_comments) as manual_comments',
				'SUM(ast.password_resets) as password_resets',
				'AVG(ast.speed) as speeds',
				'trial',
				'u.is_service',
				'SUM(ast.came) as came',
				'SUM(ast.gone) as gone',
				'SUM(ast.mongo_came) as mongo_came',
				'SUM(ast.mongo_gone) as mongo_gone',
				'SUM(ast.likes_time) as likes_time',
				'SUM(ast.comments_time) as comment_time',
				'SUM(ast.follows_time) as follow_time',
				'SUM(ast.unfollows_time) as unfollow_time',
			])
			->from(['ast' => AccountStats::tableName()])
			->innerJoin(['a' => 'accounts'], 'ast.account_id = a.id')
			->innerJoin(['u' => 'users'], 'ast.user_id = u.id')
			->orWhere(['!=', 'ast.likes', 0])
			->orWhere(['!=', 'ast.comments', 0])
			->orWhere(['!=', 'ast.follows', 0])
			->orWhere(['!=', 'ast.unfollows', 0]);

		$this->dateTo = strtotime(date('Y-m-d'));
		switch ($this->date) {
			case 'day':
			default:
				$this->dateFrom = $this->dateTo - 86400;
				break;

			case 'week':
				$this->dateFrom = $this->dateTo - 86400 * 7;
				break;

			case 'month':
				$this->dateFrom = $this->dateTo - 86400 * 30;
				break;

			case 'range':
				$this->dateFrom = strtotime(Yii::$app->request->get('from'));
				$this->dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}

		$query->andWhere(['>=', 'date', date('Y-m-d', $this->dateFrom)])
			->andWhere(['<', 'date', date('Y-m-d', $this->dateTo)]);

		$dataProvider = new ActiveDataProvider([
			'query' => $query->groupBy('account_id'),
		]);

		if ($params === null) {
			$params = \Yii::$app->request->queryParams;
		}

		if (!$this->load($params) || !$this->validate()) {
			return $dataProvider;
		}

		if ($this->mail) {
			$query->andWhere(['ilike', 'u.mail', trim($this->mail)]);
		}
		if ($this->account_id) {
			$query->andWhere(['account_id' => trim($this->account_id)]);
		}
		if (!is_null($this->trial)) {
			$query->andWhere(['trial' => $this->trial]);
			$query->andWhere(['is_service' => 0]);
		}
		if ($this->is_service) {
			$query->andWhere(['is_service' => $this->is_service]);
		}
		if ($this->login) {
			$query->andFilterWhere([
				'or',
				['ast.instagram_id' => intval($this->login)],
				['ilike', 'a.login', trim($this->login)]
			]);
		}

		return $dataProvider;
	}
}