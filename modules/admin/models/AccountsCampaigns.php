<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "accounts_campaigns".
 *
 * @property integer $id
 * @property string $name
 * @property integer $accountIdBeforeTrig
 * @property integer $frequencyOfPosting
 * @property integer $limitfollow
 * @property integer $likeLimit
 * @property integer $limitLike
 * @property string $tags
 * @property integer $fpopularFrom
 * @property integer $fpopularTo
 * @property integer $frecomendedFrom
 * @property integer $frecomendedTo
 * @property integer $followersTrigger
 * @property integer $daysTrigger
 * @property integer $hourPassTrigger
 * @property integer $hourBanTrigger
 */
class AccountsCampaigns extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accounts_campaigns';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'accountIdBeforeTrig', 'frequencyOfPosting', 'limitFollow', 'limitLike', 'fpopularFrom', 'fpopularTo', 'frecomendedFrom', 'frecomendedTo', 'followersTrigger', 'daysTrigger', 'hourPassTrigger', 'hourBanTrigger'], 'required'],
            [['accountIdBeforeTrig', 'frequencyOfPosting', 'limitFollow', 'limitLike', 'fpopularFrom', 'fpopularTo', 'frecomendedFrom', 'frecomendedTo', 'followersTrigger', 'daysTrigger', 'hourPassTrigger', 'hourBanTrigger'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['tags'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя кампании',
            'accountIdBeforeTrig' => 'После срабатывания триггеров передать на аккаунт с id',
            'frequencyOfPosting' => 'Частота пости',
            'limitFollow' => 'Лимит Follow',
            'limitLike' => 'Лимит Like',
            'tags' => 'Тэги',
            'fpopularFrom' => 'Популярные от',
            'fpopularTo' => 'Поплулярныее до',
            'frecomendedFrom' => 'Рекомендованные от',
            'frecomendedTo' => 'Рекомендованные до',
            'followersTrigger' => 'Срабатывания тригера после фоловера',
            'daysTrigger' => 'Тригер дней',
            'hourPassTrigger' => 'Hour Pass Trigger',
            'hourBanTrigger' => 'Hour Ban Trigger',
        ];
    }
}
