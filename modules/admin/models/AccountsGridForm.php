<?php

namespace app\modules\admin\models;

use app\models\Account;
use app\models\Users;
use yii\base\Model;

class AccountsGridForm extends Model
{
	public $grid;

	public $user_id;
	public $account_login;
	public $account_password;
	public $email;
	public $email_password;
	public $donor_url;
	public $sign;
	public $sign_url;
	public $mark_number;
	public $description;

	public function rules()
	{
		return [
			[['user_id', 'account_login', 'account_password', 'email', 'email_password', 'donor_url', 'mark_number'], 'required'],
			[['user_id', 'mark_number'], 'integer'],
			['email', 'email'],
			[['donor_url', 'sign_url'], 'url'],
			[['sign', 'description'], 'string'],
			[['email', 'email_password', 'donor_url', 'sign_url'], 'string', 'max' => 255],
			['account_login', 'unique', 'targetClass' => Account::className(), 'targetAttribute' => 'login', 'on' => 'create'],
			['account_password', 'validateAccount', 'on' => 'create']
		];
	}

	public function scenarios()
	{
		return [
			'create' => [
				'user_id', 'account_login', 'account_password', 'email', 'email_password', 'donor_url', 'sign',
				'sign_url', 'mark_number', 'description'
			],
			'update' => [
				'user_id', 'email', 'email_password', 'donor_url', 'sign', 'sign_url', 'mark_number', 'description'
			]
		];
	}

	public function validateAccount($attribute, $params)
	{
		if (!$this->hasErrors()) {
			$account = new Account();
			$account->login = $this->account_login;
			$account->password = $this->account_password;
			if (Users::findOne(['id' => $account->user_id])->is_service){
				$result = $account->checkCredentials(true);
			} else {
				$result = $account->checkLogin(true);
			}
			if (!$result) {
				$this->addError($attribute, 'Неправильные имя пользователя и/или пароль');
			}
		}
	}

	public function attributeLabels()
	{
		return [
			'user_id' => 'Пользователь',
			'account_login' => 'Логин',
			'account_password' => 'Пароль',
			'email' => 'E-Mail',
			'email_password' => 'Пароль',
			'donor_url' => 'Ссылка на донора',
			'sign' => 'Подпись',
			'sign_url' => 'Ссылка в подписи',
			'mark_number' => '№ метки',
			'description' => 'Отметки'
		];
	}

	public function save()
	{
		if (!$this->validate()) {
			return false;
		}

		if ($this->scenario === 'create') {
			$account = new Account();
			$account->login = $this->account_login;
			$account->password = $this->account_password;
			if (!$account->add($this->user_id)) {
				$this->addError('account_login', 'Не удалось создать аккаунт');
				return false;
			}

			$account->fillFirmComments();
			$this->grid = new AccountsGrid();
			$this->grid->account_id  = $account->id;
			$this->grid->create_date = time();
		}

		$this->grid->user_id     = $this->user_id;
		$this->grid->email       = $this->email;
		$this->grid->email_pwd   = $this->email_password;
		$this->grid->donor_url   = $this->donor_url;
		$this->grid->sign        = $this->sign;
		$this->grid->sign_url    = $this->sign_url;
		$this->grid->mark_number = $this->mark_number;
		$this->grid->description = $this->description;

		if (!$this->grid->save()) {
			$this->addError('account_login', 'Не удалось создать модель');
			if (isset($account)) {
				$account->delete();
			}
			return false;
		}

		return true;
	}
}