<?php

namespace app\modules\admin\models;

use app\models\Account;
use app\models\Invoice;
use app\models\Users;
use Yii;
use yii\db\Expression;

class OverallStatistics
{
	CONST ALLTIME_INTERVAL = 0;
	CONST WEEK_INTERVAL = 1;
	CONST MONTH_INTERVAL = 2;
	private $interval;

	/**
	 * OverallStatistics constructor.
	 * @param $interval int
	 */
	function __construct($interval)
	{
		$this->interval = $interval;
	}

	function getMonthStats()
	{
		return $this->getStaticStats(self::MONTH_INTERVAL);
	}

	public static function getStaticStats($interval)
	{
		switch ($interval) {
			case 1:
				$date_interval = "(NOW() - '1 week'::interval)";
				break;
			case 2:
				$date_interval = "(NOW() - '1 month'::interval)";
				break;
			case 0:
			default:
				$date_interval = false;
		}
		$result = new \StdClass();
		if ($date_interval) {
			$result->users = Users::find()->where(['>=', 'regdate', new Expression($date_interval)])->count();
			$result->accounts = Account::find()->where(['>=', 'added', new Expression($date_interval)])->count();
			$result->invoices = Invoice::find()
				->where(['>=', 'date', new Expression($date_interval)])
				->andWhere(['status' => Invoice::STATUS_SUCCESS])
				->count();
		} else {
			$result->users = Users::find()->count();
			$result->accounts = Account::find()->count();
			$result->invoices = Invoice::find()->where(['status' => Invoice::STATUS_SUCCESS])->count();
		}
		$intervals = [
			self::ALLTIME_INTERVAL => 'Статистика за всё время',
			self::WEEK_INTERVAL    => 'Статистика за неделю',
			self::MONTH_INTERVAL   => 'Статистика за месяц'
		];
		$result->date = $intervals[$interval];
		return $result;
	}

	function getWeekStats()
	{
		return $this->getStaticStats(self::WEEK_INTERVAL);
	}

	function getAlltimeStats()
	{
		return $this->getStaticStats(self::ALLTIME_INTERVAL);
	}

	public function getStats()
	{
		return self::getStaticStats($this->interval);
	}
}
