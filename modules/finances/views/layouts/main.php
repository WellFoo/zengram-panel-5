<?php

use app\modules\finances\ModuleAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

ModuleAsset::register($this);
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<style scoped>
	.asc:after {
		content:"\f160";
		display: inline-block;
		font: normal normal normal 14px/1 FontAwesome;
		font-size: inherit;
		text-rendering: auto;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		margin-left: 2px;
	}
	.desc:after {
		content:"\f161";
		display: inline-block;
		font: normal normal normal 14px/1 FontAwesome;
		font-size: inherit;
		text-rendering: auto;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		margin-left: 2px;
	}
</style>
<div class="wrap">
	<?php
	NavBar::begin([
		'brandLabel' => 'Zengram',
		'brandUrl' => Yii::$app->homeUrl,
		'options' => [
			'class' => 'navbar-inverse navbar-fixed-top',
		],
	]);

	$menuItems = [
//		[
//			'label' => 'Панель управления',
//			'url' => ['/admin']
//		], [
//			'label' => 'Комментирование звёзд',
//			'url' => ['/finances/commenting']
//		], [
//			'label' => 'Клиенты',
//			'url' => ['/finances/users']
//		], [
//			'label' => 'Рассылки',
//			'url' => ['/finances/campaigns']
//		], [
//			'label' => 'Скидки',
//			'url' => ['/finances/discounts']
//		],
		[
			'label' => 'Выход (' . Yii::$app->user->identity->mail . ')',
			'url' => ['/site/logout'],
			'linkOptions' => ['data-method' => 'post']
		],
	];

	echo Nav::widget([
		'options' => ['class' => 'navbar-nav navbar-right'],
		'items' => $menuItems,
	]);
	NavBar::end();
	?>

	<div class="container" style="margin-top: 60px;">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<?= $content ?>
	</div>
</div>

<footer class="footer">
	<!--div class="container">
    <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div-->
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
