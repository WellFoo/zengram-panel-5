<?php
namespace app\components;

use yii\web\View;

Class FluidableView extends View {
    public $fluid = false;
}