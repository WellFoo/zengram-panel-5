<?php

namespace app\components;

trait TreeTrait
{
	public static function getTree($selected = [])
	{
		$data = self::find()->orderBy('parent_id, id')->asArray()->all();
		return self::buildTree($data, $selected);
	}

	private static function buildTree(&$data, $selected = [], $parent_id = null, $disabled = false)
	{
		$tree = [];
		$list = self::findByParentId($data, $parent_id);
		foreach ($list as $item) {
			$result = [
				'id'       => $item['id'],
				'label'    => $item['name'],
				'parent'   => $parent_id,
				'selected' => in_array($item['id'], $selected),
				'disabled' => $disabled
			];
			$childItems = self::buildTree($data, $selected, $item['id'], $result['selected']);
			if (count($childItems)) {
				$result['items'] = $childItems;
			}
			$tree[] = $result;
		}
		return $tree;
	}

	private static function findByParentId(&$data, $parent_id)
	{
		$list = [];
		foreach ($data as $key => $item) {
			if ($item['parent_id'] === $parent_id) {
				$list[] = $item;
			}
		}
		return $list;
	}

	public static function renderTree($data, &$list = [], $level = 1) {
		foreach ($data as $item) {
			$list[$item['id']] = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level) . $item['label'];
			if (isset($item['items'])) {
				renderTree($item['items'], $list, $level + 1);
			}
		}
		return $list;
	}
}