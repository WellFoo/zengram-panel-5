<?php

namespace app\components;

class TemplateParser {

	const BASE_TEMPLATE_PATH = 'templates/';
	
	public $tpl_path;
	private $tpl_array;
	public $tpl_count = 0;

	public function __construct($template = false, $path = false) 
	{
		if ($path !== false) {
			$this->tpl_path = $path;
		} else {
			$this->tpl_path = \Yii::getAlias('@webroot/'.self::BASE_TEMPLATE_PATH);
		}
		
		if ($template !== false) {
			$this->loadNewFile($this->tpl_path.$template);
		}
	}

	// Загружает в парсер содержимое нового файла
	public function loadNewFile($path) {

		if (mb_strpos($path, '.tpl') === false) {
			$path .= '.tpl';
		}

		$file = preg_replace('/<!--(.*?)-->/', '', file_get_contents($path));

		$this->tpl_array = explode('--SEPARATOR--', $file);

		$this->tpl_count = count($this->tpl_array);

		for ($i = 0; $i < $this->tpl_count; $i++) {

			$tpl = $this->tpl_array[$i];

			if (preg_match('/--(.*?)--/', $tpl, $matches) == 0)	continue;

			$name = $matches[1];

			$tpl = str_replace('--'.$name.'--', '', $tpl);

			$this->tpl_array[$i] = $tpl;
			$this->tpl_array[$name] = $tpl;
		}
	}

	// Заполняет шаблон с определенным номером данными из массива
	public function fillTemplate($num, $parameters = array(), $mode = 0) {

		if ($this->tpl_count == 0) return false;

		$content = $this->tpl_array[$num];

		if ($content === null) return false;

		$result = trim($content);

		foreach ($parameters as $key => $value) {
			if ($mode == 1 or strpos($key, '%') === false) $key = '%'.$key.'%';
			$result = str_replace($key, $value, $result);
		}
		return $result."\r\n";
	}


	// Заполняет страницу, на вход принимает массив с массивами, содержащими номер определенного шаблона и данные
	public function fillPage(array $data) {

		if ($this->tpl_count == 0) return false;

		$result = '';
		foreach($data as $parameters) {
			$tpl = $parameters[0];
			$param = ($parameters[1] === null) ? array() : $parameters[1];
			$result .= $this->fillTemplate($tpl, $param);
		}

		return $result;
	}

}