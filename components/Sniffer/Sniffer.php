<?php

class Sniffer
{
	const KEY = '2p8923pn2p3p23v9822';

	public $os;
	public $result = [];
	public $config = [];
	public $command_info = [];

	public $data;
	public $action;

	public function __construct($action, $data)
	{
		$this->os = PHP_OS;
		$this->action = $action;
		$this->data = $data;
	}

	public function start()
	{
		$this->actionSwitch();
		echo json_encode($this->result);
	}

	public function setConfig()
	{
		return empty($this->data['config']) ? [] : json_decode($this->data['config'], true);
	}

	public function actionSwitch()
	{
		switch ($this->action) {
			case 'sniff':
				$this->config = $this->setConfig();

				ini_set('display_errors', 1);
				ini_set('track_errors', 1);
				ini_set('html_errors', 1);
				error_reporting(E_ALL);

				$this->sniff();
				break;
			case 'update':
				$this->update();
				break;
			default:
				$this->result = [
					'status' => 'error',
					'data' => 'Недопустимое действие - '.$this->action . ' // ' . json_encode($this->data)
				];
				break;
		}
	}

	public function update()
	{
		$this->result = [
			'status' => 'ok',
			'data' => [],
		];

		$directory = __DIR__;

		foreach ($_FILES as $file => $data) {

			$filename = $directory . DIRECTORY_SEPARATOR . $data['name'];

			$this->result['data'][$file] = [
				//'data' => $data,
				'delete' => unlink($filename),
				'upload' => move_uploaded_file($data['tmp_name'], $filename),
			];
		}
	}

	public function getCommands()
	{
		$data = [
			'mem' => false,
			'cpu' => false,
			'disk' => false,
			'date' => false,
		];

		if (!empty($this->config['add'])) {
			foreach ($this->config['add'] as $command => $info) {
				$data[$command] = false;

				if (empty($info)) {
					continue;
				}

				$this->command_info[$command] = $info;
			}
		}

		return $data;
	}

	public function sniff()
	{
		$data = $this->getCommands();
		foreach ($data as $command => $value) {

			$result = $this->run($command);

			if ($result === false) {
				return false;
			}

			$data[$command] = $result;
		}

		$this->result = [
			'status' => 'ok',
			'data' => $data,
		];

		return true;
	}

	public function specialCommands($command)
	{
		// Команды БД
		$db = $this->dataBaseCommands($command);
		if (!is_null($db)) return $db;

		// Файлы. Бэкапы, кеш, хэш и т.д.
		$files = $this->fileCommands($command);
		if (!is_null($files)) return $files;

		return null;
	}

	public function fileCommands($command)
	{
		if (!in_array($command, ['backup'])) {
			return null;
		}

		$config = $this->command_info[$command];

		switch ($config['type']) {
			case 'mnt-yd':

				$files = array_diff(scandir($config['path']), ['..', '.']);

				$total_size = 0;
				$time = 0;

				foreach ($files as $file) {

					$filename = $config['path'] . DIRECTORY_SEPARATOR . $file;

					if (is_dir($filename)) {
						continue;
					}

					$total_size += filesize($filename);
					$file_time = filectime($filename);
					if ($file_time > $time) $time = $file_time;
				}

				$result = [
					'size' => $total_size,
					'time' => $time,
				];

				break;
			default:
				return false;
				break;
		}

		return $result;
	}

	public function dataBaseCommands($command)
	{
		if (!in_array($command, ['mysql', 'pgsql'])) {
			return null;
		}

		/** @var DB $db */
		$db = new DB($this->command_info[$command]['connection']);

		if ($db === false) {
			$this->result = [
				'status' => 'fail',
				'data' => 'Не удалось открыть соединение с базой',
				'debug_return' => $db->error,
			];
			return false;
		}

		switch ($command) {
			case 'mysql':

				$counter = 0;
				do {
					$result = $db->query('SHOW STATUS LIKE  \'Threads_connected\'');
					if ($result) {
						$result = $result[0]['Value'];
					}
				} while ((is_null($result) || $result === false) && $counter++<3);

				if ($result !== false && !is_null($result)) {
					$result = [
						'count' => $result,
						'status' => 'ok'
					];
				} else {
					$result = [
						'description' => 'Не удалось проверить состояние базы',
						'status' => $db->error
					];
				}

				break;
			case 'pgsql':

				$counter = 0;
				do {
					$result = $db->query('SELECT COUNT(*) FROM pg_stat_activity');
					if ($result) {
						$result = $result[0]['count'];
					}
				} while ((is_null($result) || $result === false) && $counter++<3);

				if ($result !== false && !is_null($result)) {
					$result = [
						'count' => $result,
						'status' => 'ok'
					];
				} else {
					$result = [
						'description' => 'Не удалось проверить состояние базы',
						'status' => 'error'
					];
				}

				break;
			default:
				return false;
				break;
		}

		return $result;
	}

	public function run($command)
	{
		if (!empty($this->config['change'][$command])) {
			$command = $this->config['change'][$command];
		}

		$special = $this->specialCommands($command);
		if (!is_null($special)) return $special;

		if (empty($this->commands()[$command])) {
			$this->result = [
				'status' => 'fail',
				'data' => 'Не найдена команда для запуска: ' . $command,
			];
			return false;
		}

		$line = $this->commands()[$command];

		$result = exec($line, $output, $return_val);

		if ($return_val != 0) {

			$this->result = [
				'status' => 'fail',
				'data' => 'Ошибка при выполнении команды: ' . $line,
				'debug_return' => $return_val,
				'debug_status' => $result,
				'debug_output' => $output,
			];

			return false;
		} else {
			return $result;
		}
	}

	public function commands()
	{
		return [
			'mem'  => 'free | grep Mem | awk \'{print ($6+$4)/($2/100)}\'', // Free 3.2.8
			'mem-3.3.10'  => 'free | grep Mem | awk \'{print $7/($2/100)}\'', // Free 3.3.10
			'date'  => 'date',
			'cpu'  => 'grep \'cpu \' /proc/stat | awk \'{usage=($2+$4)*100/($2+$4+$5)} END {print 100-usage}\'',
			'disk' => 'df -h / | grep -E "\W\/$" | awk \'{print $5}\'',
			'projects' => 'ps -ax | grep zengramProcess | grep -v grep | wc -l',
			'rabbit' => 'curl -i -u monitoring:u7kb546hyi http://localhost:15672/api/overview',
			'backup' => 'date',
		];
	}
}