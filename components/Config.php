<?php namespace app\components;
use yii\helpers\ArrayHelper;

class Config
{
	public static function load($config) {
		$file = LANGUAGE_CONFIG_DIR . DIRECTORY_SEPARATOR . $config;
		if (!file_exists($file . '.php')) {
			return [];
		}
		if (file_exists($file . '-local.php')) {
			return ArrayHelper::merge(
				require ($file . '-local.php'),
				require ($file . '.php')
			);
		}
		return require ($file . '.php');
	}
}