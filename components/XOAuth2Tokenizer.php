<?php namespace app\components;

use League\OAuth2\Client\Grant\RefreshToken;
use League\OAuth2\Client\Provider\Google;
use yii\base\Model;

/**
 * Class XOAuth2Tokenizer
 * @package app\components
 * @property string|
 */
class XOAuth2Tokenizer extends Model
{
	private $_client_id;
	private $_client_secret;
	private $_refresh_token;
	private $_access_token;

	public function setClientId($value)
	{
		$this->_client_id = $value;
	}

	public function setClientSecret($value)
	{
		$this->_client_secret = $value;
	}

	public function setRefreshToken($value)
	{
		$this->_refresh_token = $value;
	}

	public function getAccessToken()
	{
		if (empty($this->_access_token)) {
			$this->generateAccessToken();
		}
		return $this->_access_token;
	}

	private function generateAccessToken()
	{
		$provider = new Google([
			'clientId'     => $this->_client_id,
			'clientSecret' => $this->_client_secret,
		]);
		$grant = new RefreshToken();
		$token = $provider->getAccessToken(
			$grant,
			['refresh_token' => $this->_refresh_token,]
		);
		$this->_access_token = $token->getToken();
	}
}