<?php

namespace app\components;
use app\models\Account;
use app\models\Proxy;

/**
 * @property integer $id
 * @property string $instagram_id
 * @property string $login
 * @property string $password Encripted pass
 * @property int $proxy_id
 * @property int $device_id
 * @property string $ip
 * @property int|string user_id
 * @property Users $user
 * @property string $apiKey
 * @property string $server
 * @property string $account_avatar
 * @property string $message
 * @property int $likes
 * @property int $comments
 * @property int $follow
 * @property int $unfollow
 * @property int $password_resets
 * @property int $lastfollows - num follows, when account added
 * @property int $lastfollowers - num followers, when account added
 * @property int $newcomments
 * @property int $posted_photos
 * @property int $manual_comments
 *
 * @property int $account_media
 * @property int $account_follows
 * @property int $account_followers
 * @property int $timer
 * @property int $comment_status
 *
 * @property int $comments_block_date
 * @property int $comments_block_expire
 *
 * @property string $added
 * @property string	$monitoring_status
 */

trait GearManTrait
{

	/* @type GearmanClientSingleton */
	protected $client;

	public $medias = [];
	public $mediaComments;
	public $commentResult;
	public $removeCommentResult;
	public $imageResult;
	public $findUserResult;

	/**
	 * @return \GearmanClient
	 */
	public function getClient()
	{
		return $this->client;
	}

	public function addTask($command, $fields = [], $params = null, $callback = 'jobComplete')
	{
		if (is_null($params)) {
			$params = $this->getServerParams();
		}
		foreach ($fields as $field_name => $field_value) {
			$params[$field_name] = $field_value;
		}

		$this->client->setCompleteCallback([$this, $callback]);
		$this->client->addTask($command, json_encode($params));

		return $this->client->runTasks();
	}

	public function getServerParams()
	{
		if ($this->proxy_id) {
			$proxy = $this->getProxyString($this->proxy_id);
		} else {
			$proxy = '';
		}

		$user_id = $this->retrieveUserInfo('id', 0);
		if (!$user_id){
			$user_id = $this->user_id;
		}
		$antispam = $this->retrieveUserInfo('antispam', 0);

		if (empty($this->ip)) {
			if ($ip = $this->generateIp($user_id)){
				$this->ip = $ip;
				if ($this->id) {
					$this->save();
				}
			} else {
				$this->ip = sprintf('192.168.%d.%d', rand(0, 255), rand(1, 254));
			}
		}

		$password = strlen($this->password) > self::MAX_PASSWORD_LENGTH ? call_user_func(MCRYPT_DECODE, $this->password) : $this->password;
		$data = [
			'ip'           => $this->ip,
			'mail'         => $this->retrieveEmail(),
			'proxy'        => $proxy,
			'login'        => $this->login,
			'user_id'      => $user_id,
			'table_id'     => $this->id,
			'password'     => $password,
			'account_id'   => $this->instagram_id,
			'access_token' => $this->apiKey,
			'experimental' => $this->options->experimental,
			'optionSkipPopularUpdate' => $this->options->skip_popular_update,
			'antispam' => $antispam,
		];

		return $data;
	}

	public function retrieveEmail()
	{
		$data = $this->retrieveUserInfo('identity', null);

		if (!empty($data)) {
			return \Yii::$app->user->identity->mail;
		} else {
			return \Yii::$app->params['adminEmail'];
		}
	}

	public static function getProxyString($proxy_id)
	{
		try{
			$proxyString = Proxy::findOne(['id' => $proxy_id])->proxy;
		}catch (\Exception $e){
			\Yii::error($e);
			return '';
		}
		return $proxyString;
	}

	public function countComments()
	{
		$this->addTask('countComments');
	}

	public function getMedia($max_id = false)
	{
		return $this->addTask('getMedia', [
			'max_id' => $max_id,
		]);
	}

	public function getMediaComments($id, $max_id = false)
	{
		return $this->addTask('getMediaComments', [
			'id'     => $id,
			'max_id' => $max_id,
		]);
	}

	public function sendComment($media_id, $comment_text)
	{
		return $this->addTask('sendComment', [
			'media_id'     => $media_id,
			'comment_text' => $comment_text,
		]);
	}

	public function sendImage($image, $caption, $media_info = [])
	{
		return $this->addTask('sendImage', [
			'image'     => $image,
			'caption'   => $caption,
			'media_info' => $media_info
		]);
	}

	public function removeComment($media_id, $comment_id)
	{
		return $this->addTask('removeComment', [
			'media_id'   => $media_id,
			'comment_id' => $comment_id,
		]);
	}

	public function findUser($login)
	{
		return $this->addTask('findUser', [
			'user_login' => $login
		]);
	}


	/**
	 * @param $job \GearmanTask
	 */
	public function jobComplete($job)
	{
		$data = json_decode($job->data(), true);
		$function = $data['action'] . 'Complete';
		$this->$function($data);
	}

	/**
	 * @param $data array
	 */
	public function startComplete($data)
	{
		$this->server = $data['server'];
		//Ставим отметку о запуске проекта
		$this->monitoring_status = Account::STATUS_WORK;
		$this->save();
	}

	/**
	 * @param $data array
	 */
	public function stopComplete($data)
	{
		//Ставим отметку об остановке проекта
		$this->monitoring_status = Account::STATUS_IDLE;
		$this->save();
	}

	/**
	 * @param $data array
	 */
	public function mediaComplete($data)
	{
		$this->medias = $data['data'];
	}

	public function commentsComplete($data)
	{
		$this->mediaComments = $data['data'];
	}

	public function commentComplete($data)
	{
		$this->commentResult = $data['data'];
	}

	public function imageComplete($data)
	{
		$this->imageResult = $data['data'];
	}

	public function removeCommentComplete($data)
	{
		$this->removeCommentResult = $data['data'];
	}

	public function findUserComplete($data)
	{
		$this->findUserResult = $data['data'];
	}

	public function countCommentsComplete($data)
	{
		sleep(mt_rand(0, 10));
	}
}