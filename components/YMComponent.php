<?php

namespace app\components;

use \YandexMoney\API;
use YandexMoney\ExternalPayment;
use yii\base\Component;
use yii\base\InvalidConfigException;

class YMComponent extends Component
{
	/** @property \YandexMoney\API $api */

	/** @var string */
	public $client_id;

	/** @var string */
	public $code;

	/** @var string */
	public $redirect_uri;

	/** @var null|string */
	public $client_secret = null;

	/** @var \YandexMoney\ExternalPayment */
	private $_external_payment;

	/** @var \YandexMoney\API */
	private $_api = null;

	public function initApi()
	{
		if (!$this->client_id) {
			throw new InvalidConfigException("Client_id can't be empty!");
		}

		if (!$this->code) {
			throw new InvalidConfigException("Code can't be empty!");
		}

		if (!$this->redirect_uri) {
			throw new InvalidConfigException("Redirect_uri can't be empty!");
		}

		$access_token = API::getAccessToken($this->client_id, $this->code, $this->redirect_uri, $this->client_secret);
		$this->_api = new API($access_token);

		parent::init();
	}

	public function initCard($instance_id = null){
		if (empty($instance_id)){
			/** @var \StdClass $instance_result */
			$instance_result = ExternalPayment::getInstanceId($this->client_id);
			if (!empty($instance_result->instance_id)){
				$instance_id = $instance_result->instance_id;
			}
		}
		$this->_external_payment = new ExternalPayment($instance_id);
		
		return $instance_id;
	}
	
	public function getExternal(){
		return $this->_external_payment;
	}

	public function getApi(){
		return $this->_api;
	}
}