<?php
/* @var $prices app\models\Prices[] */

use app\models\Prices;
use yii\helpers\Html;

$app = Yii::$app;
?>

<script>
	var mapsApiKey = '<?= Yii::$app->params['yandex_maps_api_key'] ?>';
</script>
<div class="site-index">
	<?php if (Yii::$app->session->hasFlash('AccountDeletedError')): ?>
    <div class="alert alert-danger">
        <?= Yii::t('views', 'There was an error deleting your account') ?>
    </div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('AccountDeleted')): ?>
    <div class="alert alert-success">
        <?= Yii::t('views', 'Your account has successfully been deleted') ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash('AccountLoginError')): ?>
    <div class="alert alert-warning">
        <?= Yii::$app->session->getFlash('AccountLoginError') ?>
    </div>
<?php endif; ?>

<!-- Sub Header -->
<section class="row sub-header">
    <!-- Btn-cont -->
    <div class="col-xs-6 col-sm-3 va-bottom">

        <?php
        if (!empty($user->allowImages)) {
            echo Html::a(
                Yii::t('views', 'Add photo'),
                ['#'],
                [
                    'class' => 'add-massive-images bot-bor btn btn-success col-xs-12 col-sm-12 col-md-12',
                    'id' => 'addMassiveImages',
                    'data-toggle' => "modal",
                    'data-target' => "#addQueue"
                ]
            );
        }
        echo Html::a(
            Yii::t('views', 'Add account'),
            ['#'],
            [
                'class' => 'addNewAccount add-acount bot-bor btn btn-success col-xs-12 col-sm-12 col-md-12',
                'id' => 'addNewAccount',
                'onClick' => 'return false;',
                'data-toggle' => 'modal',
                'data-target' => count($accounts) > 0 ? '#addAccountMoreOne' : '#addAccount'
            ]
        );

        if (count($accounts) > 1) {
            ?>
            <div class="col-xs-12 col-md-6">
                <div class="row pad-3 first"><?php
                    echo Html::a(
                        Yii::t('views', 'Start all'),
                        ['#startall'],
                        ['class' => 'start-all btn btn-success hidden-xs col-sm-12 col-md-12 ']
                    );
                    ?></div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="row pad-3 last"><?php
                    echo Html::a(
                        Yii::t('views', 'Stop all'),
                        ['#stopall'],
                        ['class' => 'stop-all btn btn-danger hidden-xs col-sm-12 col-md-12']
                    );
                    ?></div>
            </div>
        <?php } ?>

    </div>

    <!-- Counter -->
    <div class="user-balance col-xs-6 col-sm-4 text-center va-center" style="margin: 0 -5px;">
        <p>
            <span class="hidden-xs"><?= Yii::t('views', 'Time limit:') ?></span>
            <span class="visible-xs"><?= Yii::t('views', 'Time:') ?></span>
        </p>
        <?php
        $balance = max((int)$user->balance, 0);
        ?>
        <!-- <?= $balance; ?> -->
        <div class="counter-item days-last">
            <output class="btn btn-default" id="balance_d">
                <?php
                if ($balance > 86400) {
                    echo sprintf("%02d", floor($balance / 86400));
                } else {
                    echo '00';
                }
                ?></output>
            <p><?= Yii::t('views', 'days') ?></p>
        </div>

        <div class="counter-item hours-last">
            <output class="btn btn-default" id="balance_H">
                <?php
                if ($balance > 3600) {
                    $balance = $balance - (floor($balance / 86400) * 86400);
                    echo sprintf("%02d", floor($balance / 3600));
                } else {
                    echo '00';
                }
                ?></output>
            <p><?= Yii::t('views', 'hours') ?></p>
        </div>

        <div class="counter-item minutes-last">
            <output class="btn btn-default" id="balance_i"><?php if ($balance > 0) {
                    echo date("i", $balance);
                } else {
                    echo '00';
                } ?></output>
            <p><?= Yii::t('views', 'minutes') ?></p>
        </div>
    </div>

    <!-- Buy packets -->
    <div class="hidden-xs col-sm-5 text-center va-center">
        <p><?= Yii::t('views', 'Buy time package') ?></p>

        <div class="buy-packets">
            <?php foreach ($prices as $price): ?>
                <div class="col-xs-4">
                    <div class="row pad-3">
                        <p class="days">
                            <strong><?= $price->value ?></strong><?= Yii::t('app', '{n, plural, =0{ # days} one{ # day} few{ # days} many{ # days} other{ # days}}', ['n' => $price->value]) ?>
                        </p>

                        <p class="price">
                            <strong><?= Prices::formatPrice($price->price); ?></strong>
                            <span class="fa <?= Yii::t('views', 'fa-usd') ?>"></span>
                        </p>
                        <a href="/page/price?sel_id=<?= $price->id; ?>"
                           class="bot-bor btn btn-success"><?= Yii::t('views', 'Buy') ?></a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<!-- Sub header end -->