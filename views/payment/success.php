<?php
/* @var $this yii\web\View */
/* @var $model app\models\Invoice */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('views', 'Success payment');

?>

<div class="row">
	<script>
		document.addEventListener("DOMContentLoaded", function(){
			setTimeout(function(){
				var sc = document.createElement('script');
				var appended = document.body.appendChild(sc);
				document.body.lastChild.innerHTML = "window.dataLayer = window.dataLayer || [];yaCounter31898476.reachGoal('<?php	echo $_SESSION['select_for_metrika']['id']?>');	var revenue = <?php	echo $_SESSION['select_for_metrika']['price']?>;var name = 'Оплата за '+('<?php	echo $_SESSION['select_for_metrika']['id']?>'.replace(/dn/i,''))+'д.';dataLayer.push({'ecommerce': {'currencyCode': 'RUB','purchase' : {'actionField' : {'id':'TRX#<?php echo rand(1,99999)?>',	'revenue':<?php	echo $_SESSION['select_for_metrika']['price']?>},'products' : [{'id':'<?php	echo $_SESSION['select_for_metrika']['id']?>','position':1,	'price':<?php	echo $_SESSION['select_for_metrika']['price']?>,'name':'<?php	echo $_SESSION['select_for_metrika']['id']?>'}]}}});";
			},300);
		});
	</script>
	<div class="text-center h2">
		<?= Yii::t('views', 'Thank you for your purchase!'); ?>
	</div>
	<div class="row h4">
		<div class="col-xs-3"></div>
		<div class="col-xs-4"><?= Yii::t('views', 'Your order'); ?></div>
		<div class="col-xs-4"><?= ($model->price_id == 9) ? 'Пополнение баланса на 3 анализа в instashpion' : '' ?></div>
	</div>
	<div class="row h4">
		<div class="col-xs-3"></div>
		<div class="col-xs-4"><?= Yii::t('views', 'Date'); ?></div>
		<div class="col-xs-4"><?= $model->date ?></div>
	</div>
	<div class="row h4">
		<div class="col-xs-3"></div>
		<div class="col-xs-4"><?= Yii::t('views', 'Total'); ?></div>
		<div class="col-xs-4"><?= $model->sum ?><span class="fa <?= Yii::t('views', 'fa-usd') ?>"></span></div>
	</div>
	<div class="text-center h4">
		<?= Yii::t('views', 'You will be redirected to dashboard in a few seconds'); ?>
	</div>
</div>
<script>
	$(document).ready(function(){
		setTimeout(function(){
			window.location.href = '<?= ($model->price_id == 9) ? '/instashpion' : '/' ?>';
		}, 3000)
	})
</script>