<?php
use app\components\Counters;
use app\models\Account;
use app\models\Actions;
use app\models\Users;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this app\components\FluidableView */
/* @var $content string */

AppAsset::register($this);
$user_id = Yii::$app->user->id;
if (!empty(Yii::$app->request->queryParams['preview'])){
	if (Yii::$app->user->identity->isAdmin){
		$user_id = (int)Yii::$app->request->queryParams['preview'];
	}
}
if (Yii::$app->user->id === $user_id) {
	$user = Yii::$app->user->identity;
} else {
	$user = Users::findIdentity($user_id);
}

if (isset($user->isAdmin) and $user->isAdmin) {
	$this->registerJsFile('/js/jquery.sparkline.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
}
if (Yii::$app->user->isGuest) {
	$this->registerJsFile('/js/loginPage.js');
}

if (empty($this->title)) {
	$this->title = Yii::t('main', 'Buy Zengram App Free - Get More Real Instagram Followers, Likes and Comments Fast');
}

/*
if (empty($this->metaTags['description'])) {
	$this->registerMetaTag([
		'name' => 'description',
		'content' => Yii::t('main', 'Buy Zengram app free and get more real Instagram followers, likes and comments fast. Know how to get followers and promote your Instagram account with Zengram!')
	]);
}
*/


if (!empty(Yii::$app->params['counters']['verifications'])) {
	foreach (Yii::$app->params['counters']['verifications'] as $name =>$verification) {
		$this->registerMetaTag([
			'name' => $name,
			'content' => $verification
		]);
	}
}
//if (empty($this->linkTags['publisher']) && !empty(Yii::$app->params['counters']['googlePlus']['id'])) {
//	$this->registerLinkTag([
//		'rel' => 'publisher',
//		'href' => '//plus.google.com/'.Yii::$app->params['counters']['googlePlus']['id']
//	]);
//}
/*if (empty($this->metaTags['keywords'])) {
	$this->registerMetaTag([
		'name' => 'keywords',
		'content' => Yii::t('main', 'Buy Real Instagram Followers, Followers On Instagram, Get Instagram Followers, Get Likes on Instagram App, Get More Instagram Followers, How to Get Followers on Instagram, Instagram Followers, Instagram Followers App, More Followers on Instagram, Real Instagram Followers, App To Get Followers On Instagram, Followers App, Free Followers On Instagram App, Get Followers for Instagram, Get Instagram Comments, Get Instagram Followers Fast, Get More Followers, Get Real Instagram Followers, How To Get Many Likes On Instagram, How To Get More Followers On Instagram, How To Get More Likes On Instagram, Instagram Comments, Instagram Follower, Instagram Get Followers, Instagram Get More Followers')
	]);
}*/

$pages = \app\models\Page::find()->all();
$canonical = $_SERVER['HTTP_HOST'] . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<link rel="canonical" href="https://<?= $canonical ?>" />
	<link rel="alternate" href="https://<?= $canonical ?>" hreflang="<?= Yii::t('main', 'en-us') ?>" />
	<meta property="og:title" content="<?= Yii::t('main', 'Promote yourself in Instagram with Zengram!') ?>" />
	<?php if (YII_ENV_PROD) : ?>
		<meta name="robots" content="index, follow" />
	<?php endif; ?>
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://<?= $_SERVER['HTTP_HOST'] ?>/" />
	<meta property="og:image" content="<?= Yii::$app->params['og_image'] ?>" />
	<link rel="image_src" href="<?= Yii::$app->params['og_image'] ?>" />
	<meta property="og:description" content="<?= Yii::t('main', 'Promote yourself in Instagram with Zengram! A unique service to attract followers exactly targeted from your city! Fast, comfortable, safe!') ?>" />
	<link rel="icon" href="/favicon.ico" type="image/x-icon">

	<?php $this->head() ?>
	<meta name="google-site-verification" content="IoF0fDI3U1U8rfydLPjAghRI9T9arP8a4EHh4EBUM9w" />
	<meta name="msvalidate.01" content="FBB56FB22231A8B9FD76C7C4500E8FE6"/>
</head>
<body>
<?php
if (!Yii::$app->user->isGuest) {
	$user = Users::findOne($user_id);
	if ($user !== null && $user->accountsCount > 0) {

		$action = Actions::findOne(['user_id' => $user_id]);
		$nonused = Account::find()->where([
			'and',
			['user_id' => $user_id],
			['>', 'account_followers', 50],
			['not in', 'instagram_id', (new Query())
				->select(['instagram_id'])
				->from('actions_log')
				//->where(['and', ['deleted' => 0], ['>=', 'added', new Expression('DATE_SUB(NOW(), INTERVAL 60 DAY)')]])
				->where(['and', ['deleted' => 0], ['>=', 'added', new Expression("(NOW() - '60 day'::interval)")]])
			]
		])->count();
		if ($action === null && $nonused > 0 && Yii::$app->language === 'ru'): ?>
			<div id="action" class="container-fluid" onclick="window.location.href='<?= Url::to(['/actions']) ?>'" style="cursor:pointer">
				<?= Yii::t('main', 'Do you want to get extra time for free? Take part in our <a href="{url}">Sale!</a>', ['url' => Url::to(['/actions'])]) ?>
				<a href="#" onclick="closeAction(); return false;" class="close"><span aria-hidden="true">&times;</span></a>
			</div>
			<?php
		endif;
	}
}
?>

<?php $this->beginBody() ?>
<div class="wrap">
	<?php
	$show_border = true;
	?>
	<!-- HEADER -->
	<?=$this->render('_header', compact('user', 'pages', 'show_border'))?>
	<!-- HEADER End -->

	<div class="<?= $this->fluid ? 'container-fluid' : 'container' ?>">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>

		<?= $content ?>
	</div>
	<?=$this->render('_footer', compact('user', 'pages'))?>
</div>

<?php $this->endBody() ?>
<!--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>-->

<!-- Counters -->
<?= Counters::render(); ?>

<script>
	<?php
	if(!isset($_COOKIE['main_entry_from'])){
		if(isset($_GET['utm_source'])){
			setcookie("main_entry_from", "from_yandex_direct",time()+3600*24*1825);
			echo ("window.yaCounter31898476.setUserID('".md5(rand(1,10000000))."');");
			echo ("window.yaCounter31898476.userParams({main_entry_from:'from_yandex_direct'});");
		}
		else{
			setcookie("main_entry_from", "straight_entry",time()+3600*24*1825);
			echo ("window.yaCounter31898476.setUserID('".md5(rand(1,10000000))."');");
			echo ("window.yaCounter31898476.userParams({main_entry_from:'straight_entry'});");
		}
	}

	?>
</script>
<!-- Counters -->

</body>
</html>

<?php $this->endPage() ?>
