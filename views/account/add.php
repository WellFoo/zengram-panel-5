<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

//$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app->session->hasFlash('AccountIncorrect')): ?>
	<div class="alert alert-danger">
		<?= Yii::$app->session->getFlash('AccountIncorrect');?>
	</div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('AccountCheckpoint')): ?>
	<style>
		.leftstr {
			float: left;
			height: 700px;
			margin-left: 30px;
			font-size: 18px;

		}

		.rightstr {
			float: right;
			height: 700px;
		}

	</style>

	<div id="modal-checkpoint">
		<div style="width:900px; height:700px;">
			<div class="leftstr">
				<?= Yii::$app->session->getFlash('AccountLogin'); ?><br>

				<?= Yii::t('views', 'It remains only to verify Instagram Account.<br> Do it online on <a target="_blank" href="https://instagram.com">Instagram.com</a><br>or open the Instagram app right now<br>on your smartphone to end the<br>activation and we’ll start working with your account.') ?>

				<br> <br> <br><br>
			</div>

			<div class="rightstr">

				<div style="background:url(<?= Yii::getAlias('@img/telefon.jpg') ?>) no-repeat right top; width:343px; height:669px;">
					<div style="padding-top:250px; text-align:center;"><?= Yii::$app->session->getFlash('AccountLogin'); ?></div>
				</div>
			</div>
		</div>

	</div>
<?php endif; ?>
<div class="site-login">
	<h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('AccountLoginError')): ?>
        <div class="alert alert-danger">
            <?php echo Yii::$app->session->getFlash('AccountLoginError'); ?>
        </div>
    <?php endif; ?>


	<?php $form = ActiveForm::begin([
		'id' => 'addAccount-form',
		'options' => ['class' => 'form-horizontal'],
		'fieldConfig' => [
			'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
			'labelOptions' => ['class' => 'col-lg-3 control-label'],
		],
	]); ?>

	<?= $form->field($model, 'login') ?>

	<?= $form->field($model, 'password')->passwordInput() ?>

	<div class="form-group">
		<div class="col-lg-offset-3 col-lg-9">
			<?= Html::submitButton(Yii::t('app', Yii::t('views', 'Add account')), ['class' => 'btn btn-danger btn-lg', 'name' => 'login-button']) ?>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
</div>
