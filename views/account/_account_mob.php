<?php

use app\models\Account;

/* @var $speeds array
 * @var $account Account
 * @var $status boolean
 */

?>
<div class="acc-pane col-xs-12">
	<!-- User info -->
	<figure class="acc-img-set">
		<figcaption>
			<b>@DEMO</b>
					<span>
						<?= Yii::t('views', 'You have') ?> 77 <?= Yii::t('views', 'photos') ?>
						<a href="#" data-toggle="popover" data-title="<?= Yii::t('views', 'Photo') ?>"
						   data-content="<?= Yii::t('views', 'Number of account&#39;s photos') ?>" class="helper" data-original-title=""
						   title=""><i class="fa fa-question-circle"></i></a>
					</span>
		</figcaption>
		<img class="acc-ava" src="/userdata/images/2120634761.jpg">
		<a class="acc-forgot-pass addNewAccount" href="#"><?= Yii::t('views', 'Change password') ?></a>
	</figure>
	<!-- User info end -->

	<!-- Subscribe info  -->
	<dl class="acc-subscr-set">
		<dt>
			<i class="fa fa-users"></i> <?= Yii::t('views', 'Followers') ?>
		</dt>
		<dd>1200</dd>
		<dt>
			<i class="fa fa-user-plus"></i> <?= Yii::t('views', 'Followings') ?>
		</dt>
		<dd>800</dd>
		<dt class="red-inner">
			<i class="fa fa-user"></i> <?= Yii::t('views', 'New followers') ?>
		</dt>
		<dd class="red-inner">+250</dd>
	</dl>
	<!-- Subscribe info  -->

	<!-- Toggle settings -->
	<dl class="acc-tog-set">
		<dt>
			<input type="checkbox" id="options-likes" name="Options[likes]" value="1" checked=""
				   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-heart&quot;></i>"
				   data-off="<i class=&quot;fa fa-heart&quot;></i>" data-onstyle="success">
			<?= Yii::t('views', 'Likes') ?>
		</dt>
		<dd>40</dd>
		<dt>
			<input type="checkbox" id="options-comment" class="function-status" name="Options[comment]"
				   value="1" checked="" data-toggle="toggle" data-style="ios"
				   data-on="<i class=&quot;fa fa-comment&quot;></i>"
				   data-off="<i class=&quot;fa fa-comment&quot;></i>" data-onstyle="success">
			<?= Yii::t('views', 'Comments') ?>
		</dt>
		<dd>12</dd>
		<dt>
			<input type="checkbox" id="options-follow" name="Options[follow]" value="1" checked=""
				   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-check&quot;></i>"
				   data-off="<i class=&quot;fa fa-check&quot;></i>" data-onstyle="success">
			<?= Yii::t('views', 'Follows') ?>
		</dt>
		<dd>33</dd>
		<dt>
			<input type="checkbox" id="options-unfollow" name="Options[unfollow]" value="1"
				   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-times&quot;></i>"
				   data-off="<i class=&quot;fa fa-times&quot;></i>" data-onstyle="success">
			<?= Yii::t('views', 'Unfollows') ?>
		</dt>
		<dd>-</dd>
		<a href="#" class="acc-timer-reset addNewAccount"><?= Yii::t('views', 'Reset activity counter') ?></a>
	</dl>
	<!-- Toggle settings -->

	<!-- Action settings  -->
	<div class="acc-act-set">
		<figure class="col-xs-6">
			<div class="btn-group">
				<figcaption class="text-center col-xs-12 mb10 visible-xs"><?= Yii::t('views', 'Timer of days') ?></figcaption>
				<a class="col-xs-12 btn btn-default btn-white dropdown-toggle"
				   data-toggle="dropdown"><?= Yii::t('views', 'Off') ?> <i class="dd fa fa-angle-down"></i></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="#"><?= Yii::t('views', 'Off') ?></a></li>
					<li><a href="#"><?= Yii::t('views', '1 day') ?></a></li>
					<li><a href="#"><?= Yii::t('views', '2 days') ?></a></li>
					<li><a href="#"><?= Yii::t('views', '3 days') ?></a></li>
					<li><a href="#"><?= Yii::t('views', '5 days') ?></a></li>
					<li><a href="#"><?= Yii::t('views', '10 days') ?></a></li>
					<li><a href="#"><?= Yii::t('views', '20 days') ?></a></li>
					<li><a href="#"><?= Yii::t('views', '30 days') ?></a></li>
					<li><a href="#"><?= Yii::t('views', '60 days') ?></a></li>
				</ul>
			</div>

		</figure>
		<figure class="col-xs-6">
			<div class="btn-group">
				<figcaption class="text-center col-xs-12 mb10"><?= Yii::t('views', 'Speed of work') ?></figcaption>
				<br/>
				<a class="col-xs-12 btn btn-default btn-white dropdown-toggle" data-toggle="dropdown">
					<?= Yii::t('views', 'Slow') ?>
					<i class="dd fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="#"><?= Yii::t('views', 'Slow') ?></a></li>
					<li><a href="#"><?= Yii::t('views', 'Medium') ?></a></li>
					<li><a href="#"><?= Yii::t('views', 'Fast') ?></a></li>
				</ul>
			</div>
		</figure>
		<figure class="col-xs-6">
			<figcaption class="text-center col-xs-12 mb10"><?= Yii::t('views', 'Actions') ?></figcaption>
			<a class="btn btn-success col-xs-12"><?= Yii::t('views', 'Start') ?></a>
		</figure>
		<figure class="col-xs-6">
			<figcaption class="text-center col-xs-12 mb10"><?= Yii::t('views', 'Settings') ?></figcaption>
			<a href="#" class="btn btn-default btn-gray col-xs-12 addNewAccount"><?= Yii::t('views', 'Settings') ?></a>
		</figure>
	</div>
	<!-- Action settings -->
	<div class="clearfix"></div>
</div>