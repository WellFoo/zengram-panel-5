<?php

use app\models\Account;
use app\models\Users;
use yii\helpers\Html;

/* @var $speeds array
 * @var $user Users
 * @var $account Account
 * @var $status boolean
 * @var $newComments int
 */

$options = $account->options;
if (is_null($options)) {
	$options = new \app\models\Options([
		'account_id' => $account->id
	]);
	$options->save();
}

if (!isset($newComments)) {
	$newComments = $account->getNewComments();
}

// TODO: На рефакторинг, хак vs. костыль
$follow_status = $account->follow_status;

if ($account->comment_status && $account->follow_status){
	$hours = floor(($account->pause_until - time())/3600);
	if ($hours > 0) {
		$account->comment_status =
		$account->follow_status = Yii::t('views', 'There are some temporary restrictions by Instagram on using follows and comments and they are paused on {hours} hours. The actions will start automatically when pause will be ended.', ['hours' => $hours]) . ' ' . Yii::t('views', 'Time is not subs on that period.');
	} else {
		$account->comment_status = $account->follow_status = 0;
	}
} elseif($account->comment_status){
	$pause_until = $account->comments_block_date + $account->comments_block_expire * 3600 ;
	if (!$options->follow){
		if ($pause_until < $account->pause_until){
			$pause_until = $account->pause_until;
		}
	}
	$hours = ceil(($pause_until-time())/3600);
	if ($hours > 0) {
		$account->comment_status = Yii::t('views', 'There are some restrictions on using of the action by Instagram and it is paused for {hours} hours. The actions will start automatically when pause will be ended.', ['hours' => $hours]);
		if ($options->follow == false) {
			$account->comment_status .= ' ' . Yii::t('views', 'Time is not subs on that period.');
		}
	} else {
		$account->comment_status = 0;
	}
} elseif($account->follow_status){
	$pause_until = $account->follow_block_date + $account->follow_block_expire * 3600 ;
	if (!$options->comment){
		if ($pause_until < $account->pause_until){
			$pause_until = $account->pause_until;
		}
	}
	$hours = ceil(($pause_until- time())/3600);
	if ($hours > 0) {
		$account->follow_status = Yii::t('views', 'There are some restrictions on using of the action by Instagram and it is paused for {hours} hours. The actions will start automatically when pause will be ended.', ['hours' => $hours]);
		if ($options->comment == false) {
			$account->follow_status .= ' ' . Yii::t('views', 'Time is not subs on that period.');
		}
	} else {
		$account->follow_status = 0;
	}
}
?>
<div class="acc-pane col-sm-6 col-lg-4" data-account-container="<?= $account->id; ?>">
	<!-- User info -->
	<figure class="acc-img-set">
		<figcaption>
			<b>
				<?= $account->isDemo() ?
					$account->login : Html::a(
						'@'.$account->login,
						'https://instagram.com/'.$account->login.'/',
						[
							'target' => '_blank',
							'title' => ($options->experimental ? Yii::t('views', 'Experimental') : '@'.$account->login),
							'class' => 'account-login'.($options->experimental ? ' text-danger' : '')
						]
				) ?>
			</b>
			<span>
				<?= Yii::t('views', 'You have') ?>
				<span style="display: inline" id="account_media_<?= $account->id; ?>"><?= $account->account_media; ?></span>
				<?= Yii::t('views', 'photos') ?>
				<a class="helper" href="#" data-toggle="popover" data-title="<?= Yii::t('views', 'Photo') ?>"
				   data-content="<?= Yii::t('views', 'Number of account&#39;s photos') ?>" data-original-title=""
				   title=""><i class="fa fa-question-circle"></i></a>
				<span class="clearfix"></span>
				<!-- Class name "disabled" makes a button gray -->
				<?php if ($account->user->upload_photo) {
					echo Html::a(Yii::t('views', 'Add photo'), ['content/index', 'id' => $account->id, '#' => 'photo-upload'], [
						'class' => 'btnew btn btn-gray col-md-6 col-sm-6 col-xs-6 hidden-xs',
						'data-toggle' => 'modal',
						'data-target' =>
							$user->balance < Account::IMAGE_UPLOAD_COST ?
								'#low-balance' :
								//										($options->experimental ? '#photo-upload-new' : '#photo-upload'),
								'#photo-upload-new',
						'data-upload-id' => $account->id
					]);
					echo Html::a(Yii::t('views', 'Comment'), [
						'content/index', 'id' => $account->id
					], [
						'class' => 'btnew btn btn-gray col-md-6 col-sm-6 col-xs-6 btn-comment hidden-xs'
					]);
				} else {
					echo Html::a(
						Yii::t('views', 'Comment'), $account->isDemo() ? ['#'] : [
							'content/index', 'id' => $account->id
						], [
							'class' => 'btnew btn btn-gray col-sm-12 btn-comment hidden-xs',
							'style' => 'width: 260px;'
						] + ($account->isDemo() ? [
						'data-toggle' => 'modal',
						'data-target' => '#addAccount',
					] : []));
				} ?>

			</span>
		</figcaption>
		<img class="acc-ava" src="<?= str_replace('http://', '//', $account->account_avatar); ?>">
		<?= Html::a(
			Yii::t('views', 'Change password'),
			['#'],
			$account->isDemo() ? [
				'data-toggle' => 'modal',
				'data-target' => '#addAccount',
				'class' => 'acc-forgot-pass',
			] : [
				'data-id' => $account->id,
				'class' => 'acc-forgot-pass changeAccountAction',
			]) ?>

		<span class="new-comments hidden-xs">
			<?= Html::a(
				Yii::t('views', 'New comments: {count}', ['count' => $newComments]),
				$account->isDemo() ? ['#'] : [
				'content/index', 'id' => $account->id
			], [
				'id' => 'newcomments_'.$account->id,
			] + ($account->isDemo() ? [
					'data-toggle' => 'modal',
					'data-target' => '#addAccount',
				] : [])) ?>
			<a href="javascript:void(0);" data-toggle="popover" data-title="<?= Yii::t('views', 'Clear') ?>"
			   data-content="<?= Yii::t('views', 'Clear number of new comments') ?>" class="helper" data-original-title=""
			   title="" onclick="clearNewComments(<?= $account->id; ?>);">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</span>
	</figure>
	<!-- User info end -->

	<!-- Subscribe info  -->
	<dl class="acc-subscr-set">
		<dt>
			<i class="fa fa-users"></i> <?= Yii::t('views', 'Followers') ?>
		</dt>
		<dd id="account_followers_<?= $account->id; ?>"><?= $account->account_followers; ?></dd>
		<dt class="red-inner">
			<i class="fa fa-user"></i> <?= Yii::t('views', 'New followers') ?>
		</dt>
		<dd class="red-inner">+<span style="display: inline" id="lastfollowers_<?= $account->id; ?>"><?= $account->getLastFollowers() ?></span></dd>
		<dt>
			<i class="fa fa-user-plus"></i> <?= Yii::t('views', 'Followings') ?>
		</dt>
		<dd id="account_follows_<?= $account->id; ?>"><?= $account->account_follows; ?></dd>
	</dl>
	<!-- Subscribe info  -->

	<!-- Toggle settings -->
	<dl class="acc-tog-set">
		<dt>
			<input type="checkbox" id="options-likes" class="projectOption"
					<?= $options->likes ? 'checked=""' : '' ?>
                   data-account="<?= $account->id; ?>" data-option="likes"
                   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-heart&quot;></i>"
                   data-off="<i class=&quot;fa fa-heart&quot;></i>" data-onstyle="success">
			<?= Yii::t('views', 'Likes') ?>
		</dt>
		<dd id="likes_<?= $account->id; ?>"><?= ($options->likes && !$options->unfollow) ? $account->likes : '-' ?></dd>
		<dt>
			<input type="checkbox" id="options-comment" class="projectOption"
			       data-account="<?= $account->id; ?>" data-option="comment"
					<?= $options->comment ? 'checked=""' : '' ?>
                   data-toggle="toggle" data-style="ios"
                   data-on="<i class=&quot;fa fa-comment&quot;></i>"
                   data-off="<i class=&quot;fa fa-comment&quot;></i>" data-onstyle="success">
			<?= Yii::t('views', 'Comments') ?>
		</dt>
		<dd id="comments_<?= $account->id; ?>" class="<?= $account->comment_status ? 'blocked' : '' ?>"
			<?php if ($account->comment_status): ?>
				class="blocked" data-toggle="popover" data-title="<?= Yii::t('views', 'Action paused') ?>"
				data-content="<?= $account->comment_status; ?>"
			<?php endif; ?>
		>
			<?php if ($account->comment_status): ?>
				<span class="glyphicon glyphicon-info-sign anim-ico" aria-hidden="true"></span>
			<?php endif; ?>
			<?= ($options->comment && !$options->unfollow) ? $account->comments : '-' ?>
		</dd>
		<dt>
			<input type="checkbox" id="options-follow" class="projectOption"
			       data-account="<?= $account->id; ?>" data-option="follow"
					<?= $options->follow ? 'checked=""' : '' ?>
                   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-check&quot;></i>"
                   data-off="<i class=&quot;fa fa-check&quot;></i>" data-onstyle="success">
			<?= Yii::t('views', 'Follows') ?>
		</dt>
		<dd id="follow_<?= $account->id; ?>"
		    <?php if ($account->follow_status && $options->follow): ?>
		        class="blocked" data-toggle="popover" data-title="<?= Yii::t('views', 'Action paused') ?>"
			    data-content="<?= $account->getFollowsStatusMessage($follow_status); ?>"
			<?php endif; ?>
		>
			<?php if ($account->follow_status && $account->show_follow_block && $options->follow): ?>
				<span class="glyphicon glyphicon-info-sign anim-ico" aria-hidden="true"></span>
			<?php endif; ?>
			<?= ($options->follow && !$options->unfollow) ? $account->follow : '-' ?>
		</dd>
		<dt>
			<input type="checkbox" id="options-unfollow" class="projectOption"
			       data-account="<?= $account->id; ?>" data-option="unfollow"
					<?= $options->unfollow ? 'checked=""' : '' ?>
                   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-times&quot;></i>"
                   data-off="<i class=&quot;fa fa-times&quot;></i>" data-onstyle="success">
			<?= Yii::t('views', 'Unfollows') ?>
		</dt>
		<dd id="unfollow_<?= $account->id; ?>"
			<?php if ($account->unfollow_status && $options->unfollow && $account->getUnfollowsPauseExpire() > 0): ?>
				class="blocked" data-toggle="popover" data-title="<?= Yii::t('views', 'Action paused') ?>"
				data-content="<?= $account->getUnfollowsStatusMessage(); ?>"
			<?php endif; ?>
		>
			<?php if ($account->unfollow_status && $options->unfollow && $account->getUnfollowsPauseExpire() > 0): ?>
				<span class="glyphicon glyphicon-info-sign anim-ico" aria-hidden="true"></span>
			<?php endif; ?>
			<?= $options->unfollow ? $account->unfollow : '-' ?>
		</dd>
		<dt class="acc-tog-set-reset">

			<?php if (!$account->isDemo()) : ?>

				<a href="#" data-id="<?= $account->id ?>" class="acc-timer-reset resetAction"><?= Yii::t('views', 'Reset activity counter') ?></a>

			<?php else: ?>

				<a href="#" class="acc-timer-reset" data-toggle="modal" data-target="#addAccount"><?= Yii::t('views', 'Reset activity counter') ?></a>

			<?php endif; ?>

		</dt>
		
	</dl>
	<!-- Toggle settings -->

	<!-- Action settings  -->
	<div class="acc-act-set">
		<figure class="col-xs-4 hidden-xs">
			<i class="fa fa-toggle-on hidden-xs"></i>
			<a class="helper hidden-xs" href="#" data-toggle="popover" data-title="<?= Yii::t('views', 'Action') ?>"
			   data-content="<?= Yii::t('views', 'Here you can start or stop work with project') ?>"
			   data-original-title="" title=""><i class="fa fa-question-circle"></i></a>
			<figcaption class="hidden-xs"><?= Yii::t('views', 'Action') ?></figcaption>

			<?php if (!$account->isDemo()) : ?>

				<a class="btn btn-success col-md-12 col-sm-12 col-xs-12 projectStart" data-account="<?= $account->id; ?>" data-login="<?= $account->login; ?>"
				   id="start_<?= $account->id; ?>"<?php if ($status == 1) {
					echo ' style="display: none;"';
				} ?>>
				<span<?php if ($account->message) {
					echo ' style="display: block;"';
				} ?> data-toggle="popover" data-title="<?= Yii::t('views', 'Project stopped') ?>"
				     data-content="<?= $account->getErrorMessage(); ?>"
				     class="read-message glyphicon glyphicon-info-sign" aria-hidden="true"></span>
					<?= Yii::t('views', 'Start') ?>
				</a>

			<?php else: ?>

				<a href="#" class="btn btn-success col-md-12 col-sm-12 col-xs-12" data-toggle="modal" data-target="#addAccount"><?= Yii::t('views', 'Start') ?></a>

			<?php endif; ?>


			<a class="btn btn-danger col-md-12 col-sm-12 col-xs-12 projectStop" data-account="<?= $account->id; ?>"
			   id="stop_<?= $account->id; ?>"<?php if ($status == 0) {
				echo ' style="display: none;"';
			} ?>>
				<?= Yii::t('views', 'Stop') ?>
			</a>
		</figure>

		<figure class="col-xs-4">
			<i class="fa fa-tachometer hidden-xs"></i>
			<a class="helper hidden-xs" href="#" data-toggle="popover" data-title="<?= Yii::t('views', 'Speed of work') ?>"
			   data-content="<?= Yii::t('views', 'Service work speed. We are highly recommending to choose Slow speed for first days and to increase speed with time') ?>"
			   data-original-title="" title=""><i class="fa fa-question-circle"></i></a>
			<figcaption class="hidden-xs"><?= Yii::t('views', 'speed of work') ?></figcaption>
			<figcaption class="text-center col-xs-12 mb5 visible-xs"><?= Yii::t('views', 'Speed') ?></figcaption>
			<div class="btn-group">
				<a class="col-md-12 col-sm-12 col-xs-12 btn btn-default btn-white dropdown-toggle"
				   id="speedButton_<?= $account->id; ?>"
				   data-toggle="dropdown"><?= $speeds[$options->speed]; ?> <i
							class="dd fa fa-angle-down"></i></span></a>
				<ul class="dropdown-menu" role="menu">
					<li>
						<a href="javascript:void" data-value="0" data-id="<?= $account->id; ?>" class="setSpeed">
							<?= Yii::t('views', 'Slow') ?>
						</a>
					</li>
					<li>
						<a href="javascript:void" data-value="1" data-id="<?= $account->id; ?>" class="setSpeed">
							<?= Yii::t('views', 'Medium') ?>
						</a>
					</li>
					<li>
						<a href="javascript:void" data-value="2" data-id="<?= $account->id; ?>" class="setSpeed">
							<?= Yii::t('views', 'Fast') ?>
						</a>
					</li>
				</ul>
			</div>

		</figure>


		<figure class="col-xs-4 hidden">
			<i class="fa fa-clock-o hidden-xs"></i>
			<a  class="helper hidden-xs" href="#" data-toggle="popover" data-title="<?= Yii::t('views', 'Timer of days') ?>"
			    data-content="<?= Yii::t('views', 'Time is counting down only when your activity is started. When time ends, work will be stopped') ?>"
			    data-original-title="" title=""><i class="fa fa-question-circle"></i></a>
			<figcaption class="hidden-xs"><?= Yii::t('views', 'timer of days') ?></figcaption>
			<figcaption class="text-center col-xs-12 mb5 visible-xs">
				<span class="desk"><?= Yii::t('views', 'Timer') ?></span>
				<span class="mob"><?= Yii::t('views', 'Stop after') ?></span>
			</figcaption>

			<div class="btn-group" <?= (!$status ? 'style="display: none;"' : '') ?> id="timerButtonDisabled_<?= $account->id; ?>"
			     data-toggle="popover" data-original-title="" data-title="<?= Yii::t('views', 'Unavailable') ?>"
			     data-content="<?= Yii::t('views', 'You must stop works in account to change timer') ?>">
				<a class="col-md-12 col-sm-12 col-xs-12 btn btn-default btn-white dropdown-toggle disabled timerButtonDisabled text-center">
					<?= $account->getTimerText() ?>
					<i class="dd fa fa-angle-down"></i>
				</a>
			</div>
			<div class="btn-group" <?= ($status ? 'style="display: none;"' : '') ?> id="timerButton_<?= $account->id; ?>">
				<a class="col-md-12 col-sm-12 col-xs-12 btn btn-default btn-white dropdown-toggle timerButton" data-toggle="dropdown">
					<?= $account->getTimerText() ?>
					<i class="dd fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu" role="menu">
					<?php $timers = [
							-1 => Yii::t('views', 'Off'),
							1  => Yii::t('views', '1 day'),
							2  => Yii::t('views', '2 days'),
							3  => Yii::t('views', '3 days'),
							5  => Yii::t('views', '5 days'),
							10 => Yii::t('views', '10 days'),
							20 => Yii::t('views', '20 days'),
							30 => Yii::t('views', '30 days'),
							60 => Yii::t('views', '60 days'),
					]; ?>
					<?php foreach ($timers as $value => $timer): ?>
						<li><a data-value="<?= $value ?>" data-id="<?= $account->id ?>" href="javascript:void"
						       class="setTimer"><?= $timer ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div>
		</figure>



		<div class="visible-xs"></div>
		<figure class="col-xs-4">
			<i class="fa fa fa-cogs hidden-xs"></i>
			<a class="helper hidden-xs" href="#" data-toggle="popover" data-title="<?= Yii::t('views', 'Settings') ?>"
			   data-content="<?= Yii::t('views', 'Here you can go to the settings and change action&#39;s configuration, locations, comments and other settings') ?>"
			   data-original-title="" title=""><i class="fa fa-question-circle"></i></a>
			<figcaption class="hidden-xs"><?= Yii::t('views', 'Settings') ?></figcaption>
			<?= Html::a(
				Yii::t('views', 'Settings'),
				$account->isDemo() ? ['#'] : [
					'options/index',
					'id' => $account->id]
				, [
					'class' => 'btn btn-default btn-gray col-md-12 col-sm-12 col-xs-12 optionsButton',
					'id' => 'optionsButton_'.$account->id
				] + ($account->isDemo() ? [
					'data-toggle' => 'modal',
					'data-target' => '#addAccount',
				] : [])
			); ?>
		</figure>
		<div class="clearfix visible-xs"></div>
		<figure class="wide-button visible-xs">
			<i class="fa fa-toggle-on hidden-xs"></i>
			<a class="helper hidden-xs" href="#" data-toggle="popover" data-title="<?= Yii::t('views', 'Actions') ?>"
			   data-content="<?= Yii::t('views', 'Here you can start or stop work with project') ?>"
			   data-original-title="" title=""><i class="fa fa-question-circle"></i></a>
			<figcaption class="hidden-xs"><?= Yii::t('views', 'Action') ?></figcaption>
			<a class="btn btn-success col-md-12 col-sm-12 col-xs-12 projectStart<?php if ($user->balance <= 0) {
				echo " disabled";
			} ?>" data-account="<?= $account->id; ?>" data-login="<?= $account->login; ?>"
			   id="start_<?= $account->id; ?>"<?php if ($status == 1) {
				echo ' style="display: none;"';
			} ?>>
				<span<?php if ($account->message) {
					echo ' style="display: block;"';
				} ?> data-toggle="popover" data-title="<?= Yii::t('views', 'Project stopped') ?>"
				     data-content="<?= $account->getErrorMessage(); ?>"
				     class="read-message glyphicon glyphicon-info-sign" aria-hidden="true"></span>
				<?= Yii::t('views', 'Start') ?>
			</a>
			<a class="btn btn-danger col-md-12 col-sm-12 col-xs-12 projectStop" data-account="<?= $account->id; ?>"
			   id="stop_<?= $account->id; ?>"<?php if ($status == 0) {
				echo ' style="display: none;"';
			} ?>>
				<?= Yii::t('views', 'Stop') ?>
			</a>
		</figure>
	</div>

	<!-- Action settings -->
	<div class="clearfix"></div>
</div>