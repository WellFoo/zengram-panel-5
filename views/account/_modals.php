<?php
use yii\helpers\Url;

?>

<!-- Modal whitelist end -->

<!-- Modal trap message -->
<div id="modal-trap" class="modal fade" tabindex="-1" role="dialog" aria-hidden="false">
	<div class="modal-dialog accountErrorModal">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				<p class="text-center" style = "font-size: 28px;">
					<?= Yii::t('views', 'The page remained inactive projects.') ?>
					<span style = "font-size: 18px; display: block; font-size: 21px; margin-bottom: 50px; color: #888;">
                        <?= Yii::t('views', 'do not forget to turn them on before you go.') ?>
                    </span>
				</p>
			</div>
			<div class = "modal-footer">
				<div style = "float: left;">

					<input
						type="checkbox"
						id="nsf"
						class="projectOption"
						data-toggle = "toggle"
						data-style = "ios"
						data-on = '<i class="fa fa-check" style ="top:-3px;position: relative;"></i>'
						data-off = '<i class="fa fa-check" style ="top:-3px;position: relative;"></i>'
						data-onstyle = "success"
					/>

					<label style = "display: inline-block; font-weight: 100;" for = "nsf">
						<?=Yii::t('views', 'Do not show again')?>
					</label>
				</div>
				<button class="btn" data-dismiss="modal"><?=Yii::t('app', 'Close')?></button>
			</div>
		</div>
	</div>
</div>
<!-- Modal trap message end -->

<!-- Modal changeAccount -->
<div id="changeAccount" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
	<div class="modal-dialog changePasswordModal">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<div class="modal-body">

			</div>
		</div>
	</div>
</div>
<!-- Modal changeAccount end -->

<!-- Modal accountAdded -->
<div id="accountAdded" class="modal fade bs-example-modal-lg zen-modal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
	<div class="modal-dialog changePasswordModal">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<div class="modal-body">
				<div class="row accountAddedContent">
					<?= Yii::t('app', 'Your account <span id="login"></span> have beed started.'); ?><br>

					<div id="city-block"
					     style="display: none"><?= Yii::t('app', 'Works automatically started in <span id="city"></span>'); ?></div>
					<?= Yii::t('app', 'Follows and likes are on by default.'); ?><br>
					<?= Yii::t('app', 'You make changes in project by clicking "Settings" button.'); ?>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<button type="submit" class="btn btn-success"
						        data-dismiss="modal"><?= Yii::t('app', 'Ok') ?></button>
					</div>
					<div class="col-xs-6">
						<a class="btn btn-danger" id="settings-button" href="#"><?= Yii::t('app', 'Settings') ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal changeAccount end -->

<!-- Modal Login Error -->
<div id="passwordError" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-lg accountErrorModal">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<h2 class="text-center"
			    style="margin: 0; line-height: 1.428571429; font-size: 18px !important; color: #ff6666;">
				<?= Yii::t('views', 'Incorrect account&#39;s username or password') ?> <span
					id="passwordErrorAccount"></span>
			</h2>
		</div>
	</div>
</div>
<!-- Modal Login Error end -->

<!-- Modal Followers limit -->
<div id="followersLimitModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-lg accountErrorModal">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<h2 class="text-center"
			    style="margin: 0; line-height: 1.428571429; font-size: 18px !important; color: #ff6666;">
				<?= Yii::t('views', 'Limit of followings is exceeded') ?>
			</h2>

			<p class="text-center"
			   style="color: #ff6666; margin: 25px auto 45px auto; font-size: 18px; line-height: 25px; font-family: 'Open Sans', sans-serif;">
				<?= Yii::t('views', 'A limit of followings of the account <span id="{span_id}"></span> is exceeded. It is necessary to unfollow.', ['span_id' => 'followersLimitAccount']) ?>
			</p>
		</div>
	</div>
</div>
<!-- Modal Followers limit end -->

<!-- Modal empty geo -->
<div id="emptyGeoModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-lg accountErrorModal">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<h2 class="text-center"
			    style="margin: 0; line-height: 1.428571429; font-size: 18px !important; color: #ff6666;">
				<?= Yii::t('views', 'No points for work') ?>
			</h2>

			<p class="text-center"
			   style="color: #ff6666; margin: 25px auto 45px auto; font-size: 18px; line-height: 25px; font-family: 'Open Sans', sans-serif;">
				<?= Yii::t('views', 'The account <span id="{span_id}"></span> has no points for work. It is necessary to adjust geography of spread.', ['span_id' => 'emptyGeoAccount']) ?>
			</p>

			<div class="modal-controls">
				<a href = "#" id = "continueTargetFailButton" class = "btn btn-success btn-lg" style = "margin-right: 10px;"><?=\Yii::t('app', 'Continue')?></a>
				<a data-dismiss="modal" class = "btn btn-default btn-lg"><?=\Yii::t('app', 'Close')?></a>
			</div>
			<div class="modal-inner-html"></div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- Modal empty geo end -->

<!-- Modal low balance -->
<div id="low-balance" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-lg accountErrorModal">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<p class="text-center"
			   style="color: #ff6666; margin: 25px auto 25px auto; font-size: 18px; line-height: 25px; font-family: 'Open Sans', sans-serif;">
				<?= Yii::t('views', 'There are not enough funds for this operation on your account. Please, <a href="{url}">recharge balance</a>, and retry operation.', ['url' => Url::to('page/price')]) ?>
			</p>
		</div>
	</div>
</div>
<!-- Modal low balance end -->

<!-- New Modal Upload image -->
<div class="modal fade" id="photo-upload-new" tabindex="-1" role="dialog" aria-labelledby="photo-upload">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<div class="modal-body">
				<div id="upload-image-panel">
					<div id="container_image">
						<h2 class="text-center"><?= Yii::t('views', 'Add photo') ?></h2>

						<div class="upload-description" style="color: #FF6666;">
							<p><?= Yii::t('views', 'For each upload from your account will be subtracted 5 hours.') ?></p>
						</div>
						<div class="upload-description">
							<div class="progress" style="display: none">
								<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0"
								     aria-valuemin="0" aria-valuemax="100">0%
								</div>
							</div>
							<p><?= Yii::t('views', '1. Select image for upload') ?></p>
							<input id="fileupload" type="file" name="file" data-url="/content/upload">
						</div>
						<div id="imageError"></div>

						<div class="upload-image-wrapper">
							<img id="upload-image-img" src="/img/en/no_image_available.png"/>
						</div>

						<div class="upload-description">
							<p><?= Yii::t('views', '2. Change zoom settings (if necessary)') ?></p>

							<div id="image-zoom-slider"></div>
						</div>
						<div class="upload-description">
							<p><?= Yii::t('views', '3. Add description') ?></p>
						</div>
						<textarea class="form-control image-upload-input" id="image-upload-input"
						          placeholder="<?= Yii::t('views', 'You can include up to 30 hashtags to description') ?>"></textarea>

						<div id="upload-image-alert" class="alert" style="display: none"></div>

						<input type="hidden" name="upload-id" id="upload-id">
						<button class="btn btn-success"
						        id="send-image"><?= Yii::t('views', 'Upload photo') ?></button>
						<p style="display: none" class="loading-send text-center"><img src="/img/ajax-loader.gif">
						</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<!-- New Modal Upload image end -->

<!-- Modal Upload image -->
<div class="modal fade" id="photo-upload" tabindex="-1" role="dialog" aria-labelledby="photo-upload">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<div class="modal-body">
				<div id="upload-image-panel">
					<div id="container_image">
						<h2 class="text-center"><?= Yii::t('views', 'Add photo') ?></h2>

						<div class="upload-description" style="color: #FF6666;">
							<p><?= Yii::t('views', 'For each upload from your account will be subtracted 5 hours.') ?></p>
						</div>
						<div class="upload-description">
							<p><?= Yii::t('views', '1. Select image for upload') ?></p>
							<input type="file" class="cropit-image-input">
						</div>
						<div id="cropitImageError"></div>

						<div class="upload-image-wrapper">
							<!-- .cropit-image-preview-container is needed for background image to work -->
							<div class="cropit-image-preview-container">
								<div class="cropit-image-preview"></div>
							</div>
						</div>

						<div class="upload-description">
							<p><?= Yii::t('views', '2. Change zoom settings (if necessary)') ?></p>
							<input type="range" class="cropit-image-zoom-input">
						</div>
						<div class="upload-description">
							<p><?= Yii::t('views', '3. Add description') ?></p>
						</div>
						<textarea class="form-control image-upload-input" id="image-upload-input"
						          placeholder="<?= Yii::t('views', 'You can include up to 30 hashtags to description') ?>"></textarea>

						<div id="upload-image-alert" class="alert" style="display: none"></div>

						<input type="hidden" name="upload-id" id="upload-id">
						<button class="btn btn-success"
						        onclick="sendImage()"><?= Yii::t('views', 'Upload photo') ?></button>
						<p style="display: none" class="loading-send text-center"><img src="/img/ajax-loader.gif">
						</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<!-- Modal Upload image end -->

<!-- Modal alert -->
<div id="alertModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-lg accountErrorModal">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<p id="alertModalContent" class="text-center"
			   style="color: #ff6666; margin: 25px auto 45px auto; font-size: 18px; line-height: 25px; font-family: 'Open Sans', sans-serif;"></p>
		</div>
	</div>
</div>
<!-- Modal alert end -->

<!-- Modal settings -->
<div class="modal" id="settings-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="right: -25px">
					<span aria-hidden="true">&times;</span>
				</button>
				<h2 class="modal-title"></h2>
				<div class="modal-controls"></div>
				<div class="modal-inner-html"></div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<!-- end of Modal settings -->

<!-- Modal geotags -->
<div id="geotags-modal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="right: -15px">
					<span aria-hidden="true">&times;</span>
				</button>
				<div id="geotag-manager"></div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- end of Modal geotags -->