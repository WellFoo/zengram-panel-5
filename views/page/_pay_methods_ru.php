<?php
use app\components\Counters;
?>
<div id="currencies">
	<span class="">
	<button type="button" class="pay-button yandex-cards-submit"><img src="/img/currencies/BankCard.png"></button>
<!--	<button type="button" class="pay-button interkassa-submit"><img src="/img/currencies/Qiwi.gif"></button>-->
	<button type="button" class="pay-button interkassa-submit"><img src="/img/currencies/logo-sberbank-online.png"></button>
	<button type="button" class="pay-button interkassa-submit"><img src="/img/currencies/AlfaBank.gif"></button>
	<button type="button" class="pay-button yandex-submit"><img src="/img/currencies/YandexMerchant.gif"></button>
	<button type="button" class="pay-button interkassa-submit"><img src="/img/currencies/Qiwi.gif"></button>
	<button type="button" class="pay-button interkassa-submit"><img src="/img/currencies/Beeline.gif"></button>
	<button type="button" class="pay-button interkassa-submit"><img src="/img/currencies/MTS.gif"></button>
	<button type="button" class="pay-button interkassa-submit"><img src="/img/currencies/megafon_icon_100.png"></button>
	<button type="button" class="pay-button interkassa-submit"><img src="/img/currencies/tinkoff_icon_100.png"></button>
	<button type="button" class="pay-button interkassa-submit"><img src="/img/currencies/bitcoin_icon_100.png"></button>
	</span>
	<span class="hidden">
	<button type="button" class="pay-button robokassa-submit"><img src="/img/currencies/BankCard.png"></button>
	<button type="button" class="pay-button robokassa-submit"><img src="/img/currencies/Qiwi.gif"></button>
	<button type="button" class="pay-button robokassa-submit"><img src="/img/currencies/YandexMerchant.gif"></button>
	<button type="button" class="pay-button robokassa-submit"><img src="/img/currencies/Beeline.gif"></button>
	<button type="button" class="pay-button robokassa-submit"><img src="/img/currencies/MTS.gif"></button>
	</span>
	<button type="button" class="pay-button paypal-submit"><img src="/img/currencies/Paypal.gif"></button>
</div>
<script>
$(document).ready(function () {

	$('.robokassa-submit').click(function (event) {
		<?= Counters::renderAction('invoice_create') ?>
		$(this).closest('form').attr('action', '/payment/invoice').submit();
		event.preventDefault();
		return false;
	});

	$('.paypal-submit').click(function (event) {
		<?= Counters::renderAction('invoice_create') ?>
		$(this).closest('form').attr('action', '/payment/paypal').submit();
		event.preventDefault();
		return false;
	});

	$('.interkassa-submit').click(function (event) {
		<?= Counters::renderAction('invoice_create_interkassa') ?>
		$(this).closest('form').attr('action', '/payment/interkassa').submit();
		event.preventDefault();
		return false;
	});

	$('.yandex-submit').click(function (event) {
		<?= Counters::renderAction('invoice_create') ?>
		$(this).closest('form').attr('action', '/payment/yandex').submit();
		event.preventDefault();
		return false;
	});

	$('.yandex-cards-submit').click(function (event) {
		<?= Counters::renderAction('invoice_create') ?>
		$(this).closest('form').attr('action', '/payment/yandex-cards').submit();
		event.preventDefault();
		return false;
	});
});
</script>