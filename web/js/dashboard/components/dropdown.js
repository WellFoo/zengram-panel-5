var DropdownComponent = can.Component.extend({
    tag: 'dropdown-list',
    template: can.view('dropdown-list-view'),
    viewModel: {
        items: [],
        selected: null
    },

    events: {
        'li > a click': function (a, event) {
            var value = $(a).data('value'),
                selectedItem = 0,
                items = this.viewModel.attr('items');

            for (var i = 0; i < items.length; i++) {
                if (items[i].value == value) {
                    selectedItem = items[i];
                }
            }

            this.viewModel.attr('selected', selectedItem);
        }
    }
});