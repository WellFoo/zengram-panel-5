var ToggleOptionComponent = can.Component.extend({
    tag: 'toggle-option',
    template: can.view('toggle-option-view'),
    viewModel: {
        content: '',
        option: '',
        icon: '',
        active: false,
        counter: 0
    },
    helpers: {
        getInsideContent: function (type) {
            return "<i class='fa fa-" + this.icon + "'></i>";
        }
    }
});