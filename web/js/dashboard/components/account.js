var AccountComponent = can.Component.extend({
	tag: 'instagram-account',
	template: can.view('account-view'),
	viewModel: {
		name: 'demo',
		accountAvatar: '/img/demo.png'
	},
	events: {
		/*
		 *  Переключение опции
		 */
		'.toggle.btn.ios click': function (element)
		{
			var $element = $(element).find('input[type="checkbox"][data-toggle="toggle"][data-option]'),
				option = $element.data('option'),
				account_id = $element.data('account'),
				account = window.accounts[account_id];

			account.lockOption(option);

			$.post('/site/options/' + account_id,
				{ 'option': backendFormatOption(option), 'value': $element.get(0).checked ? '0' : '1' },
				function (response) {
					if (response.status == 'ok') {
						for (var key in response.data) {
							account.unlockOption(frontendFormatOption(key));
							account.updateOptionState(frontendFormatOption(key), response.data[key].checked);
						}
					}
					account.unlockOption(option);
				}, 'JSON');
		},

		/*
		 *  Запуск проекта
		 */
		'.projectStart click': function (element) {
			window.accounts[$(element).data('account')].start();
		},

		/*
		 *  Остановка проекта
		 */
		'.projectStop click': function (element)
		{
			window.accounts[$(element).data('account')].stop();
		},

		/*
		 *  Изменение скорости
		 */
		'a.setSpeed click': function (element)
		{
			var account_id = $(element).parents('.acc-pane').data('account-container'),
				speed = $(element).data('value');

			window.accounts[account_id].setSpeed(speed);
		},

		/*
		 *  Очистить новые комментарии
		 */
		'a.clear-comments click': function (element)
		{
			var account_id = $(element).parents('.acc-pane').data('account-container');

			window.accounts[account_id].clearNewComments();
		},

		/*
		 *  Таймер
		 */
		'a.setTimer click': function (element, event)
		{
			event.preventDefault();
			event.stopPropagation();

			var id = element.data('id'),
				value = element.data('value');

			window.accounts[id].attr('timerText', element.text());
			element.closest('.btn-group.open').removeClass('open');

			$.post(
				'/site/timer/' + id,
				{
					'timer': value
				}
			);
		},

		/*
		 *  Сброс счетчиков по аккаунту
		 */
		'a.resetAction click': function (element, event)
		{
			event.preventDefault();
			event.stopPropagation();

			var account_id = $(element).parents('.acc-pane').data('account-container');
			window.accounts[account_id].reset();
		},

		/*
		 *  Смена пароля
		 */
		'a.acc-forgot-pass click': function (element, event)
		{
			var id = $(element).data('id');

			if (id < 1) {
				return;
			}

			event.preventDefault();
			event.stopPropagation();

			$("#changeAccount .modal-body").html('<p class="text-center"><img src="/img/ajax-loader.gif"></p>');
			$("#changeAccount").modal('show');

			$.get("/account/change-form/" + id, function (data)
			{
				$("#changeAccount .modal-body").html(data);

				var $form = $('#changeAccountForm');
				$form.on('submit', function (e)
				{
					e.preventDefault();
					e.stopImmediatePropagation();

					$form.find('button')
						.addClass('disabled')
						.addClass('loading');
					$.ajax({
						url: '/account/change/' + id,
						data: $form.serialize(),
						type: 'post',
						beforeSend: function ()
						{
							submitting = true;
							$form.find('.alert').hide();
						},
						success: function (data)
						{
							if (data.errors !== undefined) {
								$form.find('button')
									.removeClass('disabled')
									.removeClass('loading');
								$form.find('.alert').html('').show();
								if (data.errors.hasOwnProperty('error') && data.errors.error[0] == 'checkpoint_required') {
									var text = '<div style="overflow: hidden; line-height: 30px; text-align: left; color: #333333;">' +
										'<img class="pull-right" src="' + window.Zengram.t('/img/telefon.jpg') + '" style="width: 250px;">' +
										window.Zengram.t('It is necessary to verify @{account} Instagram account. Make this action on <a target="_blank" href="https://instagram.com">Instagram.com</a> or in your Instagram app and we will start work.', {account: $form.find('input[name*=login]').val()}) +
										'</div>';
									$('#alertModalContent').html(text);
									$('#changeAccount').modal('hide');
									$('#alertModal').modal();
									delete data.errors.error;
								}
								for (var input in data.errors) {
									if (data.errors.hasOwnProperty(input)) {
										$form.find('.alert').append(data.errors[input] + '<br>');
									}
								}
							}
							$form.modal('hide');
						},
						dataType: 'json'
					});
					return false;
				});
			});

			return false;
		},
	}
});