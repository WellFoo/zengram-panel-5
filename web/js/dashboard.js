$(document).find('#addAccount-form').on('submit', function (e) {
	e.preventDefault();
	e.stopImmediatePropagation();

	$('#addAccount-form button').addClass('disabled');

	var form = $('#addAccount-form');

	var params = form.serializeArray();

	try{
		var city = ymaps.geolocation.city;

		if (city.toString().length <= 0){
			city = window.Zengram.t('Default city');
		}

		console.log(city);

		geocoderDataSource(city, function(points)
		{
			placeChanged(points[0],function(place_func)
			{
				params.push({'name': 'place' , 'value': JSON.stringify(place_func)});

				sendAddAccount(form, params);

				$('#addAccount-form button').removeClass('disabled');
				reInitPopovers(document);
			});
		})
	} catch (e){
		sendAddAccount(form, params);
		$('#addAccount-form button').removeClass('disabled');

	}
});

$('.start-all').on("click", function () {
	var balance = null;

	$.ajax({
		url: '/site/current-balance/',
		async: false,
		type: 'GET',
		success: function(data){
			balance = data;
		}
	});

	if (balance != null && balance <= 0){
		$("#low-balance").modal('show');
		return;
	}

	$.get("/site/start/all", function (data)
	{
		if (data == 'ok')
		{
			$.each(window.accounts, function (iteration, account) {
				if (account) {
					account.attr('isActive', true);
				}
			});

			updateAccounts();
		}
		else {
			if (data == 'passwordError') {
				$("#loginError").modal('show');
			}
			else {
				alert("ERROR:" + data);
			}
		}
	});
	return false;
});

$('.stop-all').on("click", function () {
	$.each(window.accounts, function (iteration, account) {
		if (account) {
			account.attr('isActive', false);
		}
	});

	$.get("/site/stop/all", function (data) {});
	return false;
});