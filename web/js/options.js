'use strict';

// Togglers block
jQuery(function($){
	var $toggler = $('#search-by-toggler'),
		$globalTogglers = $('.projectSearchBy'),
		$placesPanel = $('#places-panel'),
		$regionsPanel = $('#regions-panel'),
		manageUrl = $('#targeting-panel').data('url');

	$('.panel-sub > .panel-header .toggle input').on('change', function() {
		$(this).parents('.panel-sub').first().toggleClass('disable', !this.checked);
	});

	$toggler.find('input').change(function(ev) {
		if ($(this).parent().hasClass('disabled')) {
			ev.preventDefault();
			return false;
		}
		$.post(
			manageUrl,
			{
				newValue: this.value,
				global:   false
			}
		);

		$placesPanel.hide('fast');
		$regionsPanel.hide('fast');
		switch (this.value) {
			case '1':
				$placesPanel.show('fast');
				break;
			case '3':
				$regionsPanel.show('fast');
				break;
		}

		$toggler.find('label')
			.removeClass('btn-success')
			.addClass('btn-default');
		$(this).closest('label').addClass('btn-success');
	});

	var $searchToggle = $('#search-by-toggle');
	var searchManageUrl = $searchToggle.data('url');
	$searchToggle.find('input').on('change.search', searchChange);

	$globalTogglers.parent('div').bind('.toggle click', function() {
		var val = $(this).find('.projectSearchBy').val();
		$.post(
			manageUrl,
			{
				newValue: val,
				global:   true
			},
			function(response) {
				$toggler.find('label').toggleClass('disabled', (Number(response.value) & 1) === 0);
				$globalTogglers.each(function() {
					$(this).prop('checked', (this.value == response.value)).change();
				});
			}
		);
	});

	function searchChange()
	{
		var $inputs = $searchToggle.find('input');
		$inputs.off('change.search');

		if (this.value == 1) {
			$searchToggle.find('#options-search-hashtag').prop('checked', !this.checked).change();
		} else {
			$searchToggle.find('#options-search-place').prop('checked', !this.checked).change();
		}

		$inputs.on('change.search', searchChange);

		var newVal = $searchToggle.find('input:checked').val();

		var byPlace = (newVal == 1);
		$('.set-panel.panel-hash')
			.toggleClass('active', !byPlace)
			.toggleClass('disable', byPlace);
		$('.set-panel.panel-city')
			.toggleClass('active', byPlace)
			.toggleClass('disable', !byPlace);

		if (byPlace && $toggler.length) {
			newVal = $toggler.find('input:checked').val();
		}

		$.post(
			searchManageUrl,
			{newValue: newVal}
		);
	}
});

// Targeting block
jQuery(document).ready(function($){
	var $settingsModal = $('#settings-modal'),
		$competitorsList = $('#competitors-list'),
		$hashtagsList = $('#hashtags-list'),
		$placesList = $('#places-list'),
		$regionsTree = $('#regions-list');

	var competitorsUrl = $competitorsList.data('url'),
		hashtagsUrl = $hashtagsList.data('url'),
		placesUrl = $placesList.data('url'),
		regionsUrl = $regionsTree.data('url');

	var competitorsData = new can.List([]);
	var hashtagsData = new can.List([]);
	var placesData = new can.List([]);
	var regionsData = new can.List(JSON.parse($('#regions-initial-data').text() || '{}'));

	var CompetitorsList = can.Control.extend({
		init: function(/*el, op*/) {
			this.element.html(can.view('competitors-list-template', this.options));
		},
		'.delete click': function (el) {
			if (!confirm(window.Zengram.t('Are you sure want to remove this account?'))) {
				return;
			}
			var item  = el.parents('div.balloon').data('item');
			runAjax(competitorsUrl, {
				action: 'delete',
				competitor: {
					id: item.id
				}
			}, this);
		},
		'input keypress': function(el, event){
			if (event.keyCode === 13) {
				competitorsAddClick(el, this);
				return false;
			}
		},
		'button click': function(el) {
			competitorsAddClick(el, this);
		},
		'input hidden.bs.popover': function(el) {
			$(el).popover('destroy');
		}
	});

	var HashtagsList = can.Control.extend({
		init: function(/*el, op*/) {
			this.element.html(can.view('hashtags-list-template', this.options));
		},
		'.delete click': function (el) {
			var item  = el.parents('div.balloon').data('item');
			runAjax(hashtagsUrl, {
				action: 'delete',
				hashtag: {
					id: item.id
				}
			}, this);
		},
		'input keypress': function(el, event){
			if (event.keyCode === 13) {
				hashtagAddClick(el, this);
				return false;
			}
		},
		'button click': function(el) {
			hashtagAddClick(el, this);
		},
		'input hidden.bs.popover': function(el) {
			$(el).popover('destroy');
		}
	});

	var PlacesList = can.Control.extend({
		init: function(el/*, op*/) {
			el.html(can.view('places-list-template', this.options));

			var control = this;
			var $input = el.find('input');
			$input.typeahead({
				minLength: 2,
				//delay: 1,
				matcher: function() { return true; },
				displayText: function(item) {
					return item === undefined ? '' : item.fullName;
				},
				source: geocoderDataSource,
				afterSelect: function(place) {
					$input.data('place', place);
					addMap(place, $input, control);
				}
			});
		},
		'.delete click': function (el) {
			//if (!confirm(window.Zengram.t('Are you sure want to remove this city?'))) {
			//	return;
			//}
			var item  = el.parents('div.balloon').data('item');
			runAjax(placesUrl, {
				action: 'delete',
				place: {
					id: item.id
				}
			}, this);
		},
		'input hidden.bs.popover': function(el) {
			$(el).popover('destroy');
		}
	});

	var RegionsTree = can.Control.extend({
		init: function(el) {
			el.html(can.view('regions-list-template', this.options));
			updateRegionsTree(el);
		},
		'a.toggler click': function(el) {
			var data = el.parent('li').data('this');
			data.attr('collapsed', !data.collapsed);
		},
		'input change': function(el) {
			var data = el.parents('li').first().data('this');
			data.attr('selected', el.prop('checked'));
			disableSubtree(data, el.prop('checked'));
			updateRegionsTree(this.element);
			$.post(
				regionsUrl,
				{
					action: el.prop('checked') ? 'append' : 'remove',
					id: el.val()
				}
			);
		}
	});

	function disableSubtree(data, state)
	{
		if (!data.items || !data.items.length) {
			return;
		}
		var sub;
		for (var i = 0; i < data.items.length; i++) {
			sub = data.items[i];
			if (state && sub.selected) {
				sub.attr('selected', false);
			}
			sub.attr('disabled', state);
			disableSubtree(sub, state);
		}
	}

	can.Mustache.registerHelper('prune_context', function(options) {
		return options.fn(new can.view.Scope(options.context));
	});

	var competitorsControl = new CompetitorsList('#competitors-list', {
		items: competitorsData,
		pending: new can.compute(false)
	});
	var hashtagsControl = new HashtagsList('#hashtags-list', {
		items: hashtagsData,
		pending: new can.compute(false)
	});
	var placesControl = new PlacesList('#places-list', {
		items: placesData,
		pending: new can.compute(false)
	});

	if ($regionsTree.length) {
		new RegionsTree('#regions-list', {items: regionsData});
	}

	$('.show-all a').on('click', function(){
		var data = $(this).data();
		showSettingsModal(data.type, data.caption);
		return false;
	});

	runAjax(competitorsUrl, {action: 'list'}, competitorsControl);
	runAjax(hashtagsUrl, {action: 'list'}, hashtagsControl);
	runAjax(placesUrl, {action: 'list'}, placesControl);

	function hashtagAddClick(el, control)
	{
		var $input = el.parents('.input-group').first().find('input');

		if (!$input.val()) {
			$input.focus();
		}

		runAjax(hashtagsUrl, {
			action: 'create',
			hashtag: {
				name: $input.val()
			}
		}, control);

		return false;
	}

	function competitorsAddClick(el, control)
	{
		var $input = el.parents('.input-group').first().find('input');

		if (!$input.val()) {
			$input.focus();
		}

		runAjax(competitorsUrl, {
			action: 'create',
			competitor: {
				name: $input.val()
			}
		}, control);

		return false;
	}

	function addMap(place, $input, control)
	{
		if (place) {
			placeChanged(place, function(place_func){
				runAjax(placesUrl, place_func, control);
			});
			$input.val('');
		}
		return false;
	}

	function runAjax(url, data, control)
	{
		control.options.pending(true);
		$.post(
			url,
			data,
			ajaxCallback.bind(control),
			'JSON'
		).always(function() {
			control.options.pending(false);
		});
	}

	function ajaxCallback(response)
	{
		var list, $el = $(this.element).find('input');

		switch (response.type) {
			case 'places':
				list = placesData;
				break;

			case 'hashtags':
				list = hashtagsData;
				break;

			case 'competitors':
				list = competitorsData;
				break;
		}

		if (response.error) {
			this.options.pending(false);
			var $e = this.element.find('.error-wrapper .error');
			$e.text(response.error);
			$e.show('fast');
			setTimeout(hideError.bind($e), 3000);
			return;
		}

		var i;
		switch (response.action) {
			case 'list':
				for (i = 0; i < response.data.length; i++) {
					list.push(response.data[i]);
				}
				break;

			case 'create':
				if (response.data instanceof Array) {
					for (i = 0; i < response.data.length; i++) {
						list.push(response.data[i]);
					}
				} else {
					list.push(response.data);
				}
				$el.val('');
				break;

			case 'delete':
				var index = -1;
				list.each(function(el, i){
					if (el.id == response.id) {
						index = i;
					}
				});
				if (index < 0) {
					break;
				}
				list.splice(index, 1);
				break;
		}
	}

	function hideError()
	{
		this.hide('fast');
	}

	function showSettingsModal(type, caption)
	{
		var $dataContainer = $settingsModal.find('.modal-inner-html'),
			listClass, listData, control;

		switch (type) {
			case 'geo':
				if ($('#search-by-toggler').find('input:checked').val() > 1) {
					listClass = RegionsTree;
					listData  = regionsData;
				} else {
					listClass = PlacesList;
					listData  = placesData;
				}
				break;

			case 'hashtags':
				listClass = HashtagsList;
				listData  = hashtagsData;
				break;

			case 'accounts':
				listClass = CompetitorsList;
				listData  = competitorsData;
				break;
		}

		$settingsModal.find('.modal-title')
			.text(caption);

		$dataContainer.html('');
		control = new listClass($dataContainer, {items: listData, pending: new can.compute(false)});

		$settingsModal.modal('show');
		$settingsModal.on('hidden.bs.modal', function() {
			control.destroy();
		});
	}

	function updateRegionsTree($el)
	{
		$el.find('input')
			.each(function() {
				$(this).prop('indeterminate', false);
			}).each(function() {
				if (!this.checked) {
					return;
				}
				$(this).parentsUntil($el, 'ul').slice(0, -1)
					.parent().prev('li')
					.children('label')
					.find('input')
					.prop('indeterminate', true);
			});
	}
});

// Comments block
(function($){
	var commentsRadioList = [10, 20, 50],
		commentsRadioListIndex = parseInt(fromStorage('commentsPerPageIndex', 0)),
		commentsPerPage = commentsRadioList[commentsRadioListIndex],
		currentPage = 0,
		$firmCommentsCheckbox,
		$commentInputHolder,
		$commentIdInput,
		$commentTextInput,
		$commentAddBtn,
		$commentEditBtn,
		$commentsList,
		$commentTemplate,
		$commentsPagination,
		$commentsCountSwitch,
		$commentsShowMoreBtn,
		isSmallScreen,
		commentManageUrl,
		commentsCount;

	function fromStorage(name, def)
	{
		if (typeof window.localStorage !== 'object') {
			return def;
		}
		return window.localStorage.getItem(name) || def;
	}

	function toStorage(name, val)
	{
		if (typeof window.localStorage !== 'object') {
			return;
		}
		window.localStorage.setItem(name, val);
	}

	function commentsShowPage(page)
	{
		var data = {
			action: 'list',
			offset: (page - 1) * commentsPerPage,
			limit: commentsPerPage
		};
		$.post(
			commentManageUrl,
			data,
			commentsListCallback,
			'JSON'
		).always(commentsAjaxCallback);
	}

	function commentsShowMore()
	{
		var data = {
			action: 'list',
			offset: (currentPage++) * 3,
			limit: 3
		};
		$.post(
			commentManageUrl,
			data,
			commentsListCallback,
			'JSON'
		).always(commentsAjaxCallback);
	}

	function setCommentsPerPage(i, val)
	{
		toStorage('commentsPerPageIndex', i);
		commentsPerPage = val;
		$commentsPagination.bs3paginator({
			pages: Math.ceil(commentsCount / commentsPerPage),
			current: 1
		});
		commentsShowPage(1);
	}

	function clearInputs()
	{
		$commentIdInput.val('');
		$commentTextInput.val('');
		$commentTextInput.css('height','34px');
		toggleCommentsMode('add');
	}

	function toggleCommentsMode(mode)
	{
		var editMode = (mode === 'edit');
		$commentAddBtn.toggle(!editMode);
		$commentEditBtn.toggle(editMode);
	}

	function runAjax(data) {
		$.post(
			commentManageUrl,
			data,
			commentsSuccessCallback,
			'JSON'
		).always(commentsAjaxCallback);
		$commentInputHolder.addClass('disable');
	}

	function commentsSuccessCallback(response)
	{
		switch (response.action) {
			case 'create':
				createComment(response.id, response.text, true);
				break;

			case 'update':
				updateComment(response.id, response.text);
				break;

			case 'delete':
				deleteComment(response.id);
				break;

			case 'firm':
				useFirmChanged(response.count);
				break;
		}
	}

	function useFirmChanged(count)
	{
		$firmCommentsCheckbox.parent().removeClass('disable');
		$commentsPagination.bs3paginator({
			pages: Math.ceil(count / commentsPerPage)
		});
		commentsShowPage(1);
	}

	function commentsAjaxCallback()
	{
		$commentIdInput.val('');
		$commentTextInput.val('');
		toggleCommentsMode('add');
		$commentInputHolder.removeClass('disable');
	}

	function createComment(id, text, prepend)
	{
		var $comment = $commentTemplate.clone();
		$comment.data('id', id);
		$comment.attr({
			id: 'comment_' + id,
			style: ''
		});
		$comment.find('.content').text(text);

		if (prepend) {
			$comment.prependTo($commentsList);
			adjustListSize();
		} else {
			$comment.appendTo($commentsList);
		}
	}

	function updateComment(id, text)
	{
		var $comment = $('#comment_' + id);
		$comment.find('.content').text(text);
	}

	function deleteComment(id)
	{
		$('#comment_' + id).hide('fast', function(){
			$(this).remove();
			adjustListSize();
		});
	}

	function adjustListSize() {
		var h = 0;
		$commentsList.children().each(function () {
			h += $(this).outerHeight(true);
		});
		$commentsList.animate({
			height: h + 'px'
		}, 400);
	}

	function commentsListCallback(data)
	{
		$commentsList.css('height', $commentsList.height() + 'px');
		$commentsList.children().each(function(){
			$(this).remove();
		});

		for (var i = 0; i < data.length || i < 10; i++) {
			if (data[i]) {
				createComment(data[i].id, data[i].text);
			}
		}

		adjustListSize();
	}

	$(document).on('ready', function(){
		$firmCommentsCheckbox = $('#firmCommentsCheckbox');
		$commentInputHolder = $('#commentInputHolder');
		$commentIdInput = $commentInputHolder.find('#commentId');
		$commentTextInput = $commentInputHolder.find('#commentText');
		$commentAddBtn = $commentInputHolder.find('#commentAddBtn');
		$commentEditBtn = $commentInputHolder.find('#commentEditBtn');
		$commentsList = $('#commentsList');
		$commentTemplate = $('#commentTemplate');
		$commentsPagination = $('#commentsPagination');
		$commentsCountSwitch= $('#commentsCountSwitch');
		$commentsShowMoreBtn = $('#commentsShowMore');
		commentManageUrl = $commentTextInput.data('url');
		commentsCount = parseInt($commentsPagination.data('count'));
		isSmallScreen = (window.innerWidth < 768);

		$commentTextInput.on('keypress', function(event){
			if (event.keyCode === 13) {
				if ($commentIdInput.val().length) {
					$commentEditBtn.click();
				} else {
					$commentAddBtn.click();
				}
				return false;
			}
		});

		$commentAddBtn.on('click', function() {
			if (!$commentTextInput.val().length) {
				$commentTextInput.focus();
				return false;
			}

			var data = {
				action: 'create',
				comment: {
					text: $commentTextInput.val()
				}
			};

			runAjax(data);
			$commentTextInput.css('height','34px');
			$commentTextInput.focus();
			return false;
		});

		$commentEditBtn.on('click', function() {
			if (!$commentTextInput.val().length) {
				$commentTextInput.focus();
				return false;
			}

			var data = {
				action: 'update',
				comment: {
					text: $commentTextInput.val(),
					id: $commentIdInput.val()
				}
			};

			runAjax(data);
			return false;
		});

		$('#commentClearBtn').on('click', function(){
			clearInputs();
			return false;
		});

		$commentsList.on('click', '.delete', function(event) {
			var $target = $(event.target),
				data = {
					action: 'delete',
					comment: {
						id: $target.parents('.balloon').data('id')
					}
				};

			runAjax(data);
			return false;
		});

		$commentsList.on('click', '.edit', function(event) {
			var $comment = $(event.target).parents('.balloon');
			$commentIdInput.val($comment.data('id'));
			$commentTextInput.val($comment.find('.content').text());
			toggleCommentsMode('edit');
		});

		$firmCommentsCheckbox.on('change', function(){
			$firmCommentsCheckbox.parent().addClass('disable');
			runAjax({
				action: 'switch-firm',
				firm: $firmCommentsCheckbox[0].checked ? 1 : 0
			});
		});

		if (isSmallScreen) {
			$commentsShowMoreBtn.on('click', function(){
				commentsShowMore();
				return false;
			});
			commentsShowMore();
		} else {
			$commentsPagination.bs3paginator({
				callback: commentsShowPage,
				classes: {
					activeBtn: 'success'
				},
				pages: Math.ceil(commentsCount / commentsPerPage),
				current: 1
			});

			$commentsCountSwitch.bs3radio({
				callback: setCommentsPerPage,
				classes: {
					activeBtn: 'success'
				},
				list: commentsRadioList,
				current: commentsRadioListIndex
			});
			commentsShowPage(1);
		}
	});
})(jQuery);