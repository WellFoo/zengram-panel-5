jQuery(function($){ 'use strict';
	var supportRequestInProgress = false;

	$('#contact-form button[type=submit]').on('click', function (event, jqXHR, settings) {
		if (supportRequestInProgress) {
			return false;
		}
		var $form = $('#contact-form');
		if ($form.find('.has-error').length) {
			return false;
		}
		$form.find('#message, #error').hide();
		supportRequestInProgress = true;
		$.ajax({
			url: $form.attr('action'),
			method: 'post',
			data: $form.serialize(),
			success: function (data) {
				if (data == 'ok') {
					$('#support').modal('hide');
					$('#support-success').modal();
				} else {
					$form.find('#error').show().text(window.Zengram.t('Error sending request'));
				}
			}
		}).always(function() {
			supportRequestInProgress = false;
		});

		return false;
	});

	$('.toggle-menu').on('click', function(){
		$(this).siblings('.nav-top').toggleClass('active');
	});

	/* // TODO: не нужно?

	var togglePanels = $('.panel-city, .panel-hash');

	function setPanelHeight()
	{
		var currentPanelHeight = 0;
		togglePanels.each(function(){
			var panelHeight = $(this).find('.set-panel-body').outerHeight();
			if(currentPanelHeight < panelHeight){
				currentPanelHeight = panelHeight;
			}
		});

		togglePanels.find('.set-panel-body').css('min-height', currentPanelHeight);
	}

	if (togglePanels.length) {
		setInterval(setPanelHeight, 1000);
	}*/

});

function alertModal(text){
	$('#alertModalContent').html(text);
	$('#alertModal').modal();
}