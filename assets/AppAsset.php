<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use Yii;
use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $jsOptions = array(
		'position' => View::POS_HEAD
	);
	public $css = [
		'css/bootstrap-toggle.min.css',
		'css/font-awesome.min.css',
		'css/jquery.tagit.css',
		'css/site.css',
	];
	public $js = [
		'js/zengram-i18n.js',
		'js/bootstrap-toggle.min.js',
		'js/jquery.dotdotdot.min.js',
		'js/bootbox.min.js',
		'js/tag-it.js',
		'js/global.js',
		'js/share.js',
		'js/debug.js'
	];
	public $depends = [
		'yii\web\JqueryAsset',
		'yii\bootstrap\BootstrapAsset',
		'yii\bootstrap\BootstrapPluginAsset',
		'yii\web\YiiAsset',
		'yii\jui\JuiAsset',
		'yii\bootstrap\BootstrapPluginAsset',
	];

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();
		foreach ($this->css as $key => $file) {
			$filename = Yii::getAlias('@app/web/'.$file);
			if (!file_exists($filename)) {
				continue;
			}
			$this->css[$key] = $file.'?t='.filectime($filename);
		}
		foreach ($this->js as $key => $file) {
			$filename = Yii::getAlias('@app/web/'.$file);
			if (!file_exists($filename)) {
				continue;
			}
			$this->js[$key] = $file.'?t='.filectime($filename);
		}
	}


}
