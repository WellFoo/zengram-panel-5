<?php namespace yii\db;

/**
 * Class Command
 * @package yii\db
 * @method $this batchInsertIgnore($table_name, $columns, $data)
 */
class Command extends \yii\base\Component {}