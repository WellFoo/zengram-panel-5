<?php namespace yii;

use app\components\CustomComponents;
use yii\base\Application;
use yii\console\ConsoleApplication;
use yii\web\WebApplication;

/**
 * @property Application|WebApplication|ConsoleApplication|CustomComponents $app the application instance
 */
class BaseYii {}