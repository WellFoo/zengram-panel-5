<?php

namespace app\forms;

use app\components\StringCallback;
use app\models\Account;
use app\models\Device;
use app\models\Users;
use core\redis\Exception;
use yii\base\Model;
use yii\base\ErrorException;
use app\components\RPCHelper;
use DOMDocument;
use DOMXPath;
use Yii;

class AccountCreateForm extends Model
{
	public $login;
	public $password;
	public $new_id;

	public function rules()
	{
		return [
			['login', 'filter', 'filter' => 'trim'],
			[['login', 'password'], 'required'],
			['login', 'string', 'max' => 50],
			['password', 'string', 'max' => Account::MAX_PASSWORD_LENGTH],
			['login', 'unique',
				'targetClass'     => Account::className(),
				'targetAttribute' => 'login',
				'message'         => new StringCallback([$this, 'loginErrorMessage'])
			],
			//['login', 'checkLogin']
		];
	}

	public function loginErrorMessage()
	{
		return Yii::t('app', 'Account @{login} already exists', ['login' => $this->login]);
	}

	public function attributeLabels()
	{
		return [
			'login'    => Yii::t('app', 'Instagram login'),
			'password' => Yii::t('app', 'Password')
		];
	}

	static private function getServer()
	{
		$server = RPCHelper::getIdleServer();
		if (empty($server)){
			$server = Yii::$app->params['serverIP'];
		}

		return $server;
	}

	static private function getInstagramId($login) {
		try {
			$content = file_get_contents('http://instagram.com/' . $login);
		} catch (ErrorException $e) {
			return false;
		}

		$document = new DOMDocument();
		$document->loadHTML($content);

		$xpath = new DOMXPath($document);
		$js = $xpath->query('//body/script[@type="text/javascript"]')->item(0)->nodeValue;

		$start = strpos($js, '{');
		$end = strrpos($js, ';');
		$json = substr($js, $start, $end - $start);

		$data = json_decode($json, true);
		$data = $data['entry_data']['ProfilePage'][0];

		if (!empty($data['user']['id'])) {
			return intval($data['user']['id']);
		}

		return false;
	}

	public function save()
	{
		if (!$this->validate()) {
			return false;
		}
		$photo = null;
		$media_count = null;
		$follower_count = null;
		$following_count = null;

		/*
		 * Создаем новый аккаунт в базе данных.
		 */
		$account = new Account();
		$proxy = $account->getProxyByUserId(Yii::$app->user->id);

		$device = Device::getRandom();

		try {
			$result = Account::check($this->login, $this->password, null, $proxy, $device);

			Yii::info('Данные успешно получены');

			$instagram_id = $result['user']['pk'];
			$photo = $result['user']['profile_pic_url'];

			$media_count = $result['user']['media_count'];
			$follower_count = $result['user']['follower_count'];
			$following_count = $result['user']['following_count'];

		} catch (\Exception $e) {
			Yii::error($e->getMessage());
			Yii::error('Не удалось получить данные');

			$instagram_id = intval(self::getInstagramId($this->login));
			Yii::info('id получен через web');
		}

		/*
		 * Корректность instagram_id
		 * Если instgram_id некорректен, то такого пользователя не существует, либо не удалось определить id по другим причинам
		 */
		if ($instagram_id < 1 or $instagram_id === false) {
			$this->addError('password', Yii::t('app', 'Incorrect login or password'));
			return false;
		}

		/*
		 * Проверка наличия данного инстаграм аккаунта в базе данных
		 */
		$result = Account::find()->where(['instagram_id' => $instagram_id]);

		if ($result->exists()) {
			$this->addError('login', Yii::t('app', 'Account with same Instagram ID already exists'));
			return false;
		}

		$account->login 		= $this->login;
		$account->password 		= $this->password;
		$account->user_id 		= Yii::$app->user->id;
		$account->instagram_id	= $instagram_id;
		$account->server 		= self::getServer();
		$account->device_id 	= $device['id'];
		$account->is_new 		= 1;
		$account->auth_block 	= 0;

		if (!is_null($photo)) { $account->account_avatar = $photo; }

		if (!is_null($media_count)) { $account->account_media = intval($media_count); }
		if (!is_null($follower_count)) { $account->account_followers = intval($follower_count); }
		if (!is_null($following_count)) { $account->account_follows = intval($following_count); }

		if ($account->save())
		{
			/*
			 * Подготовка опций и комментариев для работы аккаунта
			 */
			$account->createOptions();		// Опции
			$account->fillFirmComments();	// Комментарии

			/** @var Users $user */
			$user = Users::findOne(Yii::$app->user->id);
			if ($user) {
				/*
				 * Обновляем информацию о пользователе.
				 * Помечаем аккаунт, как не демонстрационный.
				 */
				$user->demo = 0;
				$user->save();
			}

			$this->new_id = $account->id;
			return true;
		}

		/*
		if (!$account->add()){
			$this->addErrors($account->errors);
			return false;
		}
		*/
		return false;
	}
}