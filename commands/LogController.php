<?php

namespace app\commands;

use app\components\TaskController;
use app\models\Account;
use app\models\AccountProcessLog;
use app\models\Mail;
use app\models\Users;
use core\logger\codes\ZengramCodes;
use core\logger\Logger;
use ReflectionClass;
use Yii;

class LogController extends TaskController
{

	public function actionCheck()
	{
		$account = Account::findOne([
			'instagram_id' => 3111261249
		]);

		AccountProcessLog::log([
			'worker_ip' => ip2long($account->server),
			'instagram_id' => $account->instagram_id,
			'user_id' => $account->user_id,
			'code' => Logger::Z011_ACCOUNT_START_BY_SPREAD_PROXY,
		]);
	}

	public function actionClean()
	{
		$table = AccountProcessLog::tableName();
		self::log('Запуск очистки таблицы логов ' . $table);

		// Удаление всех записей без кодов через 10 дней
		self::log('Очищено (без кодов) ' .
			number_format(AccountProcessLog::deleteAll(['and',
				['code' => 0],
				['<', 'date', time() - 10 * 86400]
			]), 0, '.', ' ')
		);

		$refl = new ReflectionClass(ZengramCodes::class);

		foreach ($refl->getConstants() as $key => $value) {

			$lifetime = Logger::getCodeLifetime($value, true);

			if ($lifetime === 0) continue;

			self::log('Очищен: ' . $key . ' ' .
				number_format(AccountProcessLog::deleteAll(['and',
					['code' => $value],
					['<', 'date', time() - $lifetime]
				]), 0, '.', ' ')
			);
		}
	}

	public function actionStatus()
	{
		$table = AccountProcessLog::tableName();

		self::log('Запуск анализа таблицы логов ' . $table);

		self::log('Записей в таблице: ' .
			number_format(AccountProcessLog::find()->count(), 0, '.', ' ')
		);

		self::log('Вес таблицы:', false);

		$size = [
			'no-index' => Yii::$app->db->createCommand(
				'select pg_relation_size(\'' . $table . '\')'
			)->queryOne()['pg_relation_size'],
			'total' => Yii::$app->db->createCommand(
				'select pg_total_relation_size(\'' . $table . '\')'
			)->queryOne()['pg_total_relation_size'],
		];

		self::log(' -- без индексов: ' . $this->formatBytes($size['no-index']));

		self::log(' -- c индексами: ' . $this->formatBytes($size['total']));

	}

	private function formatBytes($size, $precision = 2) {
		$base = log($size, 1024);
		$suffixes = array('', 'Kb', 'Mb', 'Gb', 'Tb');

		return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)];
	}

	public function actionShrink()
	{
		$messages = Logger::getMessages();
		$rules = Logger::getRules();

		$continue = false;



		foreach ($messages as $code => $message) {

			if ($code === Logger::Z150_USERS_PROCEED) {
				$continue = true;
			}

			if (!$continue) continue;

			self::log('Обработка сообщения: '.$message);

			if (in_array($code, $rules[Logger::PARAM_END])) {

				$sql = sprintf('
					UPDATE "account_process_log"
					SET code = %d, description = REPLACE(description, \'%s\', \'\')
					WHERE description LIKE \'%s\'',
					$code,
					$message,
					$message.'%'
				);

			} else if (in_array($code, $rules[Logger::PARAM_START])) {

				$sql = sprintf('
					UPDATE "account_process_log"
					SET code = %d, description = REPLACE(description, \'%s\', \'\')
					WHERE description LIKE \'%s\'',
					$code,
					$message,
					'%'.$message
				);

			} else if (in_array($code, $rules[Logger::PARAM_PARAM])) {

				$start = mb_strpos($message, '%PARAM%');
				$end = $start + mb_strlen('%PARAM%');

				$start_str = mb_substr($message, 0, $start);
				$end_str = mb_substr($message, $end, mb_strlen($message));

				$sql = sprintf('
					UPDATE "account_process_log"
					SET code = %d, description = REPLACE(REPLACE(description, \'%s\', \'\'), \'%s\', \'\')
					WHERE description LIKE \'%s\' AND description LIKE \'%s\'',
					$code,
					$start_str,
					$end_str,
					$start_str.'%',
					'%'.$end_str
				);

			} else {

				$sql = sprintf('
					UPDATE "account_process_log"
					SET code = %d, description = \'\'
					WHERE description LIKE \'%s\'',
					$code,
					$message
				);

			}

			if (empty($sql)) continue;

			self::log('Обработанное количество: ' . count(Yii::$app->db->createCommand($sql)->queryAll()));
		}



	}

	public function actionShrinkStatus()
	{
		$messages = Logger::getMessages();
		$rules = Logger::getRules();

		$all_count = AccountProcessLog::find()->count();
		$counter = 0;

		foreach ($messages as $code => $message) {

			self::log('Обработка сообщения: '.$message);

			$query = AccountProcessLog::find();

			if (in_array($code, $rules[Logger::PARAM_END])) {

				$query->where([
					'LIKE', 'description', $message.'%', false
				]);

			} else if (in_array($code, $rules[Logger::PARAM_START])) {

				$query->where([
					'LIKE', 'description', '%'.$message, false
				]);

			} else if (in_array($code, $rules[Logger::PARAM_PARAM])) {

				$start = mb_strpos($message, '%PARAM%');
				$end = $start + mb_strlen('%PARAM%');

				$query->where(['and',
					['LIKE', 'description', mb_substr($message, 0, $start).'%', false],
					['LIKE', 'description', '%'.mb_substr($message, $end, mb_strlen($message)), false],
				]);

			} else {

				$query->where([
					'description' => $message
				]);
			}

			$count = $query->count();
			self::log('Количество: '.$count);
			$counter += $count;
		}

		var_dump($counter . ' / ' . $all_count);
	}

	public function actionConstantCheck()
	{
		$refl = new ReflectionClass(ZengramCodes::class);

		$res = [];

		foreach ($refl->getConstants() as $key => $value) {
			if (!empty($res[$value])) {
				print_r($key . ' = ' . $value . ' = ' . $res[$value] . PHP_EOL);
			} else {
				$res[$value] = $key;
			}
		}
	}

	public function actionT()
	{
		/*echo Yii::$app->controller->renderPartial('//layouts/mail_en', [
				'caption' => 'Zen-promo.com: Pay card new followers in instagram',
				'body' => [
				'Hello!',
				'Last few month it was impossible to pay by credit card on our site. 
We are glad to inform that now you can do payment services by card again.
Relax and get new followers in instagram with our service'
			],
				'token' => '',
				'options' => ['price_url' => true]
			]
		);*/

		$user = Users::findOne(3459);

		if (!is_null($user)) {
			Mail::deleteAll([
				'mail' => $user->mail,
				'type' => 'lowbalance'
			]);
		}
	}
}